
from datetime import datetime, timedelta
from openpyxl import Workbook, load_workbook
import requests  # für API
import json
import time


'''
Diese Funktion ruft die benötigten Wetterdaten für den betrachteten Zeitraum in der Zukunft ab.
Die Werte werden zur Berechnung der Einstrahlungs- und Leistungsprognose benötigt. 
Die abgerufenen Werte werden in einer Excel-Datei gespeichert, die dann als Input-Datei für die Klasse PVSystem dient.
'''


def create_Excel_PV_Forecast ():
    wb_weather = Workbook()  # neue Excel-Datei für Ergebnisse
    ws_weather = wb_weather.active
    ws_weather['A1'] = 'Datum und Uhrzeit'

    # Daten von openweathermap (API)
    ws_weather['B1'] = 'Temperatur in °C '

    # Daten von solcast (API)
    ws_weather['C1'] = 'ghi in W/m2'
    ws_weather['D1'] = 'dni in W/m2'
    ws_weather['E1'] = 'dhi in W/m2'
    ws_weather['F1'] = 'zenith sun'
    ws_weather['G1'] = 'azimuth sun'
    ws_weather['H1'] = 'ghi90 in W/m2'
    ws_weather['I1'] = 'ghi10 in W/m2'
    ws_weather['J1'] = 'dni90 in W/m2'
    ws_weather['K1'] = 'dni10 in W/m2'
    return wb_weather, ws_weather


def forecast_weather(latitude, longitude, hours_forecast, h_factor, openweathermap_key, solcast_key):
    wb_weather, ws_weather = create_Excel_PV_Forecast()

    timenow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    timenow = datetime.strptime(timenow, "%Y-%m-%d %H:%M:%S")
    timestamp_now = datetime.timestamp(timenow)

    local_now = timenow.astimezone()
    local_tz = local_now.tzinfo
    local_tzname = local_tz.tzname(local_now)  # aktuelle Zeitzone Sommer-oder Winterzeit

    # 1. runden h_factor (15 min) Wert ab jetzt finden
    for timestamp in range(int(timestamp_now), int(timestamp_now + ((3600 * h_factor) + 1))):
        if timestamp % (3600 * h_factor) == 0:
            timestamp_start = timestamp
            break
        else:
            timestamp += 1

    # gibt aktuelles Datum und Uhrzeit in Zeitschritt für betrachteten zukünftigen Zeitraum aus
    row = 2
    while row < (hours_forecast / h_factor + 2):
        plus_h_factor = datetime.fromtimestamp(timestamp_start)
        plus_h_factor = plus_h_factor.strftime("%Y-%m-%d %H:%M:%S")
        plus_h_factor = datetime.strptime(plus_h_factor, "%Y-%m-%d %H:%M:%S")
        ws_weather['A' + str(row)] = plus_h_factor
        timestamp_start += (3600 * h_factor)
        row += 1

    # stündliche Wetterdaten abrufen
    url = requests.get("http://api.openweathermap.org/data/3.0/onecall?lat=" + str(latitude) + "&lon=" + str(longitude)
                       + "&units=metric&appid=" + openweathermap_key + "&")
    data_complete = json.loads(url.text)
    data_list = data_complete['hourly']  # Stündliche Wettervorhersage

    # Wettervorhersage in Excel abspeichern
    for i in range(0, hours_forecast + 1):
        data_hour = data_list[i]  # für die nächsten h durchlaufen
        dt = data_hour['dt']  # timestamp
        time1 = datetime.fromtimestamp(dt)
        hour1 = time1.strftime("%H")

        # Wert aus API Antwort ziehen
        temp = data_hour['temp']

        for row in range(2, int(hours_forecast / h_factor + 2)):
            time2 = ws_weather['A' + str(row)].value  # string
            #time2 = datetime.strptime(time2, "%Y-%m-%d %H:%M:%S")
            hour2 = time2.strftime("%H")

            if hour1 == hour2:
                # bei gleicher Zeit die Werte jeweils zuordnen
                ws_weather['B' + str(row)] = temp

            row += 1

    # Einstrahlungswerte mit Solcast abrufen, geht nur 10 mal pro Tag, nach Anmeldung als Student
    # in halbstündigen Schritten
    h_factor_solcast = 0.5
    url = requests.get("https://api.solcast.com.au/world_radiation/forecasts?"
                       "latitude=" + str(latitude) + "&longitude=" + str(longitude) +
                       "&output_parameters=ghi,dni,dhi&format=json&api_key=" + str(solcast_key))
    data_complete = json.loads(url.text)
    data_forecasts = data_complete['forecasts']

    # Werte aus Antwort API sortieren
    for i in range(0, int(hours_forecast * 2 + 4)):
        data_half_hour = data_forecasts[i]
        ghi = data_half_hour['ghi']
        ghi90 = data_half_hour['ghi90']
        ghi10 = data_half_hour['ghi10']
        dni = data_half_hour['dni']
        dni90 = data_half_hour['dni90']
        dni10 = data_half_hour['dni10']
        dhi = data_half_hour['dhi']
        zenith = data_half_hour['zenith']
        azimuth = data_half_hour['azimuth']

        # nicht als timestamp sondern als Zeit angegeben, deswegn ntspr. umrechnen
        time_utc = data_half_hour['period_end']  # Ende des Mitteilungszeitraums in UTC timezone, Format ISO 8601

        for row in range(2, int(hours_forecast / h_factor + 2)):
            time_eur = ws_weather['A' + str(row)].value  # aktuelle euopäische Zeit
            time_eur = time_eur.strftime("%Y-%m-%d %H:%M:%S")
            # je nach Sommer- oder Winterzeit umrechnen in UTC Zeit
            if local_tzname == 'Mitteleuropäische Sommerzeit':
                time_eur = datetime.strptime(time_eur, "%Y-%m-%d %H:%M:%S") + timedelta(hours=-2)
                time_eur = time_eur.strftime("%Y-%m-%dT%H:%M:%S.0000000Z")  # auf gleiches Format bringen
            elif local_tzname == 'Mitteleuropäische Zeit':
                time_eur = datetime.strptime(time_eur, "%Y-%m-%d %H:%M:%S") + timedelta(hours=-1)
                time_eur = time_eur.strftime("%Y-%m-%dT%H:%M:%S.0000000Z")

            n = h_factor_solcast / h_factor  # Anzahl für die die Werte übertragen werden können, bei 15 min Werten= 2
            for h in range(1, int(n + 1)):  # für h=1 und h=2 (einmal minus15 min und einmal minus 30 min)
                # Zeiten aus der Schnittstelle an Zeitschritte anpassen
                time_utc_diff = datetime.strptime(time_utc, "%Y-%m-%dT%H:%M:%S.0000000Z") + timedelta(
                    minutes=-(h * h_factor * 60))
                time_utc_diff = time_utc_diff.strftime("%Y-%m-%dT%H:%M:%S.0000000Z")

                # wenn Zeiten gleich sind entsprechende Werte in Excel abspeichern
                if time_utc_diff == time_eur:
                    ws_weather['C' + str(row)] = ghi
                    ws_weather['D' + str(row)] = dni
                    ws_weather['E' + str(row)] = dhi
                    ws_weather['F' + str(row)] = zenith
                    ws_weather['G' + str(row)] = azimuth
                    ws_weather['H' + str(row)] = ghi90
                    ws_weather['I' + str(row)] = ghi10
                    ws_weather['J' + str(row)] = dni90
                    ws_weather['K' + str(row)] = dni10

    wb_weather.save(r'Input/VorhersageWetterdaten.xlsx')


'''
Diese Funktion ruft die Daten zur Standortlast in dem betrachteten Zeitraum ab. Dabei kann, je nach Zeitraum,
die Vergangnheit mit einbezogen werden oder nur eine Prognose. Die Daten werden aus den Standortlastprofilen des BDEW 
bezogen. Diese sind in Abhängigkeit der Jahreszeit und des Wochentags hinterlegt. Außerdem sind diese in 15 min 
Zeitschritten hinterlegt. 

Aktuell fnktioniert der Code auch nur wenn ebenfalls ein Stundenfaktor von 0,25 hinterlegt ist.  

Die hinterlegte Profile sind für einen durchschnittlichen Jahresverbrauch von 1000 kWh/a = 1 MWh/a angegeben. 
Diese werden dann mit einem durchschnittlichem Jahresverbrauch der betrachteten Institution hochgerechnet.
'''


def forecast_load(hours_forecast, h_factor, institution, average_load_a, hours_past=None):
    # In Excel Tabelle hinterlegte Standortlastprofile vom BDEW laden
    wb_load = load_workbook(r'Input/Repräsentative_Profile_VDEW.xlsx')
    ws_load = wb_load[str(institution)]

    wb_location = Workbook()  # neue Ergebnis Excel
    ws_location = wb_location.active
    ws_location['A1'] = 'Datum und Uhrzeit'
    ws_location['B1'] = 'Benötigte Last in kW'
    ws_location['C1'] = 'Datum'
    ws_location['D1'] = 'Uhrzeit'
    ws_location['E1'] = 'Wochentag'

    timenow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")  # datetime
    timenow_string = datetime.strptime(timenow, "%Y-%m-%d %H:%M:%S")  # string
    timestamp_now = datetime.timestamp(timenow_string)

    if hours_past is not None:
        time_past = datetime.strptime(timenow, "%Y-%m-%d %H:%M:%S") + timedelta(hours=-int(hours_past))
        timestamp_past = datetime.timestamp(time_past)

        hours_total = hours_past + hours_forecast

        for timestamp in range(int(timestamp_past), int(timestamp_now)):
            if timestamp % (3600 * h_factor) == 0:  # exakter 15 min Step
                timestamp_start = timestamp
                break
            else:
                timestamp += 1
    else:
        hours_total = hours_forecast

        for timestamp in range(int(timestamp_now), int(timestamp_now + ((3600 * h_factor) + 1))):
            if timestamp % (3600 * h_factor) == 0:
                timestamp_start = timestamp
                break
            else:
                timestamp += 1

    # ab timestamp_start dann immer plus sekunden h_factor ausgeben, Datum und Uhrzeit getrennt in Excel schreiben
    row = 2
    while row < (hours_total / h_factor + 2):  # Datum und Uhrzeit in h_factor Steps für betrachteten Zeitraum
        plus_h_factor = datetime.fromtimestamp(timestamp_start)

        ws_location['A' + str(row)] = plus_h_factor  # Zeile A muss datetime sein

        ws_location['C' + str(row)] = plus_h_factor.strftime("%m-%d")
        ws_location['D' + str(row)] = plus_h_factor.strftime("%H:%M:%S")
        ws_location['E' + str(row)] = datetime.weekday(plus_h_factor)  # Nummer für Wochentag hinterlegen

        row += 1
        timestamp_start += (3600 * h_factor)

    # wenn Zeiten gleich sind, die jeweiligen Lastwerte aus dem jeweiligen hinterlegten Profil übernehmen
    # Abhängigkeit von Jahreszeit vom Wochentag

    # Winter: 1.11. - 20.03
    winter_lower_bound = "11-01"  # mm-dd
    winter_lower_bound = datetime.strptime(winter_lower_bound, "%m-%d")
    winter_upper_bound = "03-20"
    winter_upper_bound = datetime.strptime(winter_upper_bound, "%m-%d")
    # Sommer: 15.05 - 14.09
    summer_lower_bound = "05-15"
    summer_lower_bound = datetime.strptime(summer_lower_bound, "%m-%d")
    summer_upper_bound = "09-14"
    summer_upper_bound = datetime.strptime(summer_upper_bound, "%m-%d")
    # Übergangszeit 21.03. - 14.05 und 15.09 bis 31.10

    for row in range(2, int((hours_total / h_factor + 2))):
        # Datum und Zeit aus Liste
        date = ws_location['C' + str(row)].value
        date = datetime.strptime(date, "%m-%d")
        weekday = ws_location['E' + str(row)].value
        time1 = str(ws_location['D' + str(row)].value)

        # Winterzeit
        if winter_upper_bound > date >= winter_lower_bound:
            if weekday == 5:  # Samstag
                column = "B"
            elif weekday == 6:  # Sonntag
                column = "C"
            else:
                column = "D"  # Werktag

        # Sommerzeit
        elif summer_upper_bound > date >= summer_lower_bound:
            if weekday == 5:  # Samstag
                column = "E"
            elif weekday == 6:  # Sonntag
                column = "F"
            else:
                column = "G"  # Werktag

        # Überganszeit
        else:
            if weekday == 5:  # Samstag
                column = "H"
            elif weekday == 6:  # Sonntag
                column = "I"
            else:
                column = "J"  # Werktag

        row_ws = 4
        # Hinterlege Standortlastprofile
        while row_ws in range(4, 100):

            # Uhrzeit aus hinterlegten Lastprofilen
            time2 = str(ws_load['A' + str(row_ws)].value)

            # funktioniert nur wenn h_factor = 0,25
            if time2 == time1:

                # könnte Spalte B - J sein je nach Jahreszeit und Wochentag
                load = ws_load[str(column) + str(row_ws)].value

                # auf durchschnittlichen Jahresverbrauch hochrechnen
                load = load * average_load_a

                ws_location['B' + str(row)] = load / 1000  # in kW angeben

                break
            else:
                row_ws += 1

    wb_location.save(r'Input/VorhersageStandortlast.xlsx')
