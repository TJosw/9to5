
'''
Diese Klasse ermittelt auf Basis von Vorhersagedaten für das Wetter und die Standortlast, die zu erwartene Leistung der
PV-Anlage. Das Ergebnis ist die erneuerbare Erzeugung, die durch die PV-Anlage, zum Laden des Elektroautos zur
Verfügung steht. Davon ist die Standortlast zu jedem Zeitschritt bereits abgezogen.
'''

from openpyxl import Workbook, load_workbook
from openpyxl.utils import get_column_letter
from datetime import datetime
from numpy import log as ln
import math


class PVsystem:
    def __init__(self, pv_angle, pv_azimut, weatherdata, age, uncertainty, inverter_power, efficiency_power,
                 name='PVAnlage', p_modul_max=None, p_max=None, noct=46, temperature_coefficient_u=-0.003,
                 temperture_coefficient_i=0.0004, efficiency_10power=None, efficiency_eur=None, efficiency_cec=None,
                 locationload=None, numbermodules=None, battery=None, historical_data=None, albedo=0.2):

        # Eigenschaften der PV Anlage
        self.name = name  # Name der PV Anlage
        self.pv_angle = pv_angle  # Anstellwinkel
        self.pv_azimut = pv_azimut  # Ausrichtung der PV Anlage nach NORD/SÜD/OST/WEST
        self.numbermodules = numbermodules  # Anzahl PV Module
        self.p_modul_max = p_modul_max  # Peakleistung kWp eines Moduls
        self.p_max = p_max  # Peakleistung kWp der gesamten Anlage
        self.age = age  # Alter
        self.noct = noct  # Normal operating cell temperature in °C
        self.temperature_coefficient_u = temperature_coefficient_u  # Temperatur Koeffizient für Spannung -0,3 %/K
        self.temperature_coefficient_i = temperture_coefficient_i  # Temperatur Koeffizeint für Strom 0,04 %/K
        self.albedo = albedo

        # Unsicherheitsfaktor für die Prognose
        self.uncertainty = uncertainty

        # Daten zum Wechselrichter
        self.inverter_power = inverter_power
        self.efficiency_power = efficiency_power
        # Wenn Wirkungsgrad bei 10 % Nennleistung nicht vorliegt MUSS Wirkungsgrad EUR ODER CEC vorliegen
        self.efficiency_10power = efficiency_10power
        self.efficiency_eur = efficiency_eur
        self.efficiency_cec = efficiency_cec

        # benötigte Vorgersage/Inputdaten, für den gleichen Zeitraum
        self.weatherdata = weatherdata  # Wetterdaten
        self.locationload = locationload  # Standortlastprofil

        # Vergangenheitsdaten der PV Leistung
        self.historical_data = historical_data

        # Batteriespeicherkonzept
        self.battery = battery

    # Ermitteln der zum Laden des Elektroautos zur Verfügung stehenden Leistung pro Zeitschritt
    def get_energy(self):

        # Standard Test Condition (STC)
        temperature_stc = 25  # °Celcius
        solar_radiation_stc = 1000  # W/m2

        # Normal operating cell temperature (NOCT)
        temperature_noct = 20  # °Celcius
        solar_radiation_noct = 800  # W/m2

        if self.p_max is None:
            p_max = self.p_modul_max * self.numbermodules
        else:
            p_max = self.p_max

        if self.p_modul_max is None:  # wenn p_modul_max nicht angegeben aus p_max berechnen
            p_modul_max = self.p_max / self.numbermodules
        else:
            p_modul_max = self.p_modul_max

        # Annahmen, wenn technische Daten nicht bekannt sind, Defaultwert ist hinterlegt
        noct = self.noct
        temperature_coefficient_u = self.temperature_coefficient_u
        temperature_coefficient_i = self.temperature_coefficient_i

        weatherdata = self.weatherdata
        wb_weather = load_workbook(weatherdata)
        ws_weather = wb_weather.active

        # Stundenfaktor ermitteln
        h_factor = abs((ws_weather.cell(row=3, column=1).value - ws_weather.cell(row=4, column=1).value)).seconds / 3600

        pv_angle = self.pv_angle
        pv_azimut = self.pv_azimut
        pv_angle = pv_angle * (math.pi / 180)  # Umrechnung degree in rad
        pv_azimut = pv_azimut * (math.pi / 180)

        # Ergebnislisten
        radiation = []  # Einstrahlung auf PV-Fläche
        p_total_time = []  # Leistung pro Zeit, inkl. WR
        p_without_inv = []  # Leistung pro Zeit, ohne WR

        # Wenn Vergangenheitsdaten vorhanden sind an Liste mit Leistung ohne WR anhängen
        if self.historical_data is not None:
            historical_data = self.historical_data
            wb_historical = load_workbook(historical_data)
            ws_historical = wb_historical.active

            for i in range(2, ws_historical.max_row + 1):
                day_time = ws_historical['A' + str(i)].value

                p = ws_historical['B' + str(i)].value  # in kW
                p_without_inv.append((day_time, p))

                e = ws_historical['C' + str(i)].value  # in W/m2
                radiation.append(e)

            len_historical = len(radiation)
        else:
            len_historical = 0

        # Einstrahlung auf die geneigte Fläche berechnen
        for row in range(2, ws_weather.max_row + 1):
            # Sonnenposition: Sonnenhöhe und Sonnenazimut aus Liste von API ziehen
            sun_zenith = ws_weather['F' + str(row)].value  # geht von 0 bis 180. 90 bedeutet am Horizont
            sun_h = 90 - sun_zenith  # Winkel der Sonnenhöhe in degree

            sun_azimut = ws_weather['G' + str(row)].value  # degree

            # Umwandlung des Schnittstellen Azimut in Berechnung Azimut
            if sun_azimut > 0:
                sun_azimut = 360 - sun_azimut
            else:
                sun_azimut = sun_azimut * -1  # wenn wert negativ, einfach Betrag verwenden

            # Umrechnung degree in rad
            sun_h = sun_h * (math.pi / 180)
            sun_azimut = sun_azimut * (math.pi / 180)

            '''
            #Berechnung Einfallswinkel aus Quaschning Rechnung 
            angle_incidence = math.acos(-math.cos(sun_h) * math.sin(pv_angle) * math.cos(sun_azimut - pv_azimut) +
                                        math.sin(sun_h) * math.cos(pv_angle))
            # Umrechnung rad in deg
            angle_incidence = angle_incidence * (180 / math.pi)  # Einfalsswinkel pro Zeitschritt in degree
            '''

            # Aus API solcast : GHI, DNI, DHI
            ghi = ws_weather['C' + str(row)].value
            dni = ws_weather['D' + str(row)].value  # nur bei Quaschning in Berechnung des direkten Anteils enthalten
            dhi = ws_weather['E' + str(row)].value

            '''
            10 % und 90 % Quartil Wert der Einstrahlung aus Solcast Schnittstelle 
            Ansatz, diese über den Mittelwert mit einzubeziehen:  
            
            ghi90 = ws_weather['H' + str(row)].value
            ghi10 = ws_weather['I' + str(row)].value
            ghi = (ghi + ghi90 + ghi10) / 3
            '''

            dir_hor = ghi - dhi  # direkter horizontaler Anteil

            # direkter Anteil Einstrahlung
            if sun_h > 0:
                radiation_dir = dir_hor * (math.sin(sun_h + pv_angle) / math.sin(sun_h))
            else:
                # bei negativer Sonnenhöhe ist die Sonne untergegangen, keine Einstrahlung mehr
                radiation_dir = 0

            # diffuser Anteil Einstrahlung, Berechnung mit isotropem Ansatz
            if ghi > 0:
                radiation_diff = dhi * (1 / 2) * (1 + math.cos(pv_angle))
            else:
                radiation_diff = 0

            # reflektierter Anteil Eintsrahlung in Abhängigkeit Albedo Wert, Annahme A = 0,2
            if type(self.albedo) == str:
                wb_data = load_workbook(r'Input/Datenbank_Wechselrichter_Albedo.xlsx')
                ws_albedo = wb_data['Albedo']

                for row in range(2, int(ws_albedo.max_row + 1)):
                    if ws_albedo['A' + str(row)].value == self.albedo:
                        albedo = ws_albedo['B' + str(row)].value
                        break
            else:
                albedo = self.albedo

            radiation_ref = ghi * albedo * (1/2) * (1 - math.cos(pv_angle))

            # Einstrahlung Summe
            radiation_pv = radiation_dir + radiation_diff + radiation_ref
            radiation.append(radiation_pv)

        # Wechselrichter "Datenbank" laden
        wb_data = load_workbook(r'Input/Datenbank_Wechselrichter_Albedo.xlsx')
        ws_inverter = wb_data['Wechselrichter']

        inverter_power = self.inverter_power    # PN
        efficiency_power = self.efficiency_power  # nN

        # wenn Wirkungsgrad bei 10 % Nennleistung nicht angegeben ist mit nEUR oder nCEC aus Datenbank ermitteln
        if self.efficiency_10power is None:

            # (nN & nEUR)
            if self.efficiency_eur is not None:
                efficiency_eur = self.efficiency_eur
                for row in range(3, 12):  # range an Länge Datenbank anpassen
                    if ws_inverter['A' + str(row)].value == round(efficiency_eur):
                        index_row = row
                for i_column in range(3, 11):  # range an Länge Datenbank anpassen
                    column = get_column_letter(i_column)
                    if ws_inverter[str(column) + '1'].value == round(efficiency_power):
                        index_column = column

                efficiency_10power = ws_inverter[str(index_column) + str(index_row)].value

            # (nN & nCEC)
            else:
                efficiency_cec = self.efficiency_cec
                for row in range(43, 52):  # range an Länge Datenbank anpassen
                    if ws_inverter['A' + str(row)].value == round(efficiency_cec):
                        index_row = row
                for i_column in range(3, 11):  # range an Länge Datenbank anpassen
                    column = get_column_letter(i_column)
                    if ws_inverter[str(column) + '41'].value == round(efficiency_power):
                        index_column = column

                efficiency_10power = ws_inverter[str(index_column) + str(index_row)].value

        # Peb und k berechnen mit PN, nN und n10N (einmalig für weitere Berechnungen)
        inverter_power_own = inverter_power/9 * ((1/(efficiency_10power/100)) - (1/(efficiency_power/100)))
        k = 10/(9*(efficiency_power/100)) - 1/(9*(efficiency_10power/100)) - 1

        # Ermittlung Modultemperatur, in Abh. von Umgebungstemperatur und Einstrahlung
        temp_modul_time = []

        i = len_historical
        for row in range(2, ws_weather.max_row + 1):
            day_time = ws_weather['A' + str(row)].value
            temp_amb = ws_weather['B' + str(row)].value
            solar_radiation = radiation[i]

            temp_modul = temp_amb + (noct - temperature_noct) * (solar_radiation / solar_radiation_noct)
            temp_modul_time.append((day_time, temp_modul))  # Liste mit Modultemperatur in Zeitschritten
            i += 1

        # Leistungswerte für die Prognose ermitteln
        row = 2
        for i in range(0, len(temp_modul_time)):
            time = ws_weather['A' + str(row)].value
            solar_radiation = radiation[i + len_historical]

            if solar_radiation == 0:
                p_modul = 0  # keine Modulleistung wenn keine Einstrahlung
            else:
                # Ermittlung Modulleistung im Arbeitspunkt in Abhängigkeit der Modultemp. und der Einstrahlung
                p_modul = p_modul_max * ((ln(solar_radiation)) / (ln(solar_radiation_stc))) * (solar_radiation/solar_radiation_stc) \
                        * (1 + temperature_coefficient_u * (temp_modul_time[i][1] - temperature_stc)) \
                        * (1 + temperature_coefficient_i * (temp_modul_time[i][1] - temperature_stc))

            # Anlagenleistung
            p_sum = p_modul * self.numbermodules

            # Alterungsfaktor
            loss_age = 0.0015  # Annahme 0,15 % pro Jahr Leistungsverlust
            p_sum = p_sum * ((1-loss_age) ** self.age)

            # Aktuell: konstanter Unsicherheitsfaktor hinterlegt, Potenzial diesen anzupassen für weitere Überlegungen
            p_uncertainty = p_sum * (1 - self.uncertainty)

            # Liste für Leistung ohne Wechselrichter, für Batteriekonzept DC
            p_without_inv.append((time, p_uncertainty))  # in kW in Liste hinterlegen

            row += 1

        # Verrechnung mit Wechselrichter
        for i in range(0, len(p_without_inv)):
            time = p_without_inv[i][0]
            p_wo_inv = p_without_inv[i][1]

            if p_wo_inv == 0:
                p_inverter = 0  # wenn Peingang = 0 kann auch keine Pausgang sein
            else:
                # Berechnung Wechselrichter Ausgangsleistung bei zentralem Wechselrichter Konzept
                p_inverter = (p_wo_inv - inverter_power_own) / (1+k)

            # kann keine negative Leistung der PV Anlage geben
            if p_inverter > 0:
                p_total_time.append((time, p_inverter))  # Liste mit Anlagenleistung in kW pro Zeitschritt
            else:
                p_total_time.append((time, 0))

        # p_total_time [] Liste mit nur PV-Erzeugung, ohne Abzug Standortlast

        locationload_time = []
        # wenn Standortlastprofil hinterlegt ist
        if type(self.locationload) == str:
            locationload = self.locationload
            wb_location = load_workbook(locationload)
            ws_location = wb_location.active

            for i in range(2, ws_location.max_row + 1):
                p = ws_location['B' + str(i)].value
                locationload_time.append(p)

        elif type(self.locationload) == int:
            locationload = self.locationload
            for i in range(0, len(p_total_time)):
                locationload_time.append(locationload)

        elif self.locationload is None:  # wenn Standortlast zu Zeitpunkten nicht bekannt ist
            locationload_time = []
            for i in range(0, len(p_total_time)):
                locationload_time.append(0)

        # Abgleich PV Leistung mit Wechselrichter / Standortlastprofil
        energy_time = []

        for i in range(0, len(locationload_time)):
            day_time = p_total_time[i][0]
            p = p_total_time[i][1] - locationload_time[i]
            energy_time.append((day_time, p))

        # Ergebnis-Excel
        wb_linechart = Workbook()
        ws_linechart = wb_linechart.active
        ws_linechart['A1'] = "Datum und Uhrzeit"
        ws_linechart['B1'] = "Zur Verfügung stehende Leistung in kW"
        ws_linechart['C1'] = "Einstrahlung in W/m^2"
        ws_linechart['D1'] = "Leistung in kW"
        ws_linechart['E1'] = "Standortlast"

        row = 2
        for i in range(0, len(energy_time)):
            time = energy_time[i][0]

            ws_linechart['A' + str(row)] = time
            ws_linechart['B' + str(row)] = energy_time[i][1]
            ws_linechart['C' + str(row)] = radiation[i]
            ws_linechart['D' + str(row)] = p_total_time[i][1]
            ws_linechart['E' + str(row)] = locationload_time[i]

            row += 1

        wb_linechart.save(r'Input/Leistung_' + str(self.name) + '.xlsx')

        # je nachdem ob Batteriespeicher vorliegt und mit welchem Konzept werden Werte weitergegeben
        # DC - Konzept kann noch erweitert werden
        if self.battery == "DC":
            # DC - Leistung der PV Anlage ohne Wechselrichter, ohne Verrechnung mit Standortlast
            return p_without_inv
        else:
            return energy_time
