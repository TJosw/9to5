import math
import csv
import datetime


def lcm(a, b, c=None):

    if c is not None:
        res = abs(int(a) * int(b) * int(c)) // math.gcd(math.gcd(int(a), int(b)), int(c))
    else:
        res = abs(int(a) * int(b)) // math.gcd(int(a), int(b))

    return res


def kgv(a, b):
    m = 1
    while m > 0:
        lcm = m * a
        if lcm % b == 0:
            return lcm
            m = 0
        m = m + 1

def kgv2(zahlen):
    mal = 1
    while True:
        vielfaches = zahlen[0] * mal
        try:
            for zahl in zahlen:
                rest = (vielfaches % zahl)
                if not rest: pass
                else:
                    raise
            break
        except: pass
        mal += 1
    return vielfaches

# brauche ich nicht, weil ich nicht mit csv Dateien arbeite?
'''
def ueberlagern(csv1, csv2):

    csv_list1 = []

    with open(csv1) as csvdatei:  # erste CSV öffnen und als Liste speichern
        csv_reader_object = csv.reader(csvdatei, delimiter=';', lineterminator='\n')

        for row in csv_reader_object:
            if not row:
                continue
            else:
                csv_list1.append(row)

    csv_list2 = []

    with open(csv2) as csvdatei:  # zweite CSV öffnen und als Liste speichern
        csv_reader_object = csv.reader(csvdatei, delimiter=';', lineterminator='\n')

        for row in csv_reader_object:
            if not row:
                continue
            else:
                csv_list2.append(row)

    del csv_list1[0]
    del csv_list2[0]
    csv_list1 += csv_list2
    list_time = []
    list_load = []
    for row in csv_list1:
        list_time.append(datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S'))  # Zeiten als datetime Format
        list_load.append(float(row[1]))




    tuple = list(zip(list_time, list_load))  # Listen zu tuple
    tuple_sort = sorted(tuple, key=lambda time: time[0])  # tuple sortieren
    t1, t2 = zip(*tuple_sort)  # tuple zu listen
    list1 = list(t1)
    list2 = list(t2)

    for i in range(len(list1)-1,-1,-1):  # gleiche Zeiten addieren
        if list1[i] == list1[i-1]:
            list2[i-1] = list2[i]+list2[i-1]
            del list1[i]
            del list2[i]
    tuple_new = list(zip(list1, list2))
    result_csv = [["Zeit", "Ladeleistung"]]
    for row_result in range(0, len(tuple_new)):
        result_csv.append([tuple_new[row_result][0], tuple_new[row_result][1]])

    result_load_csv = open('Ueberlagert.csv', 'w')  # neue CSV schreiben
    with result_load_csv:
        writer = csv.writer(result_load_csv, delimiter=';')
        for row in result_csv:
            writer.writerow(row)


    return
'''

#Winkelberechnungen
# gibt Werte in degree aus
# alpha in degree einsetzen

# sin^3
def sin_3(a):

    value = (1/4) * (3 * math.sin(a * (math.pi / 180)) - math.sin(3 * (a * (math.pi / 180))))
    return value

# cos^2
def cos_2(a):

    value = (1/2) * (1 + math.cos(2 * (a * (math.pi / 180))))
    return value

#cos^3
def cos_3(a):

    value = (1/4) * (3 * math.cos(a * (math.pi / 180)) + math.cos(3 * (a * (math.pi / 180))))
    return value


