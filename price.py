from datetime import datetime
from openpyxl import Workbook
import requests
import json


'''
Die Funktion stellt eine Schnittstelle zu den Strompreisen der Strombörse dar. Sie ruft zu dem betrachteten Zeitraum
die Strompreise ab. Dabei kann entweder ein Teil der Vergangenheit mit einbezogen werden oder nur Werte für eine 
Prognose abgerufen werden. 
Die Preisdaten werden dann in einer Excel-Datei hinterlegt. Diese dient als Input-Datei für die Klasse ECharging.
'''


def forecast_price(h_factor, hours_forecast, hours_past=None):
    timenow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    timenow = datetime.strptime(timenow, "%Y-%m-%d %H:%M:%S")
    timestamp_now = datetime.timestamp(timenow)

    price_list = []

    # Ersten runden Zeitschritt
    for timestamp in range(int(timestamp_now), int(timestamp_now+(3600*h_factor)+1)):
        if timestamp % (3600 * h_factor) == 0:
            timestamp_step = timestamp  # timestamp in 15 min Schritt

    step_end = timestamp_step + (hours_forecast*3600)  # End Timestamp der Betrachtung, nicht mehr in Liste

    # Werte in API nur stündlich angegeben, deswegen auf letzte volle Stunde zurück
    for timestamp in range(int(timestamp_now-3600), int(timestamp_now+1)):
        if timestamp % 3600 == 0:
            timestamp_end = timestamp  # Ende bei Vergangneheitsbetrachtung, Beginn bei Prognose
            break

    # je nachdem ob Vergangenheit betrachtet wird. Werte für API festlegen
    if hours_past is not None:
        step_start = timestamp_step - (hours_past*3600)  # start timestamp in Vergangenheit
        timestamp_start = timestamp_end - (hours_past*3600)  # 6 Stunden in die Vergangneheit werden noch betrachtet
    else:
        step_start = timestamp_step  # erster runder Wert der Prognosebetrachtung

    # Daten für die Vergangenheit abrufen NUR wenn in betrachtetem Zeitraum auch Vergangenheit
    if hours_past is not None:
        url = requests.get("https://api.awattar.de/v1/marketdata?start=" + str(timestamp_start) + "000&end=" +
                           str(timestamp_end) + "000")
        data_complete = json.loads(url.text)
        data_data = data_complete['data']

        for element in range(0, len(data_data)):
            data = data_data[element]
            start_time = data['start_timestamp']
            end_time = data['end_timestamp']
            price = data['marketprice']  # Preis für jeweilige 15 min Werte anhängen an priceList

            for timestep in range(step_start*1000, step_end*1000, int((3600 * h_factor) * 1000)):  # Umrechnug in Millisekunden mit *1000
                if end_time > timestep >= start_time:
                    date = datetime.fromtimestamp(timestep/1000)
                    price_list.append((date, price))

    # Daten für die Prognose, geht tehoretisch für die nächsten 24h laut webside, zeigt aber nur 14 an
    url = requests.get("https://api.awattar.de/v1/marketdata")
    data_complete = json.loads(url.text)
    data_data = data_complete['data']

    for element in range(0, len(data_data)):
        data = data_data[element]
        start_time = data['start_timestamp']
        end_time = data['end_timestamp']
        price = data['marketprice']  # Preis für jeweilige 15 min Werte anhängen an priceList

        for timestep in range(step_start*1000, step_end*1000, int((3600 * h_factor) * 1000)):
            if end_time > timestep >= start_time:
                date = datetime.fromtimestamp(timestep/1000)
                price_list.append((date, price))

    wb_price = Workbook()  # neue Excel-Datei für Ergebnisse
    ws_price = wb_price.active
    ws_price['A1'] = "Datum und Uhrzeit"
    ws_price['B1'] = "Preis in €/MWh"

    i = 0
    for row in range(2, len(price_list)+2):
        ws_price['A' + str(row)] = price_list[i][0]
        ws_price['B' + str(row)] = price_list[i][1]
        i += 1

    wb_price.save(r'Input/VorhersagePreis.xlsx')




