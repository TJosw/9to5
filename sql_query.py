import time
import pyodbc  # Kommunikation SQL Server
from datetime import datetime, timedelta
from openpyxl import Workbook

'''
Die Funktion ruft Daten der Leistung und der Einstrahlung von der PV-Anlage am elenia Institut ab. Dabei handelt es sich
um Vergangenheitsdaten. Die Aufzeichnung der Werte erfolgt pro String. Die Anlage hat 5 Strings und einen 
Einstrahlungssensor. Die Aufzeichnung ist in europäischer Winterzeit mit einem Versatz von 5 Minuten. In Abhängigkeit 
des gewählten Stundenfaktors werden die Daten einzeln und zusammengefasst in Excel-Dateien abgespeichert. Die Datei mit 
der Summe der Leistungen aus allen 5 Strings dient als Input-Datei für die Klasse PVSystem.
'''


def get_past_data(h_factor, hours_past):
    server = 'smartelenia.elenia.ing.tu-bs.de'
    database = 'SmartEleniaDataBase'
    username = 'spspv'
    password = '!spspv1!'
    connection = pyodbc.connect('DRIVER={SQL Server}; SERVER=' + server + '; DATABASE=' + database + '; UID=' + username
                                + '; PWD=' + password + ';')
    cursor = connection.cursor()

    # Funktion zur SQL Abfrage
    def def_query(number, start_time, end_time):
        query = "SELECT * " \
                "FROM Betriebsmittel.Zeitreihe " \
                "WHERE ID_metadata_series = " + number + \
                "AND datum BETWEEN '" + start_time + ":000' AND '" + end_time + ":000'"  # Abfrage definiert
        return query

    # Zeiten für die Abfrage definieren
    timenow = datetime.now().strftime("%Y-%d-%m %H:%M:%S")
    timenow = datetime.strptime(timenow, "%Y-%d-%m %H:%M:%S")  # String zu datetime um Zeitzone rauszufinden

    local_now = timenow.astimezone()
    local_tz = local_now.tzinfo
    local_tzname = local_tz.tzname(local_now)  # aktuelle Zeitzone Sommer-oder Winterzeit

    timenow = datetime.now().strftime("%Y-%d-%m %H:%M:%S")

    # in SQL Datenbank dauerhaft in Winterzeit abgespeichert
    if local_tzname == 'Mitteleuropäische Sommerzeit':
        timenow = datetime.strptime(timenow, "%Y-%d-%m %H:%M:%S") + timedelta(hours=-1)
        timenow = timenow.strftime("%Y-%d-%m %H:%M:%S")  # in Winterzeit und mit 5 Minuten Versatz

    minus_hours = datetime.strptime(timenow, "%Y-%d-%m %H:%M:%S") + timedelta(hours=-hours_past)
    minus_hours = minus_hours.strftime("%Y-%d-%m %H:%M:%S")

    # Zeiten für die jeweilige Abfrage eingeben
    query199 = def_query("199", minus_hours, timenow)  # Einstrahlungssensor hat ne andere Ausrichtung als PV
    query202 = def_query("202", minus_hours, timenow)
    query205 = def_query("205", minus_hours, timenow)
    query208 = def_query("208", minus_hours, timenow)
    query211 = def_query("211", minus_hours, timenow)
    query214 = def_query("214", minus_hours, timenow)

    cursor.execute(query199)  # jeweils die Abfrage pro Sensor durchführen
    data199 = cursor.fetchall()  # jeweils die Abfrage speichern in Variable
    cursor.execute(query202)
    data202 = cursor.fetchall()
    cursor.execute(query205)
    data205 = cursor.fetchall()
    cursor.execute(query208)
    data208 = cursor.fetchall()
    cursor.execute(query211)
    data211 = cursor.fetchall()
    cursor.execute(query214)
    data214 = cursor.fetchall()

    cursor.close()  # Zeiger wieder schließen
    connection.close()  # SQL Verbindnung wieder schließen

    wb_sql = Workbook()  # neues Wb erstellen zum abspeichern der abgerufenen SQL Daten
    ws_sql199 = wb_sql.active
    ws_sql199.title = "Einstrahlungssensor"
    ws_sql202 = wb_sql.create_sheet("Sensor 1")
    ws_sql205 = wb_sql.create_sheet("Sensor 2")
    ws_sql208 = wb_sql.create_sheet("Sensor 3")
    ws_sql211 = wb_sql.create_sheet("Sensor 4")
    ws_sql214 = wb_sql.create_sheet("Sensor 5")

    ws_sql199['A1'] = 'Datum und Uhrzeit'
    ws_sql199['B1'] = 'Einstrahlungswert in W/m2'
    ws_sql199['C1'] = 'Sensornummer'
    ws_sql199['D1'] = 'Datum und Uhrzeit'
    ws_sql199['E1'] = 'sekündl. Leistung'

    row = 2
    for i in range(0, len(data199)):
        ws_sql199['D' + str(row)] = data199[i][0]
        ws_sql199['E' + str(row)] = data199[i][1]
        i += 1
        row += 1

    ws_sql202['A1'] = 'Datum und Uhrzeit'  # Immer in Winterzeit
    ws_sql202['B1'] = 'Leistung'  # durchschnittliche Leistung pro Zeitabschnitt
    ws_sql202['C1'] = 'Sensornummer'

    ws_sql205['A1'] = 'Datum und Uhrzeit'
    ws_sql205['B1'] = 'Leistung'
    ws_sql205['C1'] = 'Sensornummer'

    ws_sql208['A1'] = 'Datum und Uhrzeit'
    ws_sql208['B1'] = 'Leistung'
    ws_sql208['C1'] = 'Sensornummer'

    ws_sql211['A1'] = 'Datum und Uhrzeit'
    ws_sql211['B1'] = 'Leistung'
    ws_sql211['C1'] = 'Sensornummer'

    ws_sql214['A1'] = 'Datum und Uhrzeit'
    ws_sql214['B1'] = 'Leistung'
    ws_sql214['C1'] = 'Sensornummer'

    # string zu datetime, damit timestamp anwendbar
    timenow = datetime.strptime(timenow, "%Y-%d-%m %H:%M:%S")
    minus_hours = datetime.strptime(minus_hours, "%Y-%d-%m %H:%M:%S")

    def build_mean(list, list_sum):
        # Timestamps unabhängig von SQL Liste berechnen
        # dann nach Timestamps in SQL Liste suchen
        timestamp_now = datetime.timestamp(timenow)  # schon in minus 1h,  wegen winterzeit in SQL
        timestamp_past = datetime.timestamp(minus_hours)

        for timestamp in range(int(timestamp_past), int(timestamp_now)):
            if timestamp % (3600*h_factor) == 0:  # erster runder Step
                timestamp_lower_bound = timestamp  # unterster Timestamp für erste untere Grenze festgelegt
                break
            else:
                timestamp += 1

        # timestamp für erste untere Grenze steht fest, nach dieser suchen in Liste
        lower_bound = 0

        # erste untere Grenze festlegen
        # Bereich in der nach Differenz gesucht wird auf 10 Sekunden festgelegt, manchmal zeichnen Sensoren nicht auf
        for diff in range(0, 10):
            for i in range(0, len(list)):
                numb_sensor = list[i][2]  # die jeweilige Nummer des Sensors übertragen
                time = list[i][0]
                timestamp_time = datetime.timestamp(time)

                # nach Zeit von timestamp lower bound suchen, gestartet mit Differenz bei null
                # wenn exakter Wert nicht in Liste vorhanden, Differenz erhöhen
                if int(timestamp_time) == timestamp_lower_bound - diff:
                    lower_bound = list[i][0]
                    index_lower_bound = i
                    break
            if lower_bound == 0:
                diff += 1
            else:
                break

        # erste untere Grenze festgelegt, in Abhängigkeit wenn genauer Wert in Datenbank nicht zu finden ist

        counter = 1
        upper_bound = 0
        # Input Daten: timestamp_lowerBound, lower bound und index lower bound, von der 1. unteren Grenze
        while counter < ((hours_past/h_factor) + 1):
            # timestamp upper_bound in abhängigkeit von timestamp lowerbound festlegen

            for timestamp in range(int(timestamp_lower_bound + 1), int(timestamp_now + (3600*h_factor))):
                if timestamp % (3600*h_factor) == 0:
                    timestamp_upper_bound = timestamp
                    break
                else:
                    timestamp += 1
            # timestamp upperbound steht fest

            # nach upperbound suchen
            for diff in range(0, int((3600*h_factor) + 1)):  # Bereich in der nach Differenz gesucht wird
                for i in range(index_lower_bound, len(list)):
                    numb_sensor = list[i][2]  # die jeweilige Nummer des Sensors übertragen
                    time = list[i][0]
                    timestamp_time = datetime.timestamp(time)

                    # nach Zeit von timestamp upper bound suchen, gestartet mit differenz bei null
                    if int(timestamp_time) == timestamp_upper_bound - diff:
                        upper_bound = list[i][0]
                        index_upper_bound = i
                        break
                # wenn der Wert für upperbound nicht gefunden werden konnte, diff erhöhen
                if upper_bound == 0 or upper_bound == lower_bound:
                    diff += 1
                else:
                    break

            # Den Mittelwert zwischen der unteren und der oberen Grenze berechnen
            if index_upper_bound == index_lower_bound:  # für Grenzfall, dass der letzte Wert genau der 15 min Wert ist
                lenght == 1
            else:
                lenght = index_upper_bound - index_lower_bound

            p = 0
            for i in range(index_lower_bound, index_upper_bound):  # Mittelwert berechnen
                p += list[i][1]

            mean = p / lenght

            # an Liste, die dann Ausgegeben wird, die jeweils untere Grenze plus Mittelwert anhängen

            list_sum.append((lower_bound, mean, numb_sensor))

            lower_bound = upper_bound
            index_lower_bound = index_upper_bound
            timestamp_lower_bound = timestamp_upper_bound
            counter += 1

        return list_sum

    # leere Variablen in denen dann Mittelwerte in Zeitschritten abgespeichert werden
    data199_sum = []
    data202_sum = []
    data205_sum = []
    data208_sum = []
    data211_sum = []
    data214_sum = []

    build_mean(data199, data199_sum)
    build_mean(data202, data202_sum)
    build_mean(data205, data205_sum)
    build_mean(data208, data208_sum)
    build_mean(data211, data211_sum)
    build_mean(data214, data214_sum)

    row = 2
    for i in range(0, len(data199_sum)):
        ws_sql199['A' + str(row)] = data199_sum[i][0]
        ws_sql199['B' + str(row)] = data199_sum[i][1]
        ws_sql199['C' + str(row)] = data199_sum[i][2]
        i += 1
        row += 1

    row = 2
    for i in range(0, len(data202_sum)):
        ws_sql202['A' + str(row)] = data202_sum[i][0]
        ws_sql202['B' + str(row)] = data202_sum[i][1]
        ws_sql202['C' + str(row)] = data202_sum[i][2]
        i += 1
        row += 1

    row = 2
    for i in range(0, len(data205_sum)):
        ws_sql205['A' + str(row)] = data205_sum[i][0]
        ws_sql205['B' + str(row)] = data205_sum[i][1]
        ws_sql205['C' + str(row)] = data205_sum[i][2]
        i += 1
        row += 1

    row = 2
    for i in range(0, len(data208_sum)):
        ws_sql208['A' + str(row)] = data208_sum[i][0]
        ws_sql208['B' + str(row)] = data208_sum[i][1]
        ws_sql208['C' + str(row)] = data208_sum[i][2]
        i += 1
        row += 1

    row = 2
    for i in range(0, len(data211_sum)):
        ws_sql211['A' + str(row)] = data211_sum[i][0]
        ws_sql211['B' + str(row)] = data211_sum[i][1]
        ws_sql211['C' + str(row)] = data211_sum[i][2]
        i += 1
        row += 1

    row = 2
    for i in range(0, len(data214_sum)):
        ws_sql214['A' + str(row)] = data214_sum[i][0]
        ws_sql214['B' + str(row)] = data214_sum[i][1]
        ws_sql214['C' + str(row)] = data214_sum[i][2]
        i += 1
        row += 1

    wb_sql.save(r'Input/DatenPVAnlage_aus_SQL.xlsx')

    wb_sum = Workbook()  # neue Excel-Datei für Summe
    ws_sum = wb_sum.active  # in dem Sheet soll Summe der 5 Sensoren berechnet werden
    ws_sum.title = 'Summe'

    ws_sum['A1'] = 'Datum und Uhrzeit'  # ab hier angepasst an Sommerzeit
    ws_sum['B1'] = 'Summe Leistung in kW'
    ws_sum['C1'] = 'Einstrahlung'

    for i in range(2, int((hours_past/h_factor) + 2)):
        winter_time = ws_sql199['A' + str(i)].value

        if local_tzname == 'Mitteleuropäische Sommerzeit':  # bei Sommerzeit Umrechnung um eine Stunde
            winter_time = datetime.strftime(winter_time, "%Y-%d-%m %H:%M:%S")  # datetime zu str
            summer_time = datetime.strptime(winter_time, "%Y-%d-%m %H:%M:%S") + timedelta(hours=1)
            timestamp_summer_time = datetime.timestamp(summer_time)

            # Aufrunden der Zeiten am Ende wieder auf runden Wert, sonst manchmal Fehler bei Ladealgorithmen
            if timestamp_summer_time % (3600*h_factor) == 0:
                ws_sum['A' + str(i)] = summer_time
            else:
                diff = 1
                for count in range(1, int(3600*h_factor)):
                    timestamp_summer_time = timestamp_summer_time + diff
                    if timestamp_summer_time % (3600*h_factor) == 0:
                        summer_time = datetime.fromtimestamp(timestamp_summer_time)
                        ws_sum['A' + str(i)] = summer_time
                        break

        elif local_tzname == 'Mitteleuropäische Zeit':
            winter_time = datetime.strftime(winter_time, "%Y-%d-%m %H:%M:%S")
            winter_time = datetime.strptime(winter_time, "%Y-%d-%m %H:%M:%S")
            timestamp_winter_time = datetime.timestamp(winter_time)
            if timestamp_winter_time % (3600*h_factor) == 0:
                ws_sum['A' + str(i)] = winter_time
            else:
                diff = 1
                for count in range(1, int(3600 * h_factor)):
                    timestamp_winter_time = timestamp_winter_time + diff
                    if timestamp_winter_time % (3600*h_factor) == 0:
                        winter_time = datetime.fromtimestamp(timestamp_winter_time)
                        ws_sum['A' + str(i)] = winter_time
                        break

        ws_sum['B' + str(i)] = (ws_sql202['B' + str(i)].value + ws_sql205['B' + str(i)].value +
                                ws_sql208['B' + str(i)].value + ws_sql211['B' + str(i)].value +
                                ws_sql214['B' + str(i)].value) / 1000  # Ausgabe in kW

        ws_sum['C' + str(i)] = ws_sql199['B' + str(i)].value

    # Grenzfall für letzten Wert, da SQL datenbank mit 5 Minuten Versatz aufzeichnet
    if ws_sum['B' + str(int((hours_past/h_factor) + 1))].value == 0:
        ws_sum['B' + str(int((hours_past/h_factor) + 1))] = ws_sum['B' + str(int(hours_past/h_factor))].value

    if ws_sum['C' + str(int((hours_past/h_factor) + 1))].value == 0:
        ws_sum['C' + str(int((hours_past/h_factor) + 1))] = ws_sum['C' + str(int(hours_past/h_factor))].value

    wb_sum.save(r'Input/Vergangenheit_PV_Leistung_summe.xlsx')











