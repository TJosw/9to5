
from openpyxl import Workbook, load_workbook
from openpyxl.utils import get_column_letter
from datetime import datetime
from numpy import log as ln
import math
import masterMath

'''
Diese Funktion dient der Valididerung der Einstrahlungs- und Leistungsprognose. Es sind zwei verschiedene Rechnungen 
hinterlegt. Diese werden miteinander verglichen und ausgewertet, welche genauer ist. 

Als für dies relevante Input-Daten werden folgende verwendet 

nameAnlage = "PV Anlage des elenia Instituts"
anstellwinkel = 40  
azimut = 180  # azimut = Ausrichutng, 180 entspicht südausrichtung
anzahl_ausrichtung = 1  
maxLeistungPVModul = 0.275  # kWp/Modul
maxLeistungPV = 14.3  # Angabe in kWp, Größe PV: 14,3 kWp
anzahlModule = 52
alter = 10  # angenommenes Alter

# Wechselrichter, aus QUaschning Buch? 
nennleistung = 17  # kW
wirkungsgrad_nennleistung = 98.2  # in Prozent, muss angegeben werden
wirkungsgrad_eur = 97.8  # in Prozent

aber bei prognosevalidierung hat eigetnlich keine verrechnung mit Standortlast stattgefrunden 
instituiton = "G0"  # Auswahl: H0, G0, G1, G2, G3, G4, G5, G6, L0, L1, L2 (nach den Standardlastprofilen des BDEW)
jahresverbrauch = 30  # Durschnittlicher Jahresverbrauch in MWh

lastprofil_max = 20 

unsicherheitsfaktor = 0.04 # 4 %, durchscnittl. Abweichung bei Leistungsberechnungtest von P peak, aber vllt eher an Auswertung Prognosetest anpassen?
'''


class PVsystemVal:
    # allgemeine Berechnung der Werte einer PVAnlage, mit dem Ziel der Leistungsberechnung
    def __init__(self, pv_angle, pv_azimut, number_orientation, weatherdata, historical_data, p_modul_max,
                 p_max, age, uncertainty,
                 # Wechselrichterdaten
                 inverter_power, efficiency_power, efficiency_eur, efficiency_cec,
                 name='PVAnlage', locationload=None, numbermodules=None, battery=None, efficiency_10power=None, noct=None,
                 temperature_coefficient_u=None, temperture_coefficient_i=None):
        # technische Eigenschaften der PV Anlage
        # iwie mit einarbeiten, dass es auch zwei Ausrichtungen bei manchen PV Anlagen gibt
        self.name = name  # Name der PVAnlage
        self.pv_angle = pv_angle  # Anstellwinkel
        self.pv_azimut = pv_azimut  # Azimut -180 bis 180 Grad, Ausrichtung der PV Anlage
        self.numbermodules = numbermodules  # Anzahl PV Module
        self.p_modul_max = p_modul_max  # Peakleistung Wp eines Moduls
        self.p_max = p_max  # Peakleistung Wp der gesamten Anlage
        self.age = age
        # self.efficiency_factor_modul = efficiency_factor_modul  # brauche ich das? Wirkungsgrad der ANlage?
        self.noct = noct  # Normal operating cell temperature
        self.temperature_coefficient_u = temperature_coefficient_u  # Temperatur Koeffizient für Spannung
        self.temperature_coefficient_i = temperture_coefficient_i  # Temperatur Koeffizeint für Strom
        # Die Art der Module einfließen lassen?

        # Verluste durch Verschaltungsart, Staubbelag - Unsicherheitsfaktor einbauen

        # Unsicherheitsfaktor
        self.uncertainty = uncertainty

        # Daten zum Wechselrichter
        self.inverter_power = inverter_power
        self.efficiency_power = efficiency_power
        self.efficiency_10power = efficiency_10power
        self.efficiency_eur = efficiency_eur
        self.efficiency_cec = efficiency_cec

        self.weatherdata = weatherdata  # für die Berechnung der Leistung benötigte Wetterdaten
        self.locationload = locationload  # Standortlastprofil
        self.historical_data = historical_data  # Vergangenheitsdaten der PV Leistung, der letzen 6 h?

        self.number_orientation = number_orientation
        if number_orientation > 1:
            self.angle1_orientation1 = pv_angle[0]  # Winkeldefinition bei Anlage mit zweifacher Ausrichtung
            self.angle1_orientation2 = pv_angle[1]
            self.angle2_orientation1 = pv_azimut[0]
            self.angle2_orientation2 = pv_azimut[1]

        self.battery = battery  # wenn Batteriespeicher vorhanden  hinterlegen und andere Werte berechnen?

    # Leistung von Modultemperatur und Einstrahlung auf die geneigte Fläche abhängig
    def get_energy(self):

        # Standart Test Condition
        temperature_stc = 25  # Standardmodultemperatur in °Celcius
        solar_radiation_stc = 1000  # in W/m2
        #  AM-WERT? unter STC-Bedingungen?

        # Normal operating cell temperature
        temperature_noct = 20  # in °Celcius
        solar_radiation_noct = 800  # in W/m2

        if self.number_orientation > 1:  # bei doppelter Ausrichtung mit diesen Werten rechnen
            angle1_orientation1 = self.angle1_orientation1
            angle1_orientation2 = self.angle1_orientation2
            angle2_orientation1 = self.angle2_orientation1
            angle2_orientation2 = self.angle2_orientation2
            # Leistungen pro Ausrichtung ausrechnen
            # (einmal für Orientation1 und einmal für Orientation2) und dann am Ende addieren, am Ende einbauen

        if self.p_max == 0:  # wenn p_max nicht angegeben aus p_modul_max berechnen
            p_max = self.p_modul_max * self.numbermodules
        else:
            p_max = self.p_max

        if self.p_modul_max == 0:  # wenn p_modul_max nicht angegeben aus p_max berechnen
            p_modul_max = self.p_max / self.numbermodules
        else:
            p_modul_max = self.p_modul_max

        # Annahmen, wenn technische Daten nicht bekannt
        if self.noct is None:
            noct = 46  # in °Celsius
        else:
            noct = self.noct

        if self.temperature_coefficient_u is None:
            temperature_coefficient_u = -0.003  # -0,3 %
        else:
            temperature_coefficient_u = self.temperature_coefficient_u

        if self.temperature_coefficient_i is None:
            temperature_coefficient_i = 0.0004  # 0,4 %
        else:
            temperature_coefficient_i = self.temperature_coefficient_i

        weatherdata = self.weatherdata
        wb_weather = load_workbook(weatherdata)
        ws_weather = wb_weather.active

        # Berechnung Einstahlung auf geneigte Fläche!!!
        pv_angle = self.pv_angle  # 40
        pv_azimut = self.pv_azimut  # 180 / Südausrichtung

        # umrechnung degree in rad
        pv_angle = pv_angle * (math.pi / 180)
        pv_azimut = pv_azimut * (math.pi / 180)

        # Einfallswinkel in Abhängigkeit der Sonnenposition berechnen
        incidence = []  # leere Liste erstellen für Einfallswinkel der geneigten Fläche
        # nur eine EIntrahlungsberechnung am Ende, jetzt zum Vergleich 2
        radiation_m = []  # leere Liste erstellen für Einstrahlung aus Mertens Buch
        radiation_q = []  # leere Liste erstellen für EInstrahlung aus Quaschniing Buch

        p_total_time_m = []  # neue Liste mit Leistung pro Zeit erstellen
        p_total_time_q = []  # neue Liste mit Leistung pro Zeit erstellen
        p_without_inv_q = []  # Liste mit Leistung pro Zeit ohne WR

        if self.historical_data is not None:
            # als erstes die Leistungsdaten und Einstrahlungsdaten der letzten 6 h an die Liste anhängen
            historical_data = self.historical_data
            wb_historical = load_workbook(historical_data)
            ws_historical = wb_historical.active
            # als Input Daten, historische Daten in Excel Format benötigt, Spalte A Zeit und Spalte B Leistung
            for i in range(2, 26):  # müssen 24 Werte hinterlegt sein in Vergangenheitsdatei für 6h (15min Schritte)
                day_time = ws_historical['A' + str(i)].value
                day_time = day_time.strftime('%Y-%m-%d %H:%M:%S')
                p = ws_historical['B' + str(i)].value
                p_total_time_m.append((day_time, p))
                p_total_time_q.append((day_time, p))
                p_without_inv_q.append((day_time, p))
                e = ws_historical['C' + str(i)].value
                radiation_q.append(e)
                radiation_m.append(e)

        # Einstrahlung auf die geneigte Fläche berechnen
        for row in range(2, 50):
            # Sonnenposition: Sonnenhöhe und Sonnenazimut aus Liste von API ziehen
            # ab zenith größer 90° ist die Sonne untergegangen, keine Berechnung mehr? Grenzfälle betrachten!
            sun_zenith = ws_weather['F' + str(row)].value  # geht von 0 bis 180. 90 bedeutet am Horizont
            sun_h = 90 - sun_zenith  # Winkel der Sonnenhöhe in degree

            # Umwandlung des Schnittstellen Azimut in Berechnung Azimut für Quaschning Rechnung
            # bei Mertens Azimut iwie nicht berücksichtigt
            sun_azimut = ws_weather['G' + str(row)].value  # degree

            if sun_azimut > 0:
                sun_azimut = 360 - sun_azimut
            else:
                sun_azimut = sun_azimut * -1  # wenn wert negativ, einfach Betrag verwenden

            # Umrechnung degree in rad
            sun_h = sun_h * (math.pi / 180)
            sun_azimut = sun_azimut * (math.pi / 180)

            # Berechnung Quaschning
            # Sonnenposition Umwandlung von Kugelkoordinaten in kartesische
            # Einfallswinkel bei geneigter zwischen Vektor in Sonnenrichtung und Flächennormalen
            angle_incidence = math.acos(-math.cos(sun_h) * math.sin(pv_angle) * math.cos(sun_azimut - pv_azimut) +
                                        math.sin(sun_h) * math.cos(pv_angle))
            # Umrechnung rad in deg
            angle_incidence = angle_incidence * (180 / math.pi)  # Einfalsswinkel pro Zeitschritt in degree
            incidence.append(angle_incidence)  # wenn winkel okay, kann das raus

            # Aus API: GHI, DNI, DHI
            ghi = ws_weather['C' + str(row)].value
            dni = ws_weather['D' + str(row)].value
            dhi = ws_weather['E' + str(row)].value

            dir_hor = ghi - dhi  # direkter horizontaler Anteil

            if sun_h > 0:
                # direkter Anteil Strahlung nach Quasching
                radiation_dir_q = dni * (math.cos(angle_incidence * (math.pi / 180)))

                # direkter Anteil Strahlung nach Mertens
                # hier wird der Azimut nicht berücksichtigt
                radiation_dir_m = dir_hor * (math.sin(sun_h + pv_angle) / math.sin(sun_h))
            else:
                # bei negativer Sonnenhöhe ist die Sonne untergegangen, keine Einstrahlung mehr
                radiation_dir_q = 0
                radiation_dir_m = 0

            if ghi > 0:
                # Diffusen Anteil mit Modell von Klucher berechnen
                f = 1 - (dhi / ghi) ** 2
                radiation_diff_q = dhi * (1/2) * (1 + math.cos(pv_angle)) * (1 + f * masterMath.sin_3(pv_angle/2)) * (1 + f * masterMath.cos_2(angle_incidence * (math.pi / 180)) * masterMath.cos_3(sun_h))

                # diffusen Anteil mit isotropem Ansatz berechnen nach Mertens Buch / Rechnung
                radiation_diff_m = dhi * (1 / 2) * (1 + math.cos(pv_angle))
            else:
                radiation_diff_q = 0
                radiation_diff_m = 0

            # Reflektierte Leistung in Abhängigkeit Albedo Wert, Annahme A = 0,2
            # für Albedo Wert je nach Untergrund Werte hinterlegen
            # bei quasching und mertens gleich
            a = 0.2
            radiation_ref = ghi * a * (1/2) * (1 - math.cos(pv_angle))

            # Einstrahlung auf geneigte Fläche als Summe der Bestandteile berechnen und an Liste anhängen

            # Berechnung nach Quaschning
            radiation_pv_q = radiation_dir_q + radiation_diff_q + radiation_ref
            radiation_q.append(radiation_pv_q)

            # Berechnung nach Mertens
            radiation_pv_m = radiation_dir_m + radiation_diff_m + radiation_ref
            radiation_m.append(radiation_pv_m)

        # Wechselrichter "Datenbank" laden
        # Datenbank vollständing hinterlegen plus Bezüge auf Datenbank entsprechend hinterlegen
        wb_data = load_workbook("Datenbank_Wechselrichter_Verluste .xlsx")
        ws_inverter = wb_data['Wechselrichter']

        inverter_power = self.inverter_power    # PN
        efficiency_power = self.efficiency_power  # nN
        efficiency_10power = self.efficiency_10power  # n10N

        # wenn Wirkungsgrad bei 10 % Nennleistung nicht angegeben ist
        # mit Hilfe nEUR oder nCEC aus hinterlegter Datenbank ermitteln
        if self.efficiency_10power is None:

            # (nN & nEUR)
            if self.efficiency_eur is not None:
                efficiency_eur = self.efficiency_eur
                for row in range(3, 12):  # range an Länge Datenbank anpassen
                    if ws_inverter['A' + str(row)].value == round(efficiency_eur):
                        index_row = row
                for i_column in range(3, 11):  # range an Länge Datenbank anpassen
                    column = get_column_letter(i_column)
                    if ws_inverter[str(column) + '1'].value == round(efficiency_power):
                        index_column = column

                efficiency_10power = ws_inverter[str(index_column) + str(index_row)].value

            # (nN & nCEC)
            else:
                efficiency_cec = self.efficiency_cec
                for row in range(43, 52):  # range an Länge Datenbank anpassen
                    if ws_inverter['A' + str(row)].value == round(efficiency_cec):
                        index_row = row
                for i_column in range(3, 11):  # range an Länge Datenbank anpassen
                    column = get_column_letter(i_column)
                    if ws_inverter[str(column) + '41'].value == round(efficiency_power):
                        index_column = column

                efficiency_10power = ws_inverter[str(index_column) + str(index_row)].value

        # Peb und k berechnen mit PN, nN und n10N (einmalig für weitere Berechnungen)
        inverter_power_own = inverter_power/9 * ((1/(efficiency_10power/100)) - (1/(efficiency_power/100)))
        k = 10/(9*(efficiency_power/100)) - 1/(9*(efficiency_10power/100)) - 1

        # Ermittlung Modultemperatur, für die nächsten 12 h, in Abh. von Umgebungstemperatur und Einstrahlung
        temp_modul_time_m = []
        temp_modul_time_q = []
        i = 24
        for row in range(2, 50):
            day_time = ws_weather['A' + str(row)].value
            day_time = datetime.strptime(day_time, '%Y-%m-%d %H:%M:%S')
            temp_amb = ws_weather['B' + str(row)].value
            solar_radiation_q = radiation_q[i]
            solar_radiation_m = radiation_m[i]

            temp_modul_m = temp_amb + (noct - temperature_noct) * (solar_radiation_m / solar_radiation_noct)
            temp_modul_q = temp_amb + (noct - temperature_noct) * (solar_radiation_q / solar_radiation_noct)
            temp_modul_time_m.append((day_time, temp_modul_m))  # Liste mit Modultemperatur in Zeitschritten (15 min)
            temp_modul_time_q.append((day_time, temp_modul_q))
            i += 1

        # Leistungswerte für die nächsten 12 h ermitteln und in Liste p_total_time anhängen
        row = 2
        for i in range(0, len(temp_modul_time_m)):
            time = ws_weather['A' + str(row)].value
            solar_radiation_q = radiation_q[i + 24]
            solar_radiation_m = radiation_m[i + 24]

            if solar_radiation_q == 0:
                p_modul_q = 0  # keine Modulleistung wenn keine Einstrahlung
            else:
                # Ermittlung Modulleistung im Arbeitspunkt in Abhängigkeit der Modultemp. und der Einstrahlung
                p_modul_q = p_modul_max * ((ln(solar_radiation_q)) / (ln(solar_radiation_stc))) * (solar_radiation_q / solar_radiation_stc) \
                        * (1 + temperature_coefficient_u * (temp_modul_time_q[i][1] - temperature_stc)) \
                        * (1 + temperature_coefficient_i * (temp_modul_time_q[i][1] - temperature_stc))

            if solar_radiation_m == 0:
                p_modul_m = 0  # keine Modulleistung wenn keine Einstrahlung
            else:
                # Ermittlung Modulleistung im Arbeitspunkt in Abhängigkeit der Modultemp. und der Einstrahlung
                p_modul_m = p_modul_max * ((ln(solar_radiation_m)) / (ln(solar_radiation_stc))) * (solar_radiation_m / solar_radiation_stc) \
                        * (1 + temperature_coefficient_u * (temp_modul_time_m[i][1] - temperature_stc)) \
                        * (1 + temperature_coefficient_i * (temp_modul_time_m[i][1] - temperature_stc))

            # Wirkungsgrad pro Modul mit berechnen / wirkungsgradverlauf angeben
            # zwischen solar_radiation und P-Modul

            # Anlagenleistung / Eingangsleistung für Wechselrichter
            p_sum_m = p_modul_m * self.numbermodules
            p_sum_q = p_modul_q * self.numbermodules

            # Verlust Werte erstmal hinterlegen
            # Alterungsfaktor
            loss_age = 0.0015  # Annahme 0,15 % pro Jahr Leistungsverlust
            p_sum_m = p_sum_m * ((1-loss_age) ** self.age)
            p_sum_q = p_sum_q * ((1-loss_age) ** self.age)

            # Verluste durch Verschmutzung, nur bei Neigungen die kleiner als 15° sind relevant

            # Verlust durch Verschattung

            # Systemverluste

            # exakten Unsicherheitsfaktor anpassen
            p_uncertainty_m = p_sum_m * (1 - self.uncertainty)
            p_uncertainty_q = p_sum_q * (1 - self.uncertainty)

            # Liste für Leistung ohne Wechselrichter, für Batteriekonzept DC
            p_without_inv_q.append((time, p_uncertainty_q))

            if p_uncertainty_m == 0:
                p_inverter_m = 0  # wenn Peingang = 0 kann auch keine Pausgang sein
            else:
                # Berechnung Wechselrichter Ausgangsleistung bei zentralem Wechselrichter Konzept
                p_inverter_m = (p_uncertainty_m - inverter_power_own) / (1+k)

            if p_uncertainty_q == 0:
                p_inverter_q = 0  # wenn Peingang = 0 kann auch keine Pausgang sein
            else:
                # Berechnung Wechselrichter Ausgangsleistung bei zentralem Wechselrichter Konzept
                p_inverter_q = (p_uncertainty_q - inverter_power_own) / (1+k)

            # jeweils Wirkungsgrad mit berechnen / wirkungsgradverlauf vom Wechselrichter mit angeben
            # Systemwirkungsgrad mit berechnen

            # kann keine negative Leistung der PV Anlage geben
            if p_inverter_q > 0:
                p_total_time_q.append((time, p_inverter_q))  # Liste mit Anlagenleistung pro Zeitschritt
            else:
                p_total_time_q.append((time, 0))
            if p_inverter_m > 0:
                p_total_time_m.append((time, p_inverter_m))  # Liste mit Anlagenleistung pro Zeitschritt
            else:
                p_total_time_m.append((time, 0))

            row += 1

        # Ausgabe von p_total_time = Leistung pro Zeit in dem Zeitabschnitt der letzten 6h plus die nächsten 12 h

        # wenn Standortlastprofil hinterlegt ist, über den Zeitraum -6h + 12h laden
        # wenn mehrere Standortlasprofile hinterlegt werden sollen, Verrechnung anpassen
        # Verbindung checken, inwieweit dies bei Ladealgorithmus mit drin ist
        if self.locationload is not None:
            locationload = self.locationload
            wb_location = load_workbook(locationload)
            ws_location = wb_location.active
            locationload_time = []
            for i in range(2, 74):
                p = ws_location['B' + str(i)].value
                p = p * 1000  # Umrechnung von kW in W
                locationload_time.append(p)
        else:
            locationload_time = []
            for i in range(0, 72):
                locationload_time.append(0)

        # Abgleich PV Leistung / Standortlastprofil
        # Jetzt mit Leistungsberechnung von Mertens verknüpft, anpassen wenn für Leistungsberechnung entschieden
        energy_time = []
        energy_without_inv = []
        for i in range(0, 72):
            day_time = p_total_time_m[i][0]
            p = p_total_time_m[i][1] - locationload_time[i]
            energy_time.append((day_time, p))
            p_without_inv = p_without_inv_q[i][1] - locationload_time[i]
            energy_without_inv.append((day_time, p_without_inv))

        # neue Excel Datei erstellen zum anzeigen und speichern der Ergebniswerte
        wb_linechart = Workbook()
        ws_linechart = wb_linechart.active
        ws_linechart['A1'] = "Datum und Uhrzeit"
        ws_linechart['B1'] = "Zur Verfügung stehende Leistung in W"
        ws_linechart['C1'] = "Einstrahlung Quaschning"
        ws_linechart['D1'] = "Einstrahlung Mertens"
        ws_linechart['E1'] = "Leistung PV Anlage Quaschning"
        ws_linechart['F1'] = "Leistung PV Anlage Mertens"
        ws_linechart['G1'] = "Standortlast"


        # Daten aus p_time_total, location_load_time und energy_time in Worksheet schreiben
        row = 2
        for i in range(0, 72):
            time = energy_time[i][0]
            time = datetime.strptime(time, '%Y-%m-%d %H:%M:%S')
            ws_linechart['A' + str(row)] = time
            ws_linechart['B' + str(row)] = energy_time[i][1]
            ws_linechart['C' + str(row)] = radiation_q[i]
            ws_linechart['D' + str(row)] = radiation_m[i]
            ws_linechart['E' + str(row)] = p_total_time_q[i][1]  # einzelne Leistungen angeben je q und m
            ws_linechart['F' + str(row)] = p_total_time_m[i][1]
            ws_linechart['G' + str(row)] = locationload_time[i]
            row += 1

        # Werte in Grafik anzeigen für p_time_total, locationload_time und energy_time
        # für die Grafik Dimensionen, Skalierung richtig einstellen

        # Anlagenwirkungsgrad berechnen, ergibt sich aus Wirkungsgrad der Module, des Wechselrichters, ...
        # Durchschnitsswirkungsgrad über betrachtete Zeit eingeben
        # Verhältnis zwischen elektrischer Leistung und eingestrahlter Leistung

        wb_linechart.save("Leistung_PV.xlsx")

        # time in datetime Typ bei ergebnislisten
        # je nachdem ob Batteriespeicher vorliegt und mit welcher Kopplung unterschiedliche Werte ausgeben
        if self.battery == None:
            return energy_time
        if self.battery == "AC":
            # Ausgangsleistung des Wechselrichters mit Standortlast verrechnet
            return energy_time
        if self.battery == "DC":
            # DC - Leistung der PV Anlage ohne Wechselrichter, auch mit Standortlast verrechnet
            return energy_without_inv

        # welche Werte muss man return, die mit denen wo anders weitergerechnet werden soll, gehen auch mehrere?
        # energy_time dann in Ladealgorithmus verwenden / einflechten in bestehenden Code

    # Output der Klasse PV-Anlage: Lastprofil, anhand dessen dann Ladestrategie angepasst wird bzw. was als Basis dient
    # plus jeweilige Angabe des Wirkungsgrades /Gesamtwirkungsgrad der Anlage
