import os.path
import json
import datetime
import csv
from openpyxl import load_workbook, Workbook
import masterMath


def create_xlsx_data(c_name, name, pv_name, batt_name, strategy ):
    if pv_name is not None:
        if os.path.isfile(r'Ergebnisse/Loading_%s_%s_%s_%s.xlsx' % (c_name, name, pv_name, strategy)):
            wb_r = load_workbook(
                r'Ergebnisse/Loading_%s_%s_%s_%s.xlsx' % (c_name, name, pv_name, strategy))
        else:
            wb_r = Workbook()
    elif batt_name is not None:
        if os.path.isfile(
                r'Ergebnisse/Loading_%s_%s_%s_%s.xlsx' % (c_name, name, batt_name, strategy)):
            wb_r = load_workbook(
                r'Ergebnisse/Loading_%s_%s_%s_%s.xlsx' % (c_name, name, batt_name, strategy))
        else:
            wb_r = Workbook()
    else:
        if os.path.isfile(r'Ergebnisse/Loading_%s_%s_%s.xlsx' % (c_name, name, strategy)):
            wb_r = load_workbook(r'Ergebnisse/Loading_%s_%s_%s.xlsx' % (c_name, name, strategy))
        else:
            wb_r = Workbook()
    return wb_r


def create_result_sheet(wb_r_uebergabe, max_load_profile):
    wb_r = wb_r_uebergabe
    ws_r = wb_r.create_sheet('Standortleistung %.2f kW' % (max_load_profile))  # Ergebnissheet erstellen

    sheets = wb_r.get_sheet_names()
    if 'Sheet' in sheets:  # Standardsheet löschen
        sheet_by_name = wb_r.get_sheet_by_name('Sheet')
        wb_r.remove_sheet(sheet_by_name)
    # Legende Ergebnisexcel
    ws_r.cell(row=1, column=1, value='Startzeitpunkt')
    ws_r.cell(row=1, column=2, value='Endzeitpunkt')
    ws_r.cell(row=1, column=3, value='Leistung in kW')
    ws_r.cell(row=1, column=4, value='geladene Energie in kWh')
    ws_r.cell(row=1, column=6, value='Preis für Netzbezug in €')
    ws_r.cell(row=1, column=7, value='Preis in € pro MWh')
    ws_r.cell(row=1, column=5, value='SoC')
    ws_r.cell(row=1, column=8, value='Erneuerbare Erzeugung')
    ws_r.cell(row=1, column=9, value='Netzbezug in kWh')
    return ws_r, wb_r


def calculate_comp_factor(h_factor_p, h_factor, h_factor_prices):
    lcm_h = masterMath.kgv(masterMath.kgv(1 / h_factor_p, 1 / h_factor), 1 / h_factor_prices)
    h_factor_new = 1 / lcm_h
    h_comp_p = lcm_h * h_factor_p  # Kompensationsfaktor
    h_comp = lcm_h * h_factor
    h_comp_prices = lcm_h * h_factor_prices
    return h_factor_new, h_comp, h_comp_p, h_comp_prices


def write_results(ws_r, result_tuple_sort, soc_res,i):
    for k in range(0, len(result_tuple_sort)):
        ws_r.cell(row=k + 2, column=1, value=result_tuple_sort[k][0])
        ws_r.cell(row=k + 2, column=2, value=result_tuple_sort[k][1])
        ws_r.cell(row=k + 2, column=3, value=result_tuple_sort[k][2])
        ws_r.cell(row=k + 2, column=4, value=result_tuple_sort[k][3])
        ws_r.cell(row=k + 2, column=5, value=result_tuple_sort[k][4])
        ws_r.cell(row=k + 2, column=6, value=result_tuple_sort[k][5])
        ws_r.cell(row=k + 2, column=7, value=soc_res[k])
        ws_r.cell(row=k + 2, column=8, value=result_tuple_sort[k][6])
        ws_r.cell(row=k + 2, column=9, value=result_tuple_sort[k][7])

    # Einfügen von Filter/Sortierungsoptionen in Ergebnisexcel
    ws_r.auto_filter.ref = ('A1:H' + str(i + 2))
    ws_r.auto_filter.add_sort_condition('A2:A' + str(i + 2))

    return ws_r


def calculate_SOC(bat_cap_load_start, result_tuple_sort, max_bat_cap):
    cap_ges = bat_cap_load_start
    soc_res = []
    # Berechnung des SOC
    for c in range(0, len(result_tuple_sort)):
        cap_ges += result_tuple_sort[c][3]
        soc_ges = cap_ges / max_bat_cap
        soc_res.append(soc_ges)
    return soc_res


def save_xlsx(wb_r, c_name, name, pv_name, batt_name, strategy):
    if pv_name is not None:
        wb_r.save(r'Ergebnisse/Loading_%s_%s_%s_%s.xlsx' % (c_name, name, pv_name, strategy))
    elif batt_name is not None:
        wb_r.save(r'Ergebnisse/Loading_%s_%s_%s_%s.xlsx' % (c_name, name, batt_name, strategy))
    else:
        wb_r.save(r'Ergebnisse/Loading_%s_%s_%s.xlsx' % (c_name, name, strategy))
    return


def rest_werte(sort_tuple, t_d2, t_s_res, i, h_factor_new, t_e_res, load_res, cap_load_res, price_res, load_grid_list, renew_data, price_data, price_h_res, renew):
    while i < len(sort_tuple):
        t_s_res.append(sort_tuple[i][0])
        t_calc_e = sort_tuple[i][0].replace(second=0, microsecond=0, minute=0, hour=sort_tuple[i][0].hour)
        t_calc_diff_e = sort_tuple[i][0] - t_calc_e
        t_h_diff_e = ((t_calc_diff_e.seconds / (
                3600 * h_factor_new)) % 1) * h_factor_new
        t_ende = sort_tuple[i][0] + datetime.timedelta(hours=1 * h_factor_new - t_h_diff_e)
        if t_ende > t_d2:
            t_ende = t_d2
        t_e_res.append(t_ende)
        load_res.append(0)
        cap_load_res.append(0)
        price_res.append(0)
        load_grid_list.append(0)

        if renew_data is not None and price_data is None:
            price_h_res.append(0)
            renew.append(sort_tuple[i][2])
        elif price_data is not None and renew_data is None:
            price_h_res.append(sort_tuple[i][2])
            renew.append(0)
        elif renew_data is not None and price_data is not None:
            price_h_res.append(sort_tuple[i][2])
            renew.append(sort_tuple[i][3])
        else:
            price_h_res.append(0)
            renew.append(0)

        i += 1
    return t_s_res, t_e_res, load_res, cap_load_res, price_res, load_grid_list, price_h_res, renew


class Car:
    def __init__(self, max_bat_cap, consumption, name='EFahrzeug', dist=None, t_arrive=None, t_depart=None,
                 start_bat_cap=None, effectiveness=None, soh=None, max_P=None, Saeulen_ID_Name='Ladesäule', min_P=None,
                 load_mode=None, voltage=None, current=None, saeulen_effectiveness=None ):
        # Maximale Batteriekapazität des Autos in kWh (derzeit Plichtfeld)
        self.max_bat_cap = float(max_bat_cap)
        # Verbrauch des Autos in kWh/100km
        self.consumption = float(consumption)
        self.name = name  # Name des Fahrzeugs
        if dist is None:
            self.dist = dist  # zurückgelegte Distanz in km
        else:
            self.dist = float(dist)
        self.t_a = t_arrive  # Ankunftszeitpunkt an Lademöglichkeit in 'yyyy:mm:dd:hh:mm:ss'
        self.t_d = t_depart  # Abfahrtszeitpunkt von Lademöglichkeit in 'yyyy:mm:dd:hh:mm:ss'
        if start_bat_cap is None:   # Wenn Startkapazität nicht gegeben
            self.start_bat_cap = start_bat_cap
        else:
            self.start_bat_cap = float(start_bat_cap)  # Batteriekapazität vor dem Zurücklegen der Strecke
        if effectiveness is None:
            self.effectiveness = effectiveness
        else:
            self.effectiveness = float(effectiveness)  # Wirkungsgrad
        if soh is None:
            self.soh = soh
        else:
            self.soh = float(soh)     # State of Health Batterie

        if self.start_bat_cap is None:  # Wenn Startkapazität nicht gegeben
            self.start_bat_cap = self.max_bat_cap

        if self.dist is not None:   # Berechnung SoC/Batteriekapazität zum Ankunftszeitpunkt nach dem Zurücklegen der Strecke
            if effectiveness is not None:
                self.bat_cap_arrive = self.start_bat_cap - self.dist * (1 / self.effectiveness) * self.consumption / 100
                self.bat_cap_arrive = round(self.bat_cap_arrive, 2)
            else:
                self.bat_cap_arrive = self.start_bat_cap - self.dist * self.consumption / 100
                self.bat_cap_arrive = round(self.bat_cap_arrive, 2)
            self.soc_arrive = self.bat_cap_arrive / self.max_bat_cap  # SoC
            self.soc_arrive = round(self.soc_arrive, 2)
        else:  # Wenn keine Strecke zurückgelegt wurde
            self.bat_cap_arrive = self.start_bat_cap
            self.soc_arrive = self.bat_cap_arrive / self.max_bat_cap
            self.soc_arrive = round(self.soc_arrive, 2)

        if soh is not None:  # Maximale Batteriekapazität mit SoH einbezogen
            self.max_bat_cap = self.max_bat_cap * self.soh

        if t_arrive and t_depart is not None:
            self.t_a = str(self.t_a)
            self.t_d = str(self.t_d)
            self.t_a = datetime.datetime.strptime(self.t_a, '%Y-%m-%d %H:%M:%S')  # Ankunftszeitpunkt in datetime
            self.t_d = datetime.datetime.strptime(self.t_d, '%Y-%m-%d %H:%M:%S')  # Abfahrtszeitpunkt in Kalendarsekunden
            self.t_diff = (self.t_d - self.t_a) / 3600  # Standzeit des Autos in h --> Lademöglichkeit

# Ladesäulen Parameter
# -------------------------------------------------------------------------------------------------------------
        if max_P is None:
            self.max_P = max_P  # maximale Ladeleistung
        else:
            self.max_P = float(max_P)
        self.Saeulen_ID_Name = Saeulen_ID_Name  # Name der Säule
        if min_P is None:
            self.min_P = min_P  # minimale Ladeleistung
        else:
            self.min_P = float(min_P)
        if voltage is None:
            self.voltage = voltage  # max. Ladespannung
        else:
            self.voltage = float(voltage)
        self.load_mode = load_mode  # Lademodus AC/DC
        if current is None:
            self.current = current  # max. Ladestrom
        else:
            self.current = float(current)
        if saeulen_effectiveness is None:
            self.saeulen_effectiveness = saeulen_effectiveness  # Wirkungsgrad
        else:
            self.saeulen_effectiveness = float(saeulen_effectiveness)

        if voltage and current is not None:  # manuelle Berechnung Ladeleistung
            if load_mode == 'DC':
                self.max_P = voltage * current
            elif load_mode == '1AC':
                self.max_P = voltage * current
            elif load_mode == '3AC':
                self.max_P = 3 * voltage * current

    # def fullName(self):
        # return self.firstName + " " + self.lastName

    def getsoc(self, dist=None, start_bat_cap=None):
        if start_bat_cap is None and self.start_bat_cap in None:
            start_bat_cap = self.max_bat_cap
        if self.start_bat_cap is not None and start_bat_cap is None:
            start_bat_cap = self.start_bat_cap
        if dist is None:
            if self.effectiveness is None:
                bat_cap = start_bat_cap - self.dist * self.consumption / 100
            else:
                bat_cap = start_bat_cap - self.dist * (1 / self.effectiveness) * self.consumption / 100
        else:
            if self.effectiveness is None:
                bat_cap = start_bat_cap - dist * self.consumption / 100
            else:
                bat_cap = start_bat_cap - dist * (1 / self.effectiveness) * self.consumption / 100

        soc = bat_cap / self.max_bat_cap
        return soc

    def getbatcap(self, dist=None, start_bat_cap=None):
        if start_bat_cap is None and self.start_bat_cap in None:
            start_bat_cap = self.max_bat_cap
        if self.start_bat_cap is not None and start_bat_cap is None:
            start_bat_cap = self.start_bat_cap
        if dist is None:
            if self.effectiveness is None:
                bat_cap = start_bat_cap - self.dist * self.consumption / 100
            else:
                bat_cap = start_bat_cap - self.dist * (1 / self.effectiveness) * self.consumption / 100
        else:
            if self.effectiveness is None:
                bat_cap = start_bat_cap - dist * self.consumption / 100
            else:
                bat_cap = start_bat_cap - dist * (1 / self.effectiveness) * self.consumption / 100

        return bat_cap

    def getposloadtime(self):
        self.t_a = str(self.t_a)
        self.t_d = str(self.t_d)
        t_a = datetime.datetime.strptime(self.t_a, '%Y-%m-%d %H:%M:%S')  # Ankunftszeitpunkt in datetime
        t_d = datetime.datetime.strptime(self.t_d, '%Y-%m-%d %H:%M:%S')  # Abfahrtszeitpunkt in datetime
        t_diff = (t_d - t_a)/3600  # Standzeit des Autos in h --> Lademöglichkeit
        return t_a, t_d, t_diff

    def sohcap(self, soh=1):
        max_bat_cap_soh = self.max_bat_cap * soh
        return max_bat_cap_soh


    def combicharging(self, price_data, renew_data, c_name, t_a, t_d, max_bat_cap, soc_load_start, soc_min=1,
                      load_profile_data=None, max_load_profile=10, effectiveness=None, powerfeed_price=7, name=None,
                      pv_name=None, batt_name=None):
        '''Ladealgorithmus in Abhängigkeit von Preis- UND Erzeugungsdaten'''
        #datetime.datetime.strptime(t_a, '%Y-%m-%d %H:%M:%S')
        t_a = str(t_a)
        t_d = str(t_d)
        t_a2 = datetime.datetime.strptime(t_a, '%Y-%m-%d %H:%M:%S') # Ankunftszeitpunkt in datetime
        t_d2 = datetime.datetime.strptime(t_d, '%Y-%m-%d %H:%M:%S') # Abfahrtszeitpunkt in datetime
        t_diff2 = t_d2 - t_a2

        # Falls kein Wirkungsgrad gegeben
        if effectiveness is None:
            effectiveness = 1

        wb_price = load_workbook(price_data)  # lade Exceldatei für Preisdaten
        ws_price = wb_price.active  # lade aktives Worksheet für Preisdaten

        wb_renew = load_workbook(renew_data)  # lade Exceldatei für erneuerbare Erzeugung
        ws_renew = wb_renew.active  # lade aktives worksheet

        # Lade Result-Worksheet, je nachdem, ob Erzeugungsdatei von PV oder Batteriespeicher kommt
        wb_r = create_xlsx_data(c_name, name, pv_name, batt_name, "combicharging")

        ws_r, wb_r = create_result_sheet(wb_r, max_load_profile)

        ws_r.cell(row=1, column=10, value='Wahl')
        ws_r.cell(row=1, column=11, value='price')
        ws_r.cell(row=1, column=12, value='renew')
        ws_r.cell(row=1, column=13, value='renew plus Netzbezug')
        ws_r.cell(row=1, column=14, value='Preis für Einspeisung')
        ws_r.cell(row=1, column=15, value='Netzbezug in kW')

        # Preisdaten
        pos_t_price = []  # mögliche Zeiten
        pos_price = []  # mögliche Preise

        # Erneuerbare Erzeugungsdaten
        pos_t_renew = []
        pos_renew = []

        # Stundenfaktor Preisdaten
        h_factor_price = abs(
            (ws_price.cell(row=3, column=1).value - ws_price.cell(row=4, column=1).value)).seconds / 3600
        # Stundenfaktor Erneuerbare Erzeugung
        h_factor_renew = abs(
            (ws_renew.cell(row=4, column=1).value - ws_renew.cell(row=5, column=1).value)).seconds / 3600

        # Auslesen der erneuerbaren Erzeugung Excel
        for row in range(2, ws_renew.max_row + 1):
            t_row = ws_renew.cell(row=row, column=1).value
            if type(t_row) != datetime.datetime:
                break  # Abbruch wenn keine Zeiten mehr
            if t_a2 - datetime.timedelta(hours=1 * h_factor_renew) < t_row < t_d2:  # Suchen der verfügbaren Ladezeiten
                t_renew = ws_renew.cell(row=row, column=1).value  # Auslesen der Zeiten
                renew = float(ws_renew.cell(row=row, column=2).value)  # Auslesen der erneuerbaren Erzeugung
                pos_t_renew.append(t_renew)
                pos_renew.append(renew)

        time_tuple_renew = list(zip(pos_t_renew, pos_renew))  # Zusammenführen zu tuple zum Sortieren
        time_sort_tuple_renew = sorted(time_tuple_renew, key=lambda time: time[0])  # Sortieren nach Zeit

        tuple1_renew, tuple2_renew = zip(*time_sort_tuple_renew)  # wieder Aufspalten in Listen
        list1_renew = list(
            tuple1_renew)  # sortierte Listen mit Werten der eneuerbaren Erzeugung zu möglichen Ladezeiten
        list2_renew = list(tuple2_renew)

        # Auslesen der Preidaten-Excel
        for row in range(2, ws_price.max_row + 1):
            t_row = ws_price.cell(row=row, column=1).value
            if type(t_row) != datetime.datetime:
                break  # Abbruch wenn keine Zeiten mehr
            if t_a2 - datetime.timedelta(hours=1 * h_factor_price) < t_row < t_d2:  # Suchen der verfügbaren Ladezeiten
                t_price = ws_price.cell(row=row, column=1).value  # Auslesen der Zeiten
                price = float(ws_price.cell(row=row, column=2).value)  # Auslesen der Preise
                pos_t_price.append(t_price)
                pos_price.append(price)

        time_tuple_price = list(zip(pos_t_price, pos_price))  # Zusammenführen zu tuple zum Sortieren
        time_sort_tuple_price = sorted(time_tuple_price, key=lambda time: time[0])  # Sortieren nach Zeit

        tuple1_price, tuple2_price = zip(*time_sort_tuple_price)  # wieder Aufspalten in Listen
        list1_price = list(tuple1_price)  # sortierte Listen mit Wertden der Preise zu möglichen Ladezeiten
        list2_price = list(tuple2_price)  # Liste Strompreise

        # Überprüfung des Load_profiles Attributes
        if load_profile_data is None:  # bei keiner Angabe Ladeleistung immer maximale Säulenleistung

            # wenn keine Daten über die Standortlast zur Verfügung stehen, maximal Ladeleistung der Säule genommen
            if max_load_profile <= self.max_P:
                p_load = max_load_profile
            else:
                p_load = self.max_P

            # Summe von zum Laden verfügbarer erneuerbarer Erzeugung im Flexibilitätszeitraum
            renew_sum = 0
            renew_cap_sum = 0
            for t_combi in range(0, len(list2_renew)):
                renew = list2_renew[t_combi]
                if renew > self.min_P:  # wenn renew größer als min. Ladeleistung ist Summe zählen
                    renew_sum = renew_sum + renew
                    renew_cap = renew * h_factor_renew
                    renew_cap_sum = renew_cap_sum + renew_cap  # Summe der zur Verfügung stehenden Kapazität durch renew

            bat_cap_load_start = max_bat_cap * soc_load_start  # Kapazität des Autos zu Ladebeginn
            bat_cap_diff = max_bat_cap * soc_min - bat_cap_load_start  # Zu ladende Kapazität

            # Erwarteter benötigter Netzbezug in dem Flexibilitätszeitraum
            grid_p = bat_cap_diff - renew_cap_sum  # in kWh

            # Pro Zeitschritt Ladeleistung ermitteln, in Abhängigkeit von verschiedenen Möglichkeiten
            # Jeden Zeitschritt in Felxibilitätszeitraum durchgehen

            load_grid = []  # Netzbezug pro Zeitschritt

            price1_list = []  # Preise nur nach price
            price2_list = []  # Preise für renew: 0 oder price
            price3_list = []  # Preis für Netzbezug, renew plus Netzbezug, wenn renew negativ = price
            price_back_list = []  # Einspeisung
            price_time_list = []

            for t_combi in range(0, len(list1_renew)):
                renew = list2_renew[t_combi]  # erneuerbare Erzeugung zu dem Zeitpunkt
                price = list2_price[t_combi]  # Strombörse Preis zu dem Zeitpunkt
                t_load_p_h = list1_renew[t_combi]

                price_time_list.append(t_load_p_h)

                # mögliche Ladeleistung = maximale Ladesäulenlast

                choice_1 = "price"  # priorisiert wie in pricecharging

                price_1 = p_load * h_factor_price * price / 1000

                choice_2 = "renew"  # priorisiert wie in renewcharging
                if renew > self.min_P:
                    price_2 = 0  # renew kostet theoretische Einspeisevergütung - weiterer Ansatz
                else:
                    # Wenn renew nicht ausreicht, nach price ausrechnen
                    price_2 = price_1

                # Netzbezug ermitteln pro Zeitschritt der benötigt wird um Ladeleistung zu decken
                choice_3 = "renew plus Netzbezug"  # berechnet preis renew nutzen und rest beziehen
                # renew nutzen plus rest aus netz beziehen
                if renew > 0:
                    if max_load_profile <= self.max_P:
                        if max_load_profile + renew <= self.max_P:
                            renew_diff = p_load
                        else:
                            renew_diff = self.max_P - renew
                    else:
                        renew_diff = p_load - renew
                    price_3 = renew_diff * h_factor_price * price / 1000
                    load_grid.append(renew_diff)  # restlichen Netzbezug anhängen
                else:
                    # Wenn renew negativ ist, nach price
                    price_3 = price_1
                    load_grid.append(p_load)  # Netzbezug

                # Einspeisung mit Berücksichtigen, Annahme 7 ct, mögl. Erweiterung?
                choice_4 = "Einspeisen"  # renew einspeisen
                feed_price = powerfeed_price
                # renew Einspeisen
                if renew > 0:
                    price_back = renew * h_factor_renew * feed_price / 100  # Einspeisung
                else:
                    price_back = 0
                price_back_list.append(price_back)  # Liste mit theoretischer Einspeisevergütung

                price1_list.append(price_1)
                price2_list.append(price_2)
                price3_list.append(price_3)  # Liste mit preis für Netzbezug

                # nach Börsenpreis sortieren
                # Liste für Netzbezug und Preis für netzbezug mitnehmen plus renew
            time_tuple_combi = list(zip(price_time_list, list2_renew, load_grid, list2_price, price3_list))
            time_sort_tuple_combi = sorted(time_tuple_combi, key=lambda price3: price3[3])

            # Auswahl nach combicharging
            combi_prices = []  # nur den niedrigsten Preis mit Möglichkeit anhängen
            combi_choices = []

            t_load_p = []  # Liste für Zeit
            load_p = []

            # in Reihenfolge nach minimalem Börsenpreis durchgehen bis erwarteter Netzbezug voll ist
            # pro Zeitschritt combi_choice, combi_preis, Ladeleistung und Zeit notieren
            for k in range(0, len(time_sort_tuple_combi)):
                time_combi = time_sort_tuple_combi[k][0]
                t_load_p.append(time_combi)

                renew = time_sort_tuple_combi[k][1]  # Überschüssige erneuerbare Erzeugung
                load_grid = time_sort_tuple_combi[k][
                    2]  # Netzbezug pro Zeitpunkt in kW, wenn renew kleiner 0 = pricecharging
                exchange_price = time_sort_tuple_combi[k][3]  # Strombörsenpreis
                price = time_sort_tuple_combi[k][4]  # Preis für Netzbezug, wenn renew kleiner 0 = pricecharging

                cap_load = load_grid * h_factor_price  # Kapazität die mit Netzbezug geladen werden kann

                # Netzbezug
                if grid_p > 0:
                    # Erwarteten Netzbezug decken in der sortierten Liste nach minimalem Netzbezug
                    if cap_load < grid_p:
                        # wenn noch Netzbezug übrig ist entweder renew plus Netzbezug oder nur preis
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                            grid_p = grid_p - cap_load  # Kapazität die noch benötigt wird, die noch übrig bleibt
                        else:
                            # renew plus netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)
                            load_p.append(renew + load_grid)
                            grid_p = grid_p - cap_load  # Kapazität die noch benötigt wird, die noch übrig bleibt
                    # letzter Schritt Netzbezug abdecken
                    elif grid_p < cap_load:
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                            grid_p = 0  # dann noch benötigte Kapazität aus netzbezug auf Null setzen
                        else:
                            # renew plus Netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)  # Preis für Netzbezug
                            load_p.append(renew + load_grid)
                            grid_p = 0  # dann noch benötigte Kapazität aus netzbezug auf Null setzen

                # Wenn erwarteter Netzbezug gedeckt ist entweder nur renew oder Preis
                else:
                    if renew > self.min_P:
                        # renew
                        combi_choices.append(choice_2)
                        combi_prices.append(0)
                        load_p.append(renew)
                    else:
                        if renew <= 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                        else:
                            # renew plus Netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)
                            load_p.append(renew + load_grid)

            # nach der zeit wieder umsortieren und aufsplitten
            time_tuple_combi = list(zip(t_load_p, load_p, combi_prices, combi_choices))
            time_sort_tuple_combi = sorted(time_tuple_combi, key=lambda t_combi: t_combi[0])

            tuple_t, tuple_load, tuple_combi_prices, tuple_combi_coices = zip(*time_sort_tuple_combi)
            list_p2_new = list(tuple_load)  # Ladeleistung pro Zeitschritt
            combi_prices = list(tuple_combi_prices)  # minimalster Preis pro Zeitschritt, nach der Kombi ausgesucht
            combi_choices = list(tuple_combi_coices)

            ''' 1. Variante 
            # Flexibilitätszeitraum zum Laden des Elektroautos wird betrachtet
            for t_combi in range(0, len(list1_price)):

                renew = list2_renew[t_combi]  # erneuerbare Erzeugung zu dem Zeitpunkt
                price = list2_price[t_combi]  # Strombörse Preis zu dem Zeitpunkt

                if powerfeed_price is not None:
                    feed_price = powerfeed_price  # Betrag für Einspeisung aus Input Daten
                else:
                    feed_price = 7  # ct/ kWh, Annahme wenn nicht vorhanden, noch final bestimmen und aufnehmen EEG!!

                # 4 MÖGLCHKEITEN; die minimalste davon aussuchen

                choice_1 = "price"  # priorisiert wie in pricecharging
                choice_2 = "renew"  # priorisiert wie in renewcharging
                choice_3 = "renew plus Netzbezug"  # berechnet preis renew nutzen und rest beziehen, prio danach
                choice_4 = "Einspeisen und Netzbezug"  # renew einspeisen und Ladelesitung aus Netz beziehen

                # die jeweiligen Preise für die jeweiligen Möglichkeiten errechnen

                # Ladeleistung unabhängig von renew, Bewertung nur nach Preis
                price_1 = p_load * h_factor_price * price / 1000

                # renew Ladeleistung mal 0 €, wenn renew nicht ausreicht, volle ladeleistung mal Preis
                if p_load > renew > self.min_P:
                    price_2 = 0
                else:
                    # Wenn renew nicht ausreicht, nach price ausrechnen, ergibt das Sinn?
                    price_2 = price_1

                # renew nutzen plus rest aus netz beziehen
                if renew > 0:
                    renew_diff = p_load - renew
                    price_3 = renew_diff * h_factor_price * price / 1000
                else:
                    # Wenn renew negativ ist, nach price wählen, ergibt Sinn?
                    price_3 = price_1

                # renew Einspeisen und volle am standort mögliche Ladeleistung mal Preis
                if renew > 0:
                    price_back = renew * h_factor_renew * feed_price / 100  # Einspeisung
                    price_load = p_load * h_factor_price * price / 1000
                    price_4 = price_load - price_back
                else:
                    price_4 = price_1

                # wenn keine erneuerbare Erzeugung da ist, nur nach preis orientieren

                choices = [price_1, price_2, price_3, price_4]
                min_choice = min(choices)  # den kleinsten Preis aussuchen
                combi_prices.append(min_choice)  # Liste nach der Zeitpunkte zum Laden sortiert werden

                # je nachdem welche Möglichkeit gewählt wird, die Ladeleistung anpassen
                if min_choice == price_1:
                    combi_choices.append(choice_1)
                    p_load = p_load  # volle mögl. Ladeleistung bei Möglichkeit 1

                elif min_choice == price_2:
                    combi_choices.append(choice_2)

                    # Anpassung Ladeleistung an renew nur bei choice 2
                    # Anpassung Ladeleistung an erneuerbare Erzeugung aber nur wenn diese Positiv ist !

                    # wenn load_p_h größer als renew data zu dem gleichen zeitpunkt, renew als Ladeleistung festlegen
                    if p_load > renew > 0:
                        # ABER nur wenn renew größer/gleich als minimale Ladesäulenleistung
                        if renew >= self.min_P:
                            p_load = renew
                    # wenn nicht mit load_p_h weiter machen
                    else:
                        p_load = p_load

                elif min_choice == price_3:
                    combi_choices.append(choice_3)
                    p_load = renew + renew_diff  # volle mögl. Ladeleistung bei Möglichkeit 3, zusam. gesetzt aus renew plus Rest

                else:
                    combi_choices.append(choice_4)
                    p_load = p_load  # volle mögl. Ladeleistung aus Netzbezug

                # Liste für Ladeleistung pro Zeitabschnitt mit Werten auffüllen
                list_p2_new.append(p_load)
            '''

            # kleinste gemeinsame Zeitvielfache für renewdaten und Preisdaten
            lcm_h = masterMath.kgv(1 / h_factor_price, 1 / h_factor_renew)
            h_factor_new = 1 / lcm_h  # Kompensationsfaktor
            h_comp_price = lcm_h * h_factor_price
            h_comp_renew = lcm_h * h_factor_renew

            list1_renew_new = []
            list2_renew_new = []
            list1_price_new = []
            list2_price_new = []

            # Anpassen der zeitanhängigen Größen anhand Kompensationsfaktoren
            for t_price in range(0, len(list1_price)):
                for h_f in range(0, int(h_comp_price)):  # Anpassen der Preisdaten an Leistungsdaten
                    l1_new = list1_price[t_price] + datetime.timedelta(hours=h_f * h_factor_new)
                    list1_price_new.append(l1_new)
                    l2_new = list2_price[t_price]
                    list2_price_new.append(l2_new)

            for t_renew in range(0, len(list1_renew)):
                for h_f_renew in range(0, int(h_comp_renew)):  # Anpassen der Preisdaten an Leistungsdaten
                    l1_renew_new = list1_renew[t_renew] + datetime.timedelta(hours=h_f_renew * h_factor_new)
                    list1_renew_new.append(l1_renew_new)
                    l2_renew_new = list2_renew[t_renew]
                    list2_renew_new.append(l2_renew_new)

            # Anpassen der Listen
            for i in range(len(list1_price_new) - 1, -1, -1):
                if list1_price_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list1_price_new[i]
                    del list2_price_new[i]

            for i in range(len(list1_renew_new) - 1, -1, -1):
                if list1_renew_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list1_renew_new[i]
                    del list2_renew_new[i]

            if t_a2 - list1_price_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list1_price_new[0] = t_a2

            if t_a2 - list1_renew_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list1_renew_new[0] = t_a2

            time_sort_tuple_2 = list(zip(list1_price_new, list2_price_new, list_p2_new, list2_renew_new, combi_prices))
            sort_tuple = sorted(time_sort_tuple_2, key=lambda price: price[4])  # Sortieren nach niedrigster Möglichkeit

        # nur einen Wert für die Standortlast angeben
        elif type(load_profile_data) == int:  # konstante maximale Leistung < maximale Ladesäulenleiste

            # Summe von zum Laden verfügbarer erneuerbarer Erzeugung im Flexibilitätszeitraum
            renew_sum = 0
            renew_cap_sum = 0
            for t_combi in range(0, len(list2_renew)):
                renew = list2_renew[t_combi]
                if renew > self.min_P:  # wenn renew größer als min. Ladeleistung ist Summe zählen
                    renew_sum = renew_sum + renew
                    renew_cap = renew * h_factor_renew
                    renew_cap_sum = renew_cap_sum + renew_cap  # Summe der zur Verfügung stehenden Kapazität durch renew

            bat_cap_load_start = max_bat_cap * soc_load_start  # Kapazität des Autos zu Ladebeginn
            bat_cap_diff = max_bat_cap * soc_min - bat_cap_load_start  # Zu ladende Kapazität

            # Erwarteter benötigter Netzbezug in dem Flexibilitätszeitraum
            grid_p = bat_cap_diff - renew_cap_sum  # in kWh

            # Pro Zeitschritt Ladeleistung ermitteln, in Abhängigkeit von verschiedenen Möglichkeiten
            # Jeden Zeitschritt in Felxibilitätszeitraum durchgehen

            load_grid = []  # Netzbezug pro Zeitschritt

            price1_list = []  # Preise nur nach price
            price2_list = []  # Preise für renew: 0 oder price
            price3_list = []  # Preis für Netzbezug, renew plus Netzbezug, wenn renew negativ = price
            price_back_list = []  # Einspeisung
            price_time_list = []

            for t_combi in range(0, len(list1_renew)):
                renew = list2_renew[t_combi]  # erneuerbare Erzeugung zu dem Zeitpunkt
                price = list2_price[t_combi]  # Strombörse Preis zu dem Zeitpunkt
                t_load_p_h = list1_renew[t_combi]

                price_time_list.append(t_load_p_h)

                # Mögliche Ladeleistung pro Zeitschritt ermitteln
                if max_load_profile + renew < self.max_P:
                    p_load = max_load_profile + renew
                    # plus überschüssiges renew
                else:
                    p_load = self.max_P

                if self.max_P > p_load >= self.min_P:  # maximale Ladeleistung ist maximale Ladesäulenleiste
                    # wenn kleiner als max. Ladesäulenleistung, nur mit vorhandener Leistung laden
                    p_load = p_load
                elif p_load < self.min_P:
                    p_load = 0
                else:
                    # maximale Ladeleistung wird genutzt
                    p_load = self.max_P

                choice_1 = "price"  # priorisiert wie in pricecharging

                # mehr als max. Standortlast kann nicht bezogen werden
                if p_load <= max_load_profile:
                    price_1 = p_load * h_factor_price * price / 1000
                else:
                    price_1 = max_load_profile * h_factor_price * price / 1000

                choice_2 = "renew"  # priorisiert wie in renewcharging
                if renew > self.min_P:
                    price_2 = 0  # renew kostet theoretische Einspeisevergütung - weiterer Ansatz
                else:
                    # Wenn renew nicht ausreicht, nach price ausrechnen
                    price_2 = price_1

                # Netzbezug ermitteln pro Zeitschritt der benötigt wird um Ladeleistung zu decken
                choice_3 = "renew plus Netzbezug"  # berechnet preis renew nutzen und rest beziehen
                # renew nutzen plus rest aus netz beziehen
                if renew > 0:
                    renew_diff = p_load - renew
                    price_3 = renew_diff * h_factor_price * price / 1000
                    load_grid.append(renew_diff)  # restlichen Netzbezug anhängen
                else:
                    # Wenn renew negativ ist, nach price
                    price_3 = price_1
                    load_grid.append(p_load)  # Netzbezug

                # Einspeisung mit Berücksichtigen, Annahme 7 ct, mögl. Erweiterung?
                choice_4 = "Einspeisen"  # renew einspeisen
                feed_price = powerfeed_price
                # renew Einspeisen
                if renew > 0:
                    price_back = renew * h_factor_renew * feed_price / 100  # Einspeisung
                else:
                    price_back = 0
                price_back_list.append(price_back)  # Liste mit theoretischer Einspeisevergütung

                price1_list.append(price_1)
                price2_list.append(price_2)
                price3_list.append(price_3)  # Liste mit preis für Netzbezug

            # nach Börsenpreis sortieren
            # Liste für Netzbezug und Preis für netzbezug mitnehmen plus renew
            time_tuple_combi = list(zip(price_time_list, list2_renew, load_grid, list2_price, price3_list))
            time_sort_tuple_combi = sorted(time_tuple_combi, key=lambda price3: price3[3])

            # Auswahl nach combicharging
            combi_prices = []  # nur den niedrigsten Preis mit Möglichkeit anhängen
            combi_choices = []

            t_load_p = []  # Liste für Zeit
            load_p = []

            # in Reihenfolge nach minimalem Börsenpreis durchgehen bis erwarteter Netzbezug voll ist
            # pro Zeitschritt combi_choice, combi_preis, Ladeleistung und Zeit notieren
            for k in range(0, len(time_sort_tuple_combi)):
                time_combi = time_sort_tuple_combi[k][0]
                t_load_p.append(time_combi)

                renew = time_sort_tuple_combi[k][1]  # Überschüssige erneuerbare Erzeugung
                load_grid = time_sort_tuple_combi[k][
                    2]  # Netzbezug pro Zeitpunkt in kW, wenn renew kleiner 0 = pricecharging
                exchange_price = time_sort_tuple_combi[k][3]  # Strombörsenpreis
                price = time_sort_tuple_combi[k][4]  # Preis für Netzbezug, wenn renew kleiner 0 = pricecharging

                cap_load = load_grid * h_factor_price  # Kapazität die mit Netzbezug geladen werden kann

                # Netzbezug
                if grid_p > 0:
                    # Erwarteten Netzbezug decken in der sortierten Liste nach minimalem Netzbezug
                    if cap_load < grid_p:
                        # wenn noch Netzbezug übrig ist entweder renew plus Netzbezug oder nur preis
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                            grid_p = grid_p - cap_load  # Kapazität die noch benötigt wird, die noch übrig bleibt
                        else:
                            # renew plus netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)
                            load_p.append(renew + load_grid)
                            grid_p = grid_p - cap_load  # Kapazität die noch benötigt wird, die noch übrig bleibt
                    # letzter Schritt Netzbezug abdecken
                    elif grid_p < cap_load:
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                            grid_p = 0  # dann noch benötigte Kapazität aus netzbezug auf Null setzen
                        else:
                            # renew plus Netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)  # Preis für Netzbezug
                            load_p.append(renew + load_grid)
                            grid_p = 0  # dann noch benötigte Kapazität aus netzbezug auf Null setzen

                # Wenn erwarteter Netzbezug gedeckt ist entweder nur renew oder Preis
                else:
                    if renew > self.min_P:
                        # renew
                        combi_choices.append(choice_2)
                        combi_prices.append(0)
                        load_p.append(renew)
                    else:
                        if renew <= 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                        else:
                            # renew plus Netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)
                            load_p.append(renew + load_grid)

            # nach der zeit wieder umsortieren und aufsplitten
            time_tuple_combi = list(zip(t_load_p, load_p, combi_prices, combi_choices))
            time_sort_tuple_combi = sorted(time_tuple_combi, key=lambda t_combi: t_combi[0])

            tuple_t, tuple_load, tuple_combi_prices, tuple_combi_coices = zip(*time_sort_tuple_combi)
            list_p2_new = list(tuple_load)  # Ladeleistung pro Zeitschritt
            combi_prices = list(tuple_combi_prices)  # minimalster Preis pro Zeitschritt, nach der Kombi ausgesucht
            combi_choices = list(tuple_combi_coices)

            ''' 1. Variante 
            # Flexibilitätszeitraum zum Laden des Elektroautos wird betrachtet
            for t_combi in range(0, len(list1_price)):

                renew = list2_renew[t_combi]  # erneuerbare Erzeugung zu dem Zeitpunkt
                price = list2_price[t_combi]  # Strombörse Preis zu dem Zeitpunkt

                if powerfeed_price is not None:
                    feed_price = powerfeed_price  # Betrag für Einspeisung aus Input Daten
                else:
                    feed_price = 7  # ct/ kWh, Annahme wenn nicht vorhanden, noch final bestimmen und aufnehmen EEG!!

                # 4 MÖGLCHKEITEN; die minimalste davon aussuchen

                choice_1 = "price"  # priorisiert wie in pricecharging
                choice_2 = "renew"  # priorisiert wie in renewcharging
                choice_3 = "renew plus Netzbezug"  # berechnet preis renew nutzen und rest beziehen, prio danach
                choice_4 = "Einspeisen und Netzbezug"  # renew einspeisen und Ladelesitung aus Netz beziehen

                # die jeweiligen Preise für die jeweiligen Möglichkeiten errechnen

                # Ladeleistung unabhängig von renew, Bewertung nur nach Preis
                price_1 = p_load * h_factor_price * price / 1000

                # renew Ladeleistung mal 0 €, wenn renew nicht ausreicht, volle ladeleistung mal Preis
                # auch wenn renew größer als p_load prei 0
                if renew > self.min_P:
                    price_2 = 0  # alternative Einspeisung
                else:
                    # Wenn renew nicht ausreicht, nach price ausrechnen, ergibt das Sinn?
                    # wenn renw kleiner als min. Ladeleistung, kann das auch genutzt werden, abzehen von Preis
                    price_2 = price_1

                # Choice 2 und 3 zusammenführen?

                # renew nutzen plus rest aus netz beziehen
                if renew > 0:
                    renew_diff = p_load - renew
                    price_3 = renew_diff * h_factor_price * price / 1000
                else:
                    # Wenn renew negativ ist, nach price wählen, ergibt Sinn?
                    price_3 = price_1

                # renew Einspeisen und volle am standort mögliche Ladeleistung mal Preis
                # gleichzeitig einspeisen und netzbezug geht nicht
                # eher "theoretischer" Vergleich, was mich EInspeisung gekostet hätte

                if renew > 0:
                    price_back = renew * h_factor_renew * feed_price / 100  # Einspeisung
                    price_load = p_load * h_factor_price * price / 1000
                    price_4 = price_load - price_back
                else:
                    price_4 = price_1

                # wenn keine erneuerbare Erzeugung da ist, nur nach preis orientiert

                choices = [price_1, price_2, price_3, price_4]
                min_choice = min(choices)  # den kleinsten Preis aussuchen
                combi_prices.append(min_choice)  # Liste nach der Zeitpunkte zum Laden sortiert werden

                # je nachdem welche Möglichkeit gewählt wird, die Ladeleistung anpassen
                if min_choice == price_1:
                    combi_choices.append(choice_1)
                    p_load = p_load  # volle mögl. Ladeleistung bei Möglichkeit 1

                elif min_choice == price_2:
                    combi_choices.append(choice_2)

                    # Anpassung Ladeleistung an renew nur bei choice 2
                    # Anpassung Ladeleistung an erneuerbare Erzeugung aber nur wenn diese Positiv ist !

                    # wenn load_p_h größer als renew data zu dem gleichen zeitpunkt, renew als Ladeleistung festlegen
                    if p_load > renew > 0:
                        # ABER nur wenn renew größer / gleich als minimale Ladesäulenleistung
                        if renew >= self.min_P:
                            p_load = renew
                    # wenn nicht mit load_p_h weiter machen
                    else:
                        p_load = p_load

                elif min_choice == price_3:
                    combi_choices.append(choice_3)
                    p_load = renew + renew_diff  # volle mögl. Ladeleistung bei Möglichkeit 3, zusam. gesetzt aus renew plus Rest

                else:
                    combi_choices.append(choice_4)
                    p_load = p_load  # volle mögl. Ladeleistung aus Netzbezug

                # Liste für Ladeleistung pro Zeitabschnitt mit Werten auffüllen
                list_p2_new.append(p_load)
            '''

            # kleinste gemeinsame Zeitvielfache für Lastprofil und Preisdaten
            lcm_h = masterMath.kgv(1 / h_factor_price, 1 / h_factor_renew)
            h_factor_new = 1 / lcm_h  # Kompensationsfaktor
            h_comp = lcm_h * h_factor_price
            h_comp_renew = lcm_h * h_factor_renew

            list1_renew_new = []
            list2_renew_new = []
            list1_price_new = []
            list2_price_new = []

            # Anpassen der zeitanhängigen Größen anhand Kompensationsfaktoren
            for t in range(0, len(list1_price)):
                for h_f in range(0, int(h_comp)):  # Anpassen der Preisdaten an Leistungsdaten
                    l1_new = list1_price[t] + datetime.timedelta(hours=h_f * h_factor_new)
                    list1_price_new.append(l1_new)
                    l2_new = list2_price[t]
                    list2_price_new.append(l2_new)

            for t_renew in range(0, len(list1_renew)):
                for h_f_renew in range(0, int(h_comp_renew)):  # Anpassen der Preisdaten an Leistungsdaten
                    l1_renew_new = list1_renew[t_renew] + datetime.timedelta(hours=h_f_renew * h_factor_new)
                    list1_renew_new.append(l1_renew_new)
                    l2_renew_new = list2_renew[t_renew]
                    list2_renew_new.append(l2_renew_new)

            # Anpassen der Listen
            for i in range(len(list1_price_new) - 1, -1, -1):
                if list1_price_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list1_price_new[i]
                    del list2_price_new[i]

            for i in range(len(list1_renew_new) - 1, -1, -1):
                if list1_renew_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list1_renew_new[i]
                    del list2_renew_new[i]

            if t_a2 - list1_price_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list1_price_new[0] = t_a2

            if t_a2 - list1_renew_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list1_renew_new[0] = t_a2

            time_sort_tuple_2 = list(zip(list1_price_new, list2_price_new, list_p2_new, list2_renew_new, combi_prices))
            sort_tuple = sorted(time_sort_tuple_2, key=lambda price: price[4])  # Sortieren nach Preis

        # Excel Tabelle für Standortlast hinterlegt
        elif type(load_profile_data) == str:  # zweite Excel-Tabelle als Input für Standardlastprofil
            wb_p = load_workbook(load_profile_data)  # Laden der Excel
            ws_p = wb_p.active  # Laden des Worksheets
            t_load_p = []  # mögliche Zeiten
            load_p_actual = []

            # Standortlast-Datei auslesen und für ladezeitraum in listen abspeichern
            for row2 in range(2, ws_p.max_row + 1):  # Auslesen der gesamten Excel
                t_row_p = ws_p.cell(row=row2, column=1).value
                if type(t_row_p) != datetime.datetime:
                    break

                # Stundenfaktor für Standortlastprofil
                h_factor_p = abs((ws_p.cell(row=3, column=1).value - ws_p.cell(row=4, column=1).value)).seconds / 3600

                # Flexibilitätszeitraum zum Laden betrachten
                if t_a2 - datetime.timedelta(hours=1 * h_factor_p) < t_row_p < t_d2:
                    t_load_p_h = ws_p.cell(row=row2, column=1).value  # Auslesen der Zeiten
                    load_actual = ws_p.cell(row=row2, column=2).value  # momentane Standortlast

                    # Listen für Flexibilitätszeitraum mit Standortlastdaten
                    t_load_p.append(t_load_p_h)
                    load_p_actual.append(load_actual)  # Liste mit momentaner Standortlast pro Zeitschritt

            # Summe von zum Laden verfügbarer erneuerbarer Erzeugung im Flexibilitätszeitraum
            renew_sum = 0
            renew_cap_sum = 0
            for t_combi in range(0, len(list2_renew)):
                renew = list2_renew[t_combi]
                if renew > self.min_P:  # wenn renew größer als min. Ladeleistung ist Summe zählen
                    renew_sum = renew_sum + renew
                    renew_cap = renew * h_factor_renew
                    renew_cap_sum = renew_cap_sum + renew_cap  # Summe der zur Verfügung stehenden Kapazität durch renew

            bat_cap_load_start = max_bat_cap * soc_load_start  # Kapazität des Autos zu Ladebeginn
            bat_cap_diff = max_bat_cap * soc_min - bat_cap_load_start  # Zu ladende Kapazität

            # Erwarteterr benötigter Netzbezug in dem Flexibilitätszeitraum
            grid_p = bat_cap_diff - renew_cap_sum  # in kWh

            # Pro Zeitschritt Ladeleistung ermitteln, in Abhängigkeit von verschiedenen Möglichkeiten
            # Jeden Zeitschritt in Felxibilitätszeitraum durchgehen

            load_grid = []  # Netzbezug pro Zeitschritt

            price1_list = []  # Preise nur nach price
            price2_list = []  # Preise für renew: 0 oder price
            price3_list = []  # Preis für Netzbezug, renew plus Netzbezug, wenn renew negativ = price
            price_back_list = []  # Einspeisung
            price_time_list = []

            for t_combi in range(0, len(list1_renew)):
                renew = list2_renew[t_combi]  # erneuerbare Erzeugung zu dem Zeitpunkt
                price = list2_price[t_combi]  # Strombörse Preis zu dem Zeitpunkt
                load_actual = load_p_actual[t_combi]  # Standortlast zu dem Zeitpunkt
                t_load_p_h = t_load_p[t_combi]

                price_time_list.append(t_load_p_h)

                # mögliche Ladeleistung in Abhängigkeit von max. Standortlast + PV-Erzeugung - momentane Stdandortlast
                load_p_h = max_load_profile + renew

                # Ladeleistung einschränken in minimale und maximale Ladeleistungen der Ladesäule
                if self.max_P > load_p_h >= self.min_P:  # maximale Ladeleistung ist maximale Ladesäulenleiste
                    # wenn kleiner als max. Ladesäulenleistung, nur mit vorhandener Leistung laden
                    load_p_h = load_p_h
                elif load_p_h < self.min_P:
                    load_p_h = 0
                else:
                    # maximale Ladeleistung wird genutzt
                    load_p_h = self.max_P
                # Max. Mögl. Ladeleistung in Abh. Standortlast, PV-Erz. + max. Standortlast und max. Säuleleistung fest

                # Ladeleistung unabhängig von renew, Bewertung nur nach Preis
                load_p_h_wo_renew = max_load_profile - load_actual
                # Ladeleistung einschränken in minimale und maximale Ladeleistungen der Ladesäule
                if self.max_P > load_p_h_wo_renew >= self.min_P:
                    load_p_h_wo_renew = load_p_h_wo_renew
                elif load_p_h_wo_renew < self.min_P:
                    load_p_h_wo_renew = 0
                else:
                    load_p_h_wo_renew = self.max_P

                choice_1 = "price"  # priorisiert wie in pricecharging
                price_1 = load_p_h_wo_renew * h_factor_price * price / 1000

                choice_2 = "renew"  # priorisiert wie in renewcharging
                if load_p_h > renew > self.min_P:
                    price_2 = 0  # renew kostet theoretische Einspeisevergütung - weiterer Ansatz
                else:
                    # Wenn renew nicht ausreicht, nach price ausrechnen
                    price_2 = price_1

                # Netzbezug ermitteln pro Zeitschritt der benötigt wird um Ladeleistung zu decken
                choice_3 = "renew plus Netzbezug"  # berechnet preis renew nutzen und rest beziehen
                # renew nutzen plus rest aus netz beziehen
                if renew > 0:
                    renew_diff = load_p_h - renew
                    price_3 = renew_diff * h_factor_price * price / 1000
                    load_grid.append(renew_diff)  # restlichen Netzbezug anhängen
                else:
                    # Wenn renew negativ ist, nach price
                    price_3 = price_1
                    load_grid.append(load_p_h)  # Netzbezug

                # Einspeisung mit Berücksichtigen, Annahme 7 ct, mögl. Erweiterung?
                choice_4 = "Einspeisen"  # renew einspeisen
                feed_price = powerfeed_price
                # renew Einspeisen
                if renew > 0:
                    price_back = renew * h_factor_renew * feed_price / 100  # Einspeisung
                else:
                    price_back = 0
                price_back_list.append(price_back)  # Liste mit theoretischer Einspeisevergütung

                price1_list.append(price_1)
                price2_list.append(price_2)
                price3_list.append(price_3)  # Liste mit preis für Netzbezug

            # nach Börsenpreis sortieren
            # Liste für Netzbezug und Preis für netzbezug mitnehmen plus renew
            time_tuple_combi = list(zip(price_time_list, list2_renew, load_grid, list2_price, price3_list))
            time_sort_tuple_combi = sorted(time_tuple_combi, key=lambda price3: price3[3])

            t_load_p = []  # Liste mit Zeiten für ladeleistung wieder neu leer
            load_p = []  # Leere Liste für Ladeleistung pro Zeitschritt, für Algorithmus am Ende

            combi_prices = []  # nur den niedrigsten Preis mit Möglichkeit anhängen, Sortierung Ladealgorithmus
            combi_choices = []

            # in Reihenfolge nach minimalem Börsenpreis durchgehen bis erwarteter Netzbezug voll ist
            # pro Zeitschritt combi_choice, combi_preis, Ladeleistung und Zeit notieren
            for k in range(0, len(time_sort_tuple_combi)):
                time_combi = time_sort_tuple_combi[k][0]
                t_load_p.append(time_combi)

                renew = time_sort_tuple_combi[k][1]  # Überschüssige erneuerbare Erzeugung
                load_grid = time_sort_tuple_combi[k][
                    2]  # Netzbezug pro Zeitpunkt in kW, wenn renew kleiner 0 = pricecharging
                exchange_price = time_sort_tuple_combi[k][3]  # Strombörsenpreis
                price = time_sort_tuple_combi[k][4]  # Preis für Netzbezug, wenn renew kleiner 0 = pricecharging

                cap_load = load_grid * h_factor_p  # Kapazität die mit Netzbezug geladen werden kann

                # Netzbezug
                if grid_p > 0:
                    # Erwarteten Netzbezug decken in der sortierten Liste nach minimalem Netzbezug
                    if cap_load < grid_p:
                        # wenn noch Netzbezug übrig ist entweder renew plus Netzbezug oder nur preis
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                            grid_p = grid_p - cap_load  # Kapazität die noch benötigt wird, die noch übrig bleibt
                        else:
                            # renew plus netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)
                            load_p.append(renew + load_grid)
                            grid_p = grid_p - cap_load  # Kapazität die noch benötigt wird, die noch übrig bleibt
                    # letzter Schritt Netzbezug abdecken
                    elif grid_p < cap_load:
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                            grid_p = 0  # dann noch benötigte Kapazität aus netzbezug auf Null setzen
                        else:
                            # renew plus Netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)  # Preis für Netzbezug
                            load_p.append(renew + load_grid)
                            grid_p = 0  # dann noch benötigte Kapazität aus netzbezug auf Null setzen

                # Wenn erwarteter Netzbezug gedeckt ist entweder nur renew oder Preis
                else:
                    if renew > self.min_P:
                        # renew
                        combi_choices.append(choice_2)
                        combi_prices.append(0)
                        load_p.append(renew)
                    else:
                        if renew < 0:
                            # price
                            combi_choices.append(choice_1)
                            combi_prices.append(price)
                            load_p.append(load_grid)
                        else:
                            # renew plus Netzbezug
                            combi_choices.append(choice_3)
                            combi_prices.append(price)
                            load_p.append(renew + load_grid)

                ''' 
                # Erster Ansatz: Sortierung Pro Zeitschritt nach minimalem Preis 
                # Hierbei nicht berücksichtigt, wenn zu Zeiten von erneuerbarer Erzeugung auch niedirge Stromrpreise sind
                # Deswegen anderen Ansatz probiert

                choices = [price_1, price_2, price_3]
                min_choice = min(choices)  # den kleinsten Preis aussuchen
                combi_prices.append(min_choice)  # Liste nach der Zeitpunkte zum Laden sortiert werden

                # je nachdem welche Möglichkeit gewählt wird, die Ladeleistung anpassen
                if min_choice == price_1:

                    load_p_h = load_p_h  # volle mögl. Ladeleistung bei Möglichkeit 1

                elif min_choice == price_2:
                    combi_choices.append(choice_2)

                    # Anpassung Ladeleistung an renew nur bei choice 2
                    # Anpassung Ladeleistung an erneuerbare Erzeugung aber nur wenn diese Positiv ist !
                    # bei renew auch so einbauen und bei load = None  und Load = int auch einbauen
                    for t in range(0, len(pos_t_renew)):
                        time_renew = pos_t_renew[t]
                        if time_renew == t_load_p_h:
                            renew = pos_renew[t]
                            # wenn load_p_h größer als renew data zu dem gleichen zeitpunkt, renew als Ladeleistung festlegen
                            if load_p_h > renew > 0:
                                # ABER nur wenn renew größer/gleich als minimale Ladesäulenleistung
                                # Eig. hier überflüssig, weil nur die Möglichkeit ausgewählt wird wenn größer als min.
                                if renew >= self.min_P:
                                    load_p_h = renew
                                # wenn nicht mit load_p_h weiter machen
                                else:
                                    load_p_h = load_p_h

                elif min_choice == price_3:
                    combi_choices.append(choice_3)
                    load_p_h = renew + renew_diff  # volle mögl. Ladeleistung bei Möglichkeit 3, zusam. gesetzt aus renew plus Rest

                else:
                    # Einspeisung renew
                    combi_choices.append(choice_4)
                    load_p_h = 0

                # Ladeleistung pro Zeitschritt an Listen für Ladevorgang anhängen
                load_p.append(load_p_h)
            '''

            # kleinste gemeinsame Zeitvielfache für Lastprofil, Preisdaten und renew daten
            lcm_h = masterMath.kgv(masterMath.kgv(1 / h_factor_p, 1 / h_factor_price), 1 / h_factor_renew)
            h_factor_new = 1 / lcm_h  # müsste 1/4 ergeben
            h_comp_p = lcm_h * h_factor_p  # Kompensationsfaktor
            h_comp_price = lcm_h * h_factor_price
            h_comp_renew = lcm_h * h_factor_renew

            # Listen für Ladealgorithmus wieder in richtige Zeitreihenfolge sortieren
            time_tuple_p = list(zip(t_load_p, load_p, combi_prices, combi_choices))
            time_sort_tuple_p = sorted(time_tuple_p, key=lambda time_p: time_p[0])  # nach Zeiten sortieren

            tuple_p1, tuple_p2, tuple_combi_prices, tuple_combi_coices = zip(*time_sort_tuple_p)
            list_p1 = list(tuple_p1)  # Zeit Ladeleistung
            list_p2 = list(tuple_p2)  # Ladeleistung
            combi_prices = list(tuple_combi_prices)  # minimalster Preis pro Zeitschritt, nach der Kombi ausgesucht
            combi_choices = list(tuple_combi_coices)

            list1_renew_new = []
            list2_renew_new = []
            list_p1_new = []
            list_p2_new = []
            list1_price_new = []
            list2_price_new = []

            # Anpassen der zeitanhängigen Größen anhand Kompensationsfaktoren
            # auch mit den beiden "neuen" listen notwendig?
            for t_p in range(0, len(list_p1)):
                for h_f_p in range(0, int(h_comp_p)):  # Anpassen der Leistungsdaten an Preisdaten
                    p1_new = list_p1[t_p] + datetime.timedelta(hours=h_f_p * h_factor_new)
                    list_p1_new.append(p1_new)
                    p2_new = list_p2[t_p]
                    list_p2_new.append(p2_new)
            for t_price in range(0, len(list1_price)):
                for h_f in range(0, int(h_comp_price)):  # Anpassen der Preisdaten an Leistungsdaten
                    l1_new = list1_price[t_price] + datetime.timedelta(hours=h_f * h_factor_new)
                    list1_price_new.append(l1_new)
                    l2_new = list2_price[t_price]
                    list2_price_new.append(l2_new)
            for t_renew in range(0, len(list1_renew)):
                for h_f_renew in range(0, int(h_comp_renew)):  # Anpassen der Preisdaten an Leistungsdaten
                    l1_renew_new = list1_renew[t_renew] + datetime.timedelta(hours=h_f_renew * h_factor_new)
                    list1_renew_new.append(l1_renew_new)
                    l2_renew_new = list2_renew[t_renew]
                    list2_renew_new.append(l2_renew_new)

            # Anpassen der Listen, was genau wird angepasst?
            for i in range(len(list1_price_new) - 1, -1, -1):
                if list1_price_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list1_price_new[i]
                    del list2_price_new[i]

            for i in range(len(list_p1_new) - 1, -1, -1):
                if list_p1_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list_p1_new[i]
                    del list_p2_new[i]

            for i in range(len(list1_renew_new) - 1, -1, -1):
                if list1_renew_new[i] < t_a2 - datetime.timedelta(hours=h_factor_new):
                    del list1_renew_new[i]
                    del list2_renew_new[i]

            if t_a2 - list1_price_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list1_price_new[0] = t_a2

            if t_a2 - list_p1_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list_p1_new[0] = t_a2

            if t_a2 - list1_renew_new[0] == datetime.timedelta(hours=0):
                None
            else:
                list1_renew_new[0] = t_a2

            time_sort_tuple_2 = list(zip(list1_renew_new, list2_price_new, list_p2_new, list2_renew_new, combi_prices))

            # mit sort-tuple wird in Ladevorgang gestartet
            # Sort_tuple hier nach anderer priorisierung sortieren, in Abhängigkeit von preis UND renew
            # Sortierung nach minimalem combi_preis der vorher ermittelt wurde, Priorisierung der Ladezeitschritte
            sort_tuple = sorted(time_sort_tuple_2, key=lambda price: price[4])  # nach min. combi_price

        # Ladevorgang Beginn
        bat_cap_load_start = max_bat_cap * soc_load_start  # Kapazität zu Ladebeginn
        bat_cap_diff = max_bat_cap * soc_min - bat_cap_load_start  # Zu ladende Kapazität *soc_thresh

        # Erstellung Ergebniswert Listen
        t_s_res = []
        t_e_res = []
        load_res = []
        cap_load_res = []
        price_res = []
        price_h_res = []
        soc_res = []
        renew = []
        load_grid_list = []  # Liste für Netzbezug

        i = 0
        price_ges = 0
        cap_load_ges = 0
        t_load_ges = datetime.timedelta(hours=0)
        while cap_load_ges <= bat_cap_diff and i < len(sort_tuple):  # Laden bis Sollwert der Batterie erreicht
            t_start_load = sort_tuple[i][0]
            t_calc = t_start_load.replace(second=0, microsecond=0, minute=0, hour=t_start_load.hour)
            t_calc_diff = t_start_load - t_calc  # Zeit bis zum nächstgrößeren Zeitschritt
            t_h_diff = ((t_calc_diff.seconds / (
                    3600 * h_factor_new)) % 1) * h_factor_new  # Berechnung der Ladezeiten in den einzelnen Zeitschritten
            t_end_load = t_start_load + datetime.timedelta(hours=1 * h_factor_new - t_h_diff)
            if t_end_load > t_d2:
                t_end_load = t_d2
            t_load = t_end_load - t_start_load  # geladene Zeit

            cap_load = (sort_tuple[i][2] * t_load.seconds / 3600) * effectiveness  # geladene Energie/Batteriekapazität
            cap_load_ges += cap_load
            if cap_load_ges > bat_cap_diff:  # Falls maximale Sollkapazität des Autos überschritten
                cap_load_ges -= cap_load
                break
            t_load_ges += t_load  # Gesamtladezeit

            # Preis nur für Netzbezug angeben
            if sort_tuple[i][3] > 0:
                load_grid = sort_tuple[i][2] - sort_tuple[i][3]  # Leistung kW die aus Netz benötigt wird
                cap_load_grid = load_grid * h_factor_renew  # Netzbezug in kWh
                price_h = cap_load_grid * sort_tuple[i][1] / 1000
            else:
                cap_load_grid = cap_load
                price_h = sort_tuple[i][1] * cap_load / 1000

            price_ges += price_h
            print('Geladen wurden %.2f kWh mit einer Leistung von %.2f kW von %s bis %s für %.4f €.' % (
                cap_load, sort_tuple[i][2], sort_tuple[i][0], t_end_load, price_h))  # Ausgabe der Daten in Zeitschritt

            t_s_res.append(t_start_load)
            t_e_res.append(t_end_load)
            cap_load_res.append(cap_load)
            price_res.append(price_h)
            price_h_res.append(sort_tuple[i][1])
            load_res.append(sort_tuple[i][2])
            renew.append(sort_tuple[i][3])
            load_grid_list.append(cap_load_grid)

            i += 1

        # Wenn Sollwert noch nicht erreicht, letzter theoretischer Ladeschritt
        if round(cap_load_ges, 5) != bat_cap_diff and i < len(sort_tuple):
            cap_load_rest = bat_cap_diff - cap_load_ges
            t_rest = cap_load_rest / (sort_tuple[i][2] * effectiveness)  # restliche benötigte Zeit
            t_start_load = sort_tuple[i][0]  # Ladestartzeitpunkt des letzten Elements
            t_end_load = t_start_load + datetime.timedelta(hours=t_rest)  # Ladeendzeitpunkt
            if t_end_load > t_d2:  # Einschränkung falls Abfahrtszeitpunkt erreicht werden würde
                t_end_load = t_d2
            t_load = t_end_load - t_start_load  # Ladezeit
            t_load_ges += t_load  # Gesamtladezeit
            cap_load = (sort_tuple[i][2] * t_load.seconds / 3600) * effectiveness  # geladene Energie
            cap_load_ges += cap_load  # gesamte Energie

            # Preis nur für Netzbezug angeben
            if sort_tuple[i][3] > 0:
                load_grid = sort_tuple[i][2] - sort_tuple[i][3]  # Leistung kW die aus Netz benötigt wird
                cap_load_grid = load_grid * h_factor_renew  # Netzbezug in kWh
                price_h = cap_load_grid * sort_tuple[i][1] / 1000
            else:
                cap_load_grid = cap_load
                price_h = sort_tuple[i][1] * cap_load / 1000

            price_ges += price_h  # Gesamtpreis
            print('Geladen wurden %.2f kWh mit einer Leistung von %.2f kW von %s bis %s für %.4f €.' % (
                cap_load, sort_tuple[i][2], sort_tuple[i][0], t_end_load, price_h))  # Ausgabe der Daten

            t_s_res.append(t_start_load)
            t_e_res.append(t_end_load)
            cap_load_res.append(cap_load)
            price_res.append(price_h)
            price_h_res.append(sort_tuple[i][1])
            load_res.append(sort_tuple[i][2])
            renew.append(sort_tuple[i][3])
            load_grid_list.append(cap_load_grid)

            i += 1

        # Werte für restliche Zeitschritte im Flexibilitätszeitraum
        t_s_res, t_e_res, load_res, cap_load_res, price_res, load_grid_list, price_h_res, renew = \
            rest_werte(sort_tuple, t_d2, t_s_res, i, h_factor_new, t_e_res, load_res, cap_load_res, price_res,
                       load_grid_list, renew_data, price_data, price_h_res, renew)

        print('Insgesamt wurden %.2f kWh für %.2f € in einem Zeitraum über %s Stunden geladen.' % (
        cap_load_ges, price_ges, t_diff2))

        result_tuple = list(
            zip(t_s_res, t_e_res, load_res, cap_load_res, price_res, price_h_res, renew, load_grid_list))
        result_tuple_sort = sorted(result_tuple, key=lambda t_s_res: t_s_res[0])  # Sortieren nach Zeit

        soc_res = calculate_SOC(bat_cap_load_start, result_tuple_sort, max_bat_cap)

        # Schreibe Ergebnis Excel
        for k in range(0, len(result_tuple_sort)):
            ws_r.cell(row=k + 2, column=10, value=combi_choices[k])  # Möglichkeit die priorisiert wird
            ws_r.cell(row=k + 2, column=11, value=price1_list[k])
            ws_r.cell(row=k + 2, column=12, value=price2_list[k])
            ws_r.cell(row=k + 2, column=13, value=price3_list[k])
            ws_r.cell(row=k + 2, column=14, value=price_back_list[k])
            ws_r.cell(row=k + 2, column=15, value=result_tuple_sort[k][7])

        ws_r = write_results(ws_r, result_tuple_sort, soc_res, i)

        # Speichern der Ergebnisexcel
        save_xlsx(wb_r, c_name, name, pv_name, batt_name, "combicharging")





"""
# Aktuelles Fahrzeug zur cars_Daten.JSON hinzufügen
dicData['A'] = cars.__dict__
dicData['B'] = charge.__dict__
Car.updateCars(dicData)
# Car.updateCars(dicData)
"""



"""
# cars = getCars()    # Bekommt alle Daten von der JSON File
def getJsonInformationCars(cars):
    for x in cars:
        # print(cars[x]['max_bat_cap'])
        autobatterie_kapazitaet = cars[x]['max_bat_cap']
        auto_verbrauch = cars[x]['consumption']
        autoname = cars[x]['name']
        auto_gefahrene_strecke = cars[x]['dist']
        ankunftszeit = cars[x]['t_arrive']
        abfahrtszeit = cars[x]['t_depart']
        start_bat_cap = cars[x]['start_bat_cap']
        effectiveness = cars[x]['effectiveness']
        soh = cars[x]['soh']

        cars[x] = Car(autobatterie_kapazitaet, auto_verbrauch, autoname, auto_gefahrene_strecke, ankunftszeit, abfahrtszeit, start_bat_cap, effectiveness, soh)

        # als __dic__ damit für uns lesbar und in JSON abspeichern!
        # cars[x] = Car(autobatterie_kapazitaet, auto_verbrauch, autoname, auto_gefahrene_strecke, ankunftszeit, abfahrtszeit, start_bat_cap, effectiveness, soh).__dict__     # Neues Auto hinzufügen
        # updateCars(cars)    # Bearbeitet die JSON Datei und ändert alle bearbeiteten Werte und fügt neue hinzu

        # hier wird gerade nur das erste Auto übergeben!!!!! #TODO FIXEN
        return cars[x]
"""

# Fügt ein neues Auto der JSON datei hinzu
# cars['A'] = Car(autobatterie_kapazitaet, auto_verbrauch, autoname, auto_gefahrene_strecke, ankunftszeit, abfahrtszeit, start_bat_cap, effectiveness, soh).__dict__     # Neues Auto hinzufügen
# updateCars(cars)
# auto1 = Car(autobatterie_kapazitaet, auto_verbrauch, autoname, auto_gefahrene_strecke, ankunftszeit, abfahrtszeit, start_bat_cap, effectiveness, soh)
# print(type(autos['ECrafter']))
# print(autos['ECrafter']['max_bat_cap'])  # Ausgabe von bestimmten Werten
# del autos['ECrafter']       # Entfernt max_ komplett
# print(autos)
"""
cars = {}
carDict = {}
for id in cars:
    carDict[id] = cars[id].__dict__     # Konvertiert cars in eine für Json nutzbare Form
# print(carDict)
updateCars(carDict)     # Schreibt JSON File
"""
# cars = getCars()
# cars = json.dumps(cars)     # Wandelt dictionary in JSON um
# print(cars)