package _to5

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/EAAZE/energy-management/internal/model"
	"github.com/EAAZE/energy-management/internal/service/strategy"
	"github.com/EAAZE/energy-management/pkg"
	"io"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"reflect"
	"strconv"
	"time"

	"io/ioutil"
)

type to5 struct {
	RfidKey                   interface{} `json:"RfidKey"`
	Distance                  string      `json:"Distance"`
	AutoVerbrauch             string      `json:"AutoVerbrauch"`
	DriverType                string      `json:"DriverType"`
	DriverParams              string      `json:"DriverParams"`
	ParentDeviceId            *string     `json:"ParentDeviceId"`
	Name                      string      `json:"Name"`
	Description               string      `json:"Description"`
	MaxCurrentSinglePhase     *float64    `json:"MaxCurrentSinglePhase"`
	MaxCurrentPhase1          *float64    `json:"MaxCurrentPhase1"`
	MaxCurrentPhase2          *float64    `json:"MaxCurrentPhase2"`
	MaxCurrentPhase3          *float64    `json:"MaxCurrentPhase3"`
	MinCurrentSinglePhase     *float64    `json:"MinCurrentSinglePhase"`
	MinCurrentPhase1          *float64    `json:"MinCurrentPhase1"`
	MinCurrentPhase2          *float64    `json:"MinCurrentPhase2"`
	MinCurrentPhase3          *float64    `json:"MinCurrentPhase3"`
	AvailableCurrentSteps     []float64   `json:"AvailableCurrentSteps"`
	GreenEnergy               bool        `json:"GreenEnergy"`
	BatteryCapacity           *float64    `json:"BatteryCapacity"`
	BatteryCapacityLowerLimit *float64    `json:"BatteryCapacityLowerLimit"`
	IsMainMeter               bool        `json:"IsMainMeter"`
	IgnoreState               bool        `json:"IgnoreState"`
	PhaseMappingL1            pkg.Phase   `json:"L1"`
	PhaseMappingL2            pkg.Phase   `json:"L2"`
	PhaseMappingL3            pkg.Phase   `json:"L3"`
	IsControllable            bool        `json:"IsControllable"`
	DistributeCurrent         bool        `json:"DistributeCurrent"`
}

//TODO "FÜR später" erzeugt Pythondaten aus der JSON auslesen, wenn neuer Plan notwendig
func (e *to5) Execute(graph model.DeviceGraph, gridCurrent *float64, params json.RawMessage) (map[string]model.TargetDeviceState, error) {

	//Test aus evcc um restliche Ladedauer zu bestimmen
	//whRemaining := 77.0
	//chargePower := 7.0
	//Hallo := time.Duration(float64(time.Hour) * whRemaining / chargePower).Round(time.Second)
	//println(Hallo)

	currentTime := time.Now()
	current_time := currentTime.Format("2006.01.02 15:04:05")
	use_python_code := false

	fInfo, err := os.Stat("measure_time/Go_time.csv")
	if err != nil {
		log.Fatal(err)
	}
	fsize := fInfo.Size() // Datei Größe auslesen

	if fsize == 0 { // Wenn Datei größe gleich NULL (nix in der Datei steht) aktuelle Zeit schrieben

		fileName := "measure_time/Go_time.csv"
		data := []byte(current_time)
		err := ioutil.WriteFile(fileName, data, 0644)
		if err != nil {
			log.Fatal(err)
		}
	} else { // Wenn etwas in der Datei steht, auslesen welche Uhrzeit genau und mit "jetziger" Uhrzeit abgleichen

		content, err := ioutil.ReadFile("measure_time/Go_time.csv")
		if err != nil {
			log.Fatal(err)
		}
		row1 := string(content)
		current_time_time, err1 := time.Parse("2006.01.02 15:04:05", current_time)
		if err1 != nil {
			log.Fatal(err1)
		}
		start_date, err2 := time.Parse("2006.01.02 15:04:05", row1)
		if err2 != nil {
			log.Fatal(err2)
		}
		differ_time := current_time_time.Sub(start_date)

		if differ_time >= 5*time.Minute { // Wenn unterschied der jetzigen und Uhrzeit in Datei größer als 5min
			use_python_code = true
		}
	}

	if use_python_code == true { // Neue "jetzige" Uhrzeit in Datei schreiben

		fileName := "measure_time/Go_time.csv"
		data := []byte(current_time)
		err := ioutil.WriteFile(fileName, data, 0644)
		if err != nil {
			log.Fatal(err)
		}
	}

	if gridCurrent == nil {
		return nil, fmt.Errorf("strategy requires available grid current")
	}

	// 1. Collect "ALL" data from the graph
	data, err := strategy.GetGraphData(&graph)
	fmt.Println(data)
	if err != nil {
		return nil, fmt.Errorf("get graph data: %w", err)
	}
	if graph.Root() == nil {
		return nil, fmt.Errorf("no root device in graph")
	}

	devices := graph.Devices()
	cars := getCars()

	struct_map := make(map[string]to5) // Map für das gebündelte schreiben aller Daten in JSON
	for id, _ := range devices {

		change_status := graph.GetDevice(id).State.RequestedCurrent // Statusänderungen prüfen
		if change_status != nil {                                   // Prüfe, ob ein Auto geladen werden soll

			carData := graph.GetDevice(id).Config           //Devicegraph config auslesen
			RfidKey := graph.GetDevice(id).State.Additional // TODO Mit echten Daten Prüfen ob es so funktioniert!
			// für weitere Test random generator beibehalten
			fmt.Println(RfidKey)
			//Werte den einzelnen Struct Elementen aus der Car_GO.json zuweisen
			//Alle "NICHT" gesetzten Werte auf "". Python macht den rest
			y := rand.Intn(1000) // Zufallsgenerator
			t := strconv.Itoa(y)

			cars.RfidKey = t
			cars.Distance = ""
			cars.AutoVerbrauch = ""
			cars.DriverType = carData.DriverType
			cars.DriverParams = string(carData.DriverParams)
			cars.ParentDeviceId = carData.ParentDeviceId
			cars.Name = carData.Name
			cars.Description = carData.Description
			cars.MaxCurrentSinglePhase = carData.MaxCurrentSinglePhase
			cars.MaxCurrentPhase1 = carData.MaxCurrentPhase1
			cars.MaxCurrentPhase2 = carData.MaxCurrentPhase2
			cars.MaxCurrentPhase3 = carData.MaxCurrentPhase3
			cars.MinCurrentSinglePhase = carData.MinCurrentSinglePhase
			cars.MinCurrentPhase1 = carData.MinCurrentPhase1
			cars.MinCurrentPhase2 = carData.MinCurrentPhase2
			cars.MinCurrentPhase3 = carData.MinCurrentPhase3
			cars.AvailableCurrentSteps = carData.AvailableCurrentSteps
			cars.GreenEnergy = carData.GreenEnergy
			cars.BatteryCapacity = carData.BatteryCapacity
			cars.BatteryCapacityLowerLimit = carData.BatteryCapacityLowerLimit
			cars.IsMainMeter = carData.IsMainMeter
			cars.IgnoreState = carData.IgnoreState
			cars.PhaseMappingL1 = carData.PhaseMapping.L1
			cars.PhaseMappingL2 = carData.PhaseMapping.L2
			cars.PhaseMappingL3 = carData.PhaseMapping.L3
			cars.IsControllable = carData.IsControllable
			cars.DistributeCurrent = carData.DistributeCurrent

			//[id] ist der Parent-Key des Structs in JSON
			//Hier wird nur die Map erstellt. Sie wird noch nicht in JSON gespeichert
			struct_map[id] = to5{RfidKey: cars.RfidKey, Distance: cars.Distance, AutoVerbrauch: cars.AutoVerbrauch,
				DriverType: cars.DriverType, DriverParams: cars.DriverParams,
				ParentDeviceId: cars.ParentDeviceId, Name: cars.Name, Description: cars.Description,
				MaxCurrentSinglePhase: cars.MaxCurrentSinglePhase, MaxCurrentPhase1: cars.MaxCurrentPhase1,
				MaxCurrentPhase2: cars.MaxCurrentPhase2, MaxCurrentPhase3: cars.MaxCurrentPhase3, MinCurrentSinglePhase: cars.MinCurrentSinglePhase,
				MinCurrentPhase1: cars.MinCurrentPhase1, MinCurrentPhase2: cars.MinCurrentPhase2, MinCurrentPhase3: cars.MinCurrentPhase3,
				AvailableCurrentSteps: cars.AvailableCurrentSteps, GreenEnergy: cars.GreenEnergy, BatteryCapacity: cars.BatteryCapacity,
				BatteryCapacityLowerLimit: cars.BatteryCapacityLowerLimit, IsMainMeter: cars.IsMainMeter, IgnoreState: cars.IgnoreState,
				PhaseMappingL1: cars.PhaseMappingL1, PhaseMappingL2: cars.PhaseMappingL2, PhaseMappingL3: cars.PhaseMappingL3,
				IsControllable: cars.IsControllable, DistributeCurrent: cars.DistributeCurrent}

		}
	}

	// Python ausführen, wenn alle zu ladenden Fahrzeuge in cars.go abgespeichert, 5min um, oder Änderungen zum vorherigen Durchlauf vorhanden sind
	read_carsjson := read_cars_json()
	gleiche_dateien := reflect.DeepEqual(read_carsjson, struct_map)

	if gleiche_dateien == true && use_python_code == false { // Wenn keine Änderungen vorhanden "und" noch keine 5min um sind, seit letztem Python aufruf
		// They are the same --- nix weiter machen

		return map[string]model.TargetDeviceState{}, nil

	} else if gleiche_dateien == false || use_python_code == true { // Wenn Änderungen vorhanden "oder" 5min um sind, seit letztem Python aufruf
		// They are different --- neuen Struct abspeichern und python ausführen

		use_python_code = false

		saveCars(struct_map) //struct_map abspeichern für Python

		cmd := exec.Command("python", "Charging.py", "--input-file", "documents/doc.png") //main.go File im Hauptordner Go wird ausgeführt
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			panic(err)
		}
		stderr, err := cmd.StderrPipe()
		if err != nil {
			panic(err)
		}
		err = cmd.Start()
		if err != nil {
			panic(err)
		}

		go copyOutput(stdout) // Python ausgaben in GO anzeigen
		go copyOutput(stderr)

		cmd.Wait()
	}

	return map[string]model.TargetDeviceState{}, nil
}

func getCars() (ID to5) { // Funktion um Json File Lesbar zu machen

	file, err := os.Open("./JSON_files/cars_GO.json")
	if err != nil {
		log.Fatal(err)
	}

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println("error reading file", err)
		return
	}

	var Conf to5
	err = json.Unmarshal(fileBytes, &Conf)

	if err != nil {
		fmt.Println("unmarshalling error ", err)
		return
	}
	/*
		fmt.Printf("File content:\n%v\n", string(fileBytes))
		fmt.Printf("Conf: %v\n", Conf)
		fmt.Printf("Content: \n %v \nType: %T", Conf.RFID.Consumption, Conf)
	*/
	return Conf
}

func read_cars_json() map[string]to5 { // Funktion um Json File Lesbar zu machen

	file, err := os.Open("./JSON_files/cars.json")
	if err != nil {
		log.Fatal(err)
	}

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println("error reading file", err)
	}
	if len(fileBytes) == 0 {
		fileBytes = []byte{'{', '}'}
	}
	var objmap map[string]to5
	if err := json.Unmarshal(fileBytes, &objmap); err != nil {
		log.Fatal(err)
	}
	return objmap
}

func saveCars(struct_map map[string]to5) {
	// Funktion um lesbare Json File wieder zu speichern
	//Erzeugt sichtbare JSON datei erst nach Beendigung des Codes, Daten sind aber schon geschrieben!
	carBytes, err := json.Marshal(struct_map) // wieder in Bytes umwandeln
	if err != nil {
		panic(err)
	}
	//Beim Ausführen wird alles Vorherige überschrieben!
	err = ioutil.WriteFile("JSON_files/cars.json", carBytes, 0644) // Devicegraph config in JSON schreiben
	if err != nil {
		panic(err)
	}
}

func copyOutput(r io.Reader) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func fPtr(f float64) *float64 {
	return &f
}

func init() { // Init wird nur einmalig aufgerufen für den Zufallsgenerator

	rand.Seed(time.Now().UnixNano())
}
