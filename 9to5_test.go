package _to5

import (
	"encoding/json"
	"github.com/EAAZE/energy-management/internal/model"
	"github.com/EAAZE/energy-management/internal/service/strategy/strategytest"
	"testing"
)

func Test_to5_Execute(t *testing.T) {
	type args struct {
		graph       model.DeviceGraph
		gridCurrent *float64
		params      json.RawMessage
	}

	in := `{"greenEnergyOnly": true}`
	in2 := `{"greenEnergyOnly": false}`
	in3 := `{"greenEnergyOnly": true, "priorityDevices": {"bender1": 0, "bender4": 10}}`

	configBytes := []byte(in)
	configBytes2 := []byte(in2)
	configBytes3 := []byte(in3)

	tests := []struct {
		name    string
		args    args
		want    map[string]model.TargetDeviceState
		wantErr bool
		setup   func(a args)
	}{
		{
			name: "green energy - unsatisfied minimums",
			args: args{
				graph:       strategytest.BuildDeviceGraph(),
				gridCurrent: fPtr(50000),
				params:      configBytes,
			},
			want: map[string]model.TargetDeviceState{
				"bender_master1": {CurrentPhase1: fPtr(0), CurrentPhase2: fPtr(0), CurrentPhase3: fPtr(0)},
				"bender_master2": {CurrentPhase1: fPtr(0), CurrentPhase2: fPtr(0), CurrentPhase3: fPtr(0)},
			},
			wantErr: false,
		},
		{
			name: "green energy - partly satisfied minimums",
			setup: func(a args) {
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L1 = fPtr(-1800) // only needed for direction
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L2 = fPtr(-1800) // only needed for direction
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L3 = fPtr(1800)  // only needed for direction

				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L1 = fPtr(2)
				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L2 = fPtr(6)
				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L3 = fPtr(4)
			},
			args: args{
				graph:       strategytest.BuildDeviceGraph(),
				gridCurrent: fPtr(50000),
				params:      configBytes,
			},
			want: map[string]model.TargetDeviceState{
				"bender_master1": {CurrentPhase1: fPtr(4), CurrentPhase2: fPtr(4), CurrentPhase3: fPtr(4)},
				"bender_master2": {CurrentPhase1: fPtr(0), CurrentPhase2: fPtr(0), CurrentPhase3: fPtr(0)},
			},
			wantErr: false,
		},
		{
			name: "green energy - satisfied minimums",
			setup: func(a args) {
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L1 = fPtr(-1800) // only needed for direction
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L2 = fPtr(-1800) // only needed for direction
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L3 = fPtr(1800)  // only needed for direction

				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L1 = fPtr(16)
				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L2 = fPtr(15)
				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L3 = fPtr(17)
			},
			args: args{
				graph:       strategytest.BuildDeviceGraph(),
				gridCurrent: fPtr(50000),
				params:      configBytes,
			},
			want: map[string]model.TargetDeviceState{
				"bender_master1": {CurrentPhase1: fPtr(3.67), CurrentPhase2: fPtr(3.67), CurrentPhase3: fPtr(3.67)},
				"bender_master2": {CurrentPhase1: fPtr(3.67), CurrentPhase2: fPtr(3.67), CurrentPhase3: fPtr(3.67)},
			},
			wantErr: false,
		},
		{
			name: "all energy",
			args: args{
				graph:       strategytest.BuildDeviceGraph(),
				gridCurrent: fPtr(50000),
				params:      configBytes2,
			},
			want: map[string]model.TargetDeviceState{
				"bender_master1": {
					CurrentPhase1: fPtr(12.67),
					CurrentPhase2: fPtr(12.67),
					CurrentPhase3: fPtr(12.67),
				},
				"bender_master2": {
					CurrentPhase1: fPtr(5.33),
					CurrentPhase2: fPtr(5.33),
					CurrentPhase3: fPtr(5.33),
				},
			},
			wantErr: false,
		},
		{
			name: "prioritized",
			setup: func(a args) {
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L1 = fPtr(-400) // only needed for direction
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L2 = fPtr(-380) // only needed for direction
				a.graph.GetDevice("root").State.PowerReading.AC3.Power.L3 = fPtr(420)  // only needed for direction

				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L1 = fPtr(150)
				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L2 = fPtr(180)
				a.graph.GetDevice("root").State.PowerReading.AC3.Current.L3 = fPtr(200)
			},
			args: args{
				graph:       strategytest.BuildDeviceGraph(),
				gridCurrent: fPtr(50000),
				params:      configBytes3,
			},
			want: map[string]model.TargetDeviceState{
				"bender_master1": {
					CurrentPhase1: fPtr(12.67),
					CurrentPhase2: fPtr(12.67),
					CurrentPhase3: fPtr(12.67),
				},
				"bender_master2": {
					CurrentPhase1: fPtr(5.33),
					CurrentPhase2: fPtr(5.33),
					CurrentPhase3: fPtr(5.33),
				},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &to5{}

			if tt.setup != nil {
				tt.setup(tt.args)
			}

			got, err := e.Execute(tt.args.graph, tt.args.gridCurrent, tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("Execute() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			wantKeys := make([]string, len(tt.want))
			gotKeys := make([]string, len(got))

			i := 0
			for k := range tt.want {
				wantKeys[i] = k
				i++
			}

			j := 0
			for k := range got {
				gotKeys[j] = k
				j++
			}
			/*
				// iteration order of map keys is random, so we need to sort them
				sort.Strings(wantKeys)
				sort.Strings(gotKeys)

				if !reflect.DeepEqual(gotKeys, wantKeys) {
					t.Errorf("Execute() expected other devices: got %v, want %v", gotKeys, wantKeys)
				}

				for id, target := range tt.want {
					if val, ok := got[id]; ok {
						if !val.Equals(target) {
							t.Errorf("Execute() device with id %v: got: %v, want: %v", id, val.ToString(), target.ToString())
						}
					}
				}
			*/
		})
	}
}

/*
func Test_to5_Execute(t *testing.T) {

	// TODO ROUNDCURRENT verwenden zum runden der Phasen-Werte
	s := to5{}

	// So setzt man alle Werte für ein Device
	configs := map[string]pkg.DeviceConfiguration{
		"1": {
			DriverType:                "Bender_Master",
			DriverParams:              nil,
			ParentDeviceId:            nil,
			Name:                      "",
			Description:               "Nur einmal laden Bitte",
			MaxCurrentSinglePhase:     fPtr(18),
			MaxCurrentPhase1:          fPtr(6),
			MaxCurrentPhase2:          fPtr(6),
			MaxCurrentPhase3:          fPtr(6),
			MinCurrentSinglePhase:     fPtr(6),
			MinCurrentPhase1:          fPtr(2),
			MinCurrentPhase2:          fPtr(2),
			MinCurrentPhase3:          fPtr(2),
			AvailableCurrentSteps:     nil,
			GreenEnergy:               false,
			BatteryCapacity:           nil,
			BatteryCapacityLowerLimit: nil,
			IsMainMeter:               false,
			IgnoreState:               false,
			PhaseMapping:              pkg.PhaseMapping{L1: 3, L2: 2, L3: 1},
			IsControllable:            false,
			DistributeCurrent:         false,
		},
	}

	graph := model.MakeDeviceGraph(configs, "1") // convertiere in JSOn sieht alle infos die zur verfügung stehen

	target, err := s.Execute(graph)
	if err != nil {
		panic(err)
	}
	fmt.Println(target)
}


configs := map[string]pkg.DeviceConfiguration{
"1": {
DriverType:                "Bender_Master",
DriverParams:              nil,
ParentDeviceId:            nil,
Name:                      "",
Description:               "",
MaxCurrentSinglePhase:     nil,
MaxCurrentPhase1:          nil,
MaxCurrentPhase2:          nil,
MaxCurrentPhase3:          nil,
MinCurrentSinglePhase:     nil,
MinCurrentPhase1:          nil,
MinCurrentPhase2:          nil,
MinCurrentPhase3:          nil,
AvailableCurrentSteps:     nil,
GreenEnergy:               false,
BatteryCapacity:           nil,
BatteryCapacityLowerLimit: nil,
IsMainMeter:               false,
IgnoreState:               false,
PhaseMapping:              pkg.PhaseMapping{},
IsControllable:            false,
DistributeCurrent:         false,
},
}
graph := model.MakeDeviceGraph(configs, "1")
target, err := s.Execute(graph, nil, []byte{})
	if err != nil {
		panic(err)
	}
	fmt.Println(target)
*/
