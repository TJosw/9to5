package _to5

type Params struct {
	GreenEnergyOnly bool
	CollectMetrics  bool
	PriorityDevices map[string]int // priority  device id
}
