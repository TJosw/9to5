
'''
Diese Klasse gibt neue Werte aus, mit der zum Laden zur Verfügung stehenden Leistung pro betrachtetem Zeitschritt,
unter Berücksichtigung der PV Erzeugung in Verknüpfung mit einem Batteriespeicher.
Als Input wird die zum Laden verfügbare Energie aus der Klasse PVsystem genutzt. Hier ist die Standortlast bereits abgezogen.
Der Batteriezustand pro Zeitschritt wird ermittelt. Es wird das AC-Konzept der Kopplung betrachtet.
Eine mögliche Erweiterung bietet das DC-Konzept.
'''

import datetime
from openpyxl import Workbook


class BatteryStorage:
    def __init__(self, name, total_capacity, max_charge_p, max_discharge_p, min_charge_p, pv_data, car_arrive, car_depart,
                 useable_capacity=None, dod=None, soc=0, concept=None, max_charge_car=None, efficiency_factor=0.95):
        self.name = name
        self.total_capacity = total_capacity  # Kapazität (brutto)
        self.useable_capacity = useable_capacity  # Kapazität (netto)
        self.max_charge_p = max_charge_p  # max. Ladeleistung in kW
        self.max_discharge_p = max_discharge_p  # max. Entladeleistung in kW
        self.min_charge_p = min_charge_p  # min. Lade- und Entladeleistung
        self.dod = dod  # Depth of Discharge / Entladetiefe
        self.soc = soc  # State of Charge / Ladezustand zu Beginn

        self.pv_data = pv_data  # auf Basis von energy_time Speicher be- und entladen
        self.car_arrive = car_arrive  # Ankuftszeit Autos
        self.car_depart = car_depart  # Abfahrtzeit Auto
        self.concept = concept  # None, AC, DC, je nach Konzept auch Input Daten anders, Wechselrichter noch einbauen?

        # maximale Ladeleistung der betrachteten Ladesäule
        self.max_charge_car = max_charge_car

        # Wirkungsgrad des Wechselrichters des Batteriespeichers
        self.efficiency_factor = efficiency_factor

    def get_energy(self):
        if self.concept == "AC":

            # Ergebnislisten
            energy_time = []
            soc_time = []
            charge_power_time = []
            capacity_time = []

            # Flexibilitätszeitraum zum Laden zwischen Ankunfts- und Abfahrtszeitpunkt des Autos, Prio Auto
            car_arrive = datetime.datetime.strptime(self.car_arrive, '%Y-%m-%d %H:%M:%S')
            car_depart = datetime.datetime.strptime(self.car_depart, '%Y-%m-%d %H:%M:%S')

            # PV-Erzeugung minus Standortlast aus Klasse PVSystem
            pv_data = self.pv_data

            # Stundenfaktor ermitteln
            h_factor = abs((pv_data[0][0] - pv_data[1][0])).seconds / 3600

            # DoD oder Nutzbare Kapazität errechnen, wenn nicht vorhanden
            if self.useable_capacity is None and self.dod is not None:
                useable_capacity = round(self.total_capacity - (self.total_capacity * (self.dod / 100)))
            elif self.useable_capacity is not None and self.dod is None:
                dod = (1 - (self.useable_capacity / self.total_capacity)) * 100
                useable_capacity = self.useable_capacity
            else:
                useable_capacity = self.useable_capacity
                dod = self.dod

            # minimale Kapazität, darf nicht unterschritten werden
            capacity_min = self.total_capacity - useable_capacity

            # mit Angabe DOD, darf trotzdem nicht unter min. Kapazität gehen
            if (dod / 100) * self.total_capacity < capacity_min:
                dod = (capacity_min / self.total_capacity) * 100

            # Maximale Ladeleistung (BE und Entladeleistung) pro Zeitschritt von der Batterie benötigt
            max_charge_p = self.max_charge_p  # in kW
            max_discharge_p = self.max_discharge_p  # in kW

            # SOC zu Beginn der Betrachtung
            soc_start = self.soc

            # Kapazität zu Betrachtungsbeginn, Anteil nutzbarer Kapazität
            capacity_start = (soc_start / 100) * self.useable_capacity  # in kWh

            # Zu ladene Kapazität
            capacity_diff = self.useable_capacity - capacity_start   # in kWh

            # Jeder einzelne "Zeitschritt" wird durchlaufen
            for i in range(0, len(pv_data)):

                t = pv_data[i][0]  # datetime

                # Aus PV Anlage zur Verfügung stehende Leistung, schon mit Abzug Standortlast, prinzipiell erstmal
                # wenn negativ, dann Entladen Batteriespeicher
                # wenn positiv, dann Beladen Batteriespeicher
                p = pv_data[i][1]  # in kW

                # Auto nicht da
                if t < car_arrive or t >= car_depart:

                    # Beladevorgang
                    # wenn pv_data positiv beladen bis Batterie voll
                    if p > 0 and capacity_diff > 0:

                        # Ladeleistung in Abh. zur Verfügung stehender PV Erzeugung festlegen
                        if p < max_charge_p:
                            charge_power_t = p
                        # wenn volle Ladeleistung durch PV Erzeugung zur Verfügung steht
                        else:
                            charge_power_t = max_charge_p

                        capacity_t = charge_power_t * h_factor  # geladene Kapazität pro Zeitschritt

                        # Berücksichtigung Wechselrichter und Verluste über Wirkungsgrad
                        capacity_t = self.efficiency_factor * capacity_t

                        capacity_total = capacity_min + capacity_start + capacity_t  # aufaddieren der Gesamtkapazität

                        # Gesamtkapazität darf maximal möglche Kapazität nicht überschreiten, letzter Ladeschritt!!
                        if capacity_total > self.total_capacity:
                            diff = capacity_total - self.total_capacity
                            capacity_total = capacity_total - diff  # = max. Kapazität dann
                            capacity_t = capacity_total - capacity_start - capacity_min  # = Kapazität Diff bei letztem Ladeschritt
                            charge_power_t = capacity_t / h_factor

                        # bei Unterschreitung minimaler Ladeleistung, kein Beladevorgang mehr
                        if charge_power_t < self.min_charge_p:
                            charge_power_t = 0
                            capacity_t = 0
                            capacity_total = capacity_min + capacity_start + capacity_t

                        new_p = p - charge_power_t

                    # Entladevorgang, wenn PV Leistung Standortlast nicht decken kann und Kapazität größer als min
                    # wenn pv_data negativ entladen, bis keine nutzbarer Kapazität
                    elif p < 0 < capacity_start:
                        # Entladeleistung in Abhängigkeit benötigter Leistung feststellen
                        if abs(p) <= max_discharge_p:
                            charge_power_t = abs(p)
                        else:
                            charge_power_t = max_discharge_p

                        # Kapazität die in dem Zeitschritt von Gesamtkapazität abgezogen wird
                        capacity_t = charge_power_t * h_factor * (-1)

                        # Berücksichtigung Wechselrichter und Verluste über Wirkungsgrad
                        capacity_t = self.efficiency_factor * capacity_t

                        capacity_total = capacity_min + capacity_start + capacity_t

                        # minimale Kapazität darf nicht unterschritten werden!!
                        if capacity_total < capacity_min:
                            diff = capacity_min - capacity_total
                            capacity_total = capacity_total + diff  # = min. Kapazität
                            capacity_t = capacity_start  # letzte Rest wird entladen
                            charge_power_t = capacity_t / h_factor

                        # bei Unterschreitung minimaler Ladeleistung, kein Beladevorgang mehr
                        if charge_power_t < self.min_charge_p:
                            charge_power_t = 0
                            capacity_t = 0
                            capacity_total = capacity_min + capacity_start + capacity_t

                        new_p = p + charge_power_t

                    # Weder be- noch entladen
                    else:
                        charge_power_t = 0
                        capacity_t = 0
                        capacity_total = capacity_min + capacity_start + capacity_t
                        capacity_diff = capacity_diff - capacity_t

                        new_p = p

                    soc_t = ((capacity_total - capacity_min) / self.useable_capacity) * 100

                # Auto da, Auto Vorrang
                # maximale Ladeleistung vom Auto, für den Fall das Auto da ist, erst alles was darüber ist Beladen
                if car_arrive <= t < car_depart:

                    # wenn max. Ladeleistung Ladesäule vorliegt
                    if self.max_charge_car is not None:

                        # Beladevorgang, wenn mehr PV Erzeugung als max. Ladeleistung Ladesäule
                        if p > self.max_charge_car and capacity_diff > 0:
                            p_charge = p - self.max_charge_car
                            # Ladeleistung in Abh. zur Verfügung stehender PV Erzeugung festlegen
                            if p_charge < max_charge_p:
                                charge_power_t = p_charge
                            # wenn volle Ladeleistung durch PV Erzeugung zur Verfügung steht
                            else:
                                charge_power_t = max_charge_p

                            capacity_t = charge_power_t * h_factor  # geladene Kapazität pro Zeitschritt

                            # Berücksichtigung Wechselrichter und Verluste über Wirkungsgrad
                            capacity_t = self.efficiency_factor * capacity_t

                            capacity_total = capacity_min + capacity_start + capacity_t  # Gesamtkapazität

                            # Gesamtkapazität darf maximal möglche Kapazität nicht überschreiten
                            if capacity_total > self.total_capacity:
                                diff = capacity_total - self.total_capacity
                                capacity_total = capacity_total - diff  # = max. Kapazität dann
                                capacity_t = capacity_total - capacity_start - capacity_min  # = Kapazität Diff bei letzem Ladeschritt
                                charge_power_t = capacity_t / h_factor

                            new_p = p - charge_power_t

                        # Entladevorgang, wenn PV Erzeugung max. Ladeleistung Auto nicht decken kann
                        # Entladen was maximal möglich ist in dem Zeitschritt, p könnte aber auch negativ sein
                        elif p < self.max_charge_car and capacity_start > 0:
                            if p < 0:
                                # Entladeleistung in Abhängigkeit benötigter Leistung feststellen
                                if abs(p) + self.max_charge_car <= max_discharge_p:
                                    charge_power_t = abs(p) + self.max_charge_car
                                else:
                                    charge_power_t = max_discharge_p

                                # Kapazität die in dem Zeitschritt von Gesamtkapazität abgezogen wird
                                capacity_t = charge_power_t * h_factor * (-1)

                                # Berücksichtigung Wechselrichter und Verluste über Wirkungsgrad
                                capacity_t = self.efficiency_factor * capacity_t

                                capacity_total = capacity_min + capacity_start + capacity_t

                                # minimale Kapazität darf nicht unterschritten werden!!
                                if capacity_total < capacity_min:
                                    diff = capacity_min - capacity_total
                                    capacity_total = capacity_total + diff  # = min. Kapazität
                                    capacity_t = capacity_start
                                    charge_power_t = capacity_t / h_factor

                                # bei Unterschreitung minimaler Ladeleistung, kein Beladevorgang mehr
                                if charge_power_t < self.min_charge_p:
                                    charge_power_t = 0
                                    capacity_t = 0
                                    capacity_total = capacity_min + capacity_start + capacity_t

                                new_p = p + charge_power_t

                            # Wenn PV Data positiv ist
                            else:
                                diff = self.max_charge_car - p
                                # nur so viel Entladeleistung um max. Ladeleistung Auto zu decken
                                # aber nur bis zu max. Entladeleistung des Batteriespeichers
                                if diff > max_discharge_p:
                                    charge_power_t = max_discharge_p
                                else:
                                    charge_power_t = abs(diff)

                                # Kapazität die in dem Zeitschritt von Gesamtkapazität abgezogen wird
                                capacity_t = charge_power_t * h_factor * (-1)

                                # Berücksichtigung Wechselrichter und Verluste über Wirkungsgrad
                                capacity_t = self.efficiency_factor * capacity_t

                                capacity_total = capacity_min + capacity_start + capacity_t

                                # minimale Kapazität darf nicht unterschritten werden!!
                                if capacity_total < capacity_min:
                                    diff = capacity_min - capacity_total
                                    capacity_total = capacity_total + diff  # = min. Kapazität
                                    capacity_t = capacity_start
                                    charge_power_t = capacity_t / h_factor

                                # bei Unterschreitung minimaler Ladeleistung, kein Beladevorgang mehr
                                if charge_power_t < self.min_charge_p:
                                    charge_power_t = 0
                                    capacity_t = 0
                                    capacity_total = capacity_min + capacity_start + capacity_t

                                new_p = p + charge_power_t

                        # ansonsten findet keine Be- oder Entladung in den Zeitschritten statt
                        else:
                            charge_power_t = 0
                            capacity_t = 0
                            capacity_total = capacity_min + capacity_start + capacity_t

                            new_p = p

                    # wenn max. Ladeleistung Ladesäule nicht vorliegt
                    else:
                        # NUR ENTLADEVORGANG, unabhängig von p, wenn Kapazität des Batteriespeichers es zulässt
                        if capacity_start > 0:

                            charge_power_t = max_discharge_p

                            # Kapazität die in dem Zeitschritt von Gesamtkapazität abgezogen wird
                            capacity_t = charge_power_t * h_factor * (-1)

                            # Berücksichtigung Wechselrichter und Verluste über Wirkungsgrad
                            capacity_t = self.efficiency_factor * capacity_t

                            capacity_total = capacity_min + capacity_start + capacity_t

                            # minimale Kapazität darf nicht unterschritten werden!!
                            if capacity_total < capacity_min:
                                diff = capacity_min - capacity_total
                                capacity_total = capacity_total + diff  # = min. Kapazität
                                capacity_t = capacity_start
                                charge_power_t = capacity_t / h_factor

                            # bei Unterschreitung minimaler Ladeleistung, kein Beladevorgang mehr
                            if charge_power_t < self.min_charge_p:
                                charge_power_t = 0
                                capacity_t = 0
                                capacity_total = capacity_min + capacity_start + capacity_t

                        else:
                            # Kein Beladevorgang
                            charge_power_t = 0
                            capacity_t = 0
                            capacity_total = capacity_min + capacity_start + capacity_t

                        new_p = p + charge_power_t

                    soc_t = ((capacity_total - capacity_min) / self.useable_capacity) * 100

                capacity_time.append((capacity_t, capacity_total))
                # zum Laden verfügbare Leistung pro Zeitschritt, in Abhängigkeit Batterie und PV
                energy_time.append((t, new_p))
                charge_power_time.append(charge_power_t)  # Leistung, die in dem Zeitschritt genutzt wird
                soc_time.append(soc_t)

                capacity_start = capacity_total - capacity_min  # neue Startkapazität für den nächsten Zeitschritt
                capacity_diff = capacity_diff - capacity_t  # neue zu ladene Kapazität für nächsten Schritt
                soc_start = soc_t  # neuer SoC für den nächsten Zeitschritt

            wb_battery = Workbook()  # neue Ergebnis Excel Datei erstellen
            ws_battery = wb_battery.active
            ws_battery['A1'] = "Datum und Uhrzeit"
            ws_battery['B1'] = "Zur Verfügung stehende Leistung in kW"
            ws_battery['C1'] = "SoC in % zum Ende des Zeitschritts"
            ws_battery['D1'] = "Ladeleistung in kW"
            ws_battery['E1'] = "Kapazität in kWh"
            ws_battery['F1'] = "Gesamtkapazität in kWh"

            # Ergebnis-Excel
            i = 0
            for row in range(2, len(energy_time)+2):
                ws_battery['A' + str(row)] = energy_time[i][0]
                ws_battery['B' + str(row)] = energy_time[i][1]
                ws_battery['C' + str(row)] = soc_time[i]
                ws_battery['D' + str(row)] = charge_power_time[i]
                ws_battery['E' + str(row)] = capacity_time[i][0]  # positiv heißt Beladung, negativ Entladung
                ws_battery['F' + str(row)] = capacity_time[i][1]
                i += 1

            wb_battery.save(r'INPUT/Leistung_PV_' + str(self.name) + '.xlsx')

            return energy_time

        elif self.concept == "DC":
            '''
            Möglichkeit zur Erweiterung des Programms. 
            Ansatz für die DC-Kopplung. Wechselrichter der PV-Anlage müsste hier mit eingebaut werden und die 
            Standortlastdaten. Be- und Entladeprozess müssten aber trotzdem schon in Abhängigkeit der Standortlast 
            stattfinden. Je nachdem in welcher Form Standortlastdaten zur verfügung stehen.   
            '''

            # PV-Erzeugung ohne Abzug Standortlast und ohne Verrechnung Wechselrichter aus PV-Klasse
            pv_data = self.pv_data

            # leere Listen für Ausgabe Ergebnisse erstellen
            energy_time = []
            soc_time = []
            charge_power_time = []
            capacity_time = []