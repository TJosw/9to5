import os.path
import json
import datetime


def getCars():             # json File lesen
    if os.path.isfile("JSON_files/cars.json"):
        with open('JSON_files/cars.json', newline='') as carFile:
            data = carFile.read()
            if data == "":
                data = '{}'
            cars = json.loads(data)  # Wandelt Daten wieder in ein lesbaren Dictionary
            return cars
    else:
        return {}


def getEqualCars():             # json File lesen
    if os.path.isfile("JSON_files/Equal_cars.json"):
        with open('JSON_files/Equal_cars.json', newline='') as carFile:
            data = carFile.read()
            if data == "":
                data = '{}'
            cars = json.loads(data)  # Wandelt Daten wieder in ein lesbaren Dictionary
            return cars
    else:
        return {}

def get_cars_Data():
    if os.path.isfile("JSON_files/cars_Daten.json"):
        with open('JSON_files/cars_Daten.json', newline='') as carFile:
            data = carFile.read()
            if data == "":
                data = '{}'
            daten = json.loads(data)  # Wandelt Daten wieder in ein lesbaren Dictionary
            return daten
    else:
        return {}

# Für ein spezifisches Auto
def getCar(carID):
    car = getCars()
    return car[carID]


def updateCars(cars):
    with open('JSON_files/cars_Daten.json', 'w', newline='') as carFile:  # öffnet File mit "w" für falls noch nicht existend
        # json.dump(cars, carFile)
        carJSON = json.dumps(cars, indent=4, sort_keys=True, default=str)
        carFile.write(carJSON)


def update_Equal_Cars(cars):
    with open('JSON_files/Equal_cars.json', 'w', newline='') as carFile:  # öffnet File mit "w" für falls noch nicht existend
        # json.dump(cars, carFile)
        carJSON = json.dumps(cars, indent=4, sort_keys=True, default=str)
        carFile.write(carJSON)


def read_Data_from_Dic(dicAutos):
    new_dict = {}
    for key, value in dicAutos.items():
        # Diesen vorgehen gewählt, da keine leeren Werte ("") an die Klassen übergeben werden dürfen.
        # Werden in GO allerdings Leer gesetzt!
        # So sind alle Daten immer im richtigen Format abrufbereit
        Rfid_key = value['RfidKey']
        if Rfid_key == "":
            Rfid_key = None
        Distance = value['Distance']
        if Distance == "":
            Distance = None
        AutoVerbrauch = value['AutoVerbrauch']
        if AutoVerbrauch == "":
            AutoVerbrauch = None
        DriverType = value['DriverType']
        if DriverType == "":
            DriverType = None
        DriverParams = value['DriverParams']
        if DriverParams == "":
            DriverParams = None
        ParentDeviceId = value['ParentDeviceId']
        if ParentDeviceId == "":
            ParentDeviceId = None
        Name = value['Name']
        if Name == "":
            Name = None
        Description = value['Description']
        if Description == "":
            Description = None
        MaxCurrentSinglePhase = value['MaxCurrentSinglePhase']
        if MaxCurrentSinglePhase == "":
            MaxCurrentSinglePhase = None
        MaxCurrentPhase1 = value['MaxCurrentPhase1']
        if MaxCurrentPhase1 == "":
            MaxCurrentPhase1 = None
        MaxCurrentPhase2 = value['MaxCurrentPhase2']
        if MaxCurrentPhase2 == "":
            MaxCurrentPhase2 = None
        MaxCurrentPhase3 = value['MaxCurrentPhase3']
        if MaxCurrentPhase3 == "":
            MaxCurrentPhase3 = None
        MinCurrentSinglePhase = value['MinCurrentSinglePhase']
        if MinCurrentSinglePhase == "":
            MinCurrentSinglePhase = None
        MinCurrentPhase1 = value['MinCurrentPhase1']
        if MinCurrentPhase1 == "":
            MinCurrentPhase1 = None
        MinCurrentPhase2 = value['MinCurrentPhase3']
        if MinCurrentPhase2 == "":
            MinCurrentPhase2 = None
        MinCurrentPhase3 = value['MinCurrentPhase3']
        if MinCurrentPhase3 == "":
            MinCurrentPhase3 = None
        AvailableCurrentSteps = value['AvailableCurrentSteps']
        if AvailableCurrentSteps == "":
            AvailableCurrentSteps = None
        GreenEnergy = value['GreenEnergy']
        if GreenEnergy == "":
            GreenEnergy = None
        BatteryCapacity = value['BatteryCapacity']
        if BatteryCapacity == "":
            BatteryCapacity = None
        BatteryCapacityLowerLimit = value['BatteryCapacityLowerLimit']
        if BatteryCapacityLowerLimit == "":
            BatteryCapacityLowerLimit = None
        IsMainMeter = value['IsMainMeter']
        if IsMainMeter == "":
            IsMainMeter = None
        IgnoreState = value['IgnoreState']
        if IgnoreState == "":
            IgnoreState = None
        L1 = value['L1']
        if L1 == "":
            L1 = None
        L2 = value['L2']
        if L2 == "":
            L2 = None
        L3 = value['L3']
        if L3 == "":
            L3 = None
        IsControllable = value['IsControllable']
        if IsControllable == "":
            IsControllable = None
        DistributeCurrent = value['DistributeCurrent']
        if DistributeCurrent == "":
            DistributeCurrent = None

        new_dict[key] = {'RfidKey': Rfid_key, 'Distance': Distance, 'AutoVerbrauch': AutoVerbrauch,
                         'DriverType': DriverType, 'DriverParams': DriverParams,
                         'ParentDeviceId': ParentDeviceId, 'Name': Name, 'Description': Description,
                         'MaxCurrentSinglePhase': MaxCurrentSinglePhase, 'MaxCurrentPhase1': MaxCurrentPhase1,
                         'MaxCurrentPhase2': MaxCurrentPhase2, 'MaxCurrentPhase3': MaxCurrentPhase3,
                         'MinCurrentSinglePhase': MinCurrentSinglePhase, 'MinCurrentPhase1': MinCurrentPhase1,
                         'MinCurrentPhase2': MinCurrentPhase2, 'MinCurrentPhase3': MinCurrentPhase3,
                         'AvailableCurrentSteps': AvailableCurrentSteps, 'GreenEnergy': GreenEnergy,
                         'BatteryCapacity': BatteryCapacity, 'BatteryCapacityLowerLimit': BatteryCapacityLowerLimit,
                         'IsMainMeter': IsMainMeter, 'IgnoreState': IgnoreState, 'L1': L1, 'L2': L2, 'L3': L3,
                          'IsControllable': IsControllable, 'DistributeCurrent': DistributeCurrent}
    return new_dict


def estimated_distance(wochentag):
    strecke_montag = 37.0  # in km, Wert aus MiD
    strecke_Di_Mi_Do = 37.0  # in km
    strecke_Fr = 37.0  # in km
    strecke_samstag = 30.0  # in km
    strecke_sonntag = 25.0  # in km
    unsicherheitsfaktor = 2.0  # Damit rechnen, dass ein Auto die doppelte Strecke fährt (nach "MID" möglich)

    match wochentag:
            case 0:  # print("Montag") # Annahme: Das Fzg. wird grundsätzlich nicht am Wochenende geladen
                auto_gefahrene_strecke = (strecke_samstag + strecke_sonntag + (0.5 * strecke_montag) + (0.5 * strecke_Fr)) * unsicherheitsfaktor
                return auto_gefahrene_strecke  # Unter der Annahme: Das Fzg. wurde an den vorherigen Werktagen geladen.

            case 1 | 2 | 3:  # print("Di, Mi, Do")

                auto_gefahrene_strecke = ((0.5 * strecke_Di_Mi_Do) + (0.5 * strecke_montag)) * unsicherheitsfaktor
                return auto_gefahrene_strecke

            case 4:  # print("Fr")
                auto_gefahrene_strecke = ((0.5 * strecke_Fr) + (0.5 * strecke_Di_Mi_Do)) * unsicherheitsfaktor
                return auto_gefahrene_strecke

            case 5 | 6:  # print("Sa, So")
                auto_gefahrene_strecke = ((0.5 * strecke_samstag) + (0.5 * strecke_Fr)) * unsicherheitsfaktor
                return auto_gefahrene_strecke

            case 6:  # print("Sa, So")
                auto_gefahrene_strecke = ((0.5 * strecke_sonntag) + (0.5 * strecke_samstag)) * unsicherheitsfaktor
                return auto_gefahrene_strecke


def estimated_consumption(temperatur):

    verbrauch = 20.0    # Durchschnittlicher angenommener Verbrauch in kWh/100km bei 20°C
    heizung = 0.1       # in kWh pro minus 1°C Abweichung unter 20°C, also 1kWh jeweils bei 10°C weniger
    kuehlen = 0.05      # in kWh pro plus 1°C Abweichung über 20°C
    verbrauch_zusatz = 0.0  # Zusätzlicher Verbrauch
    auto_verbrauch = 0.0

    if 10.0 < temperatur < 20.0:
        grad_unterschied = 20.0 - temperatur
        verbrauch_zusatz = heizung * grad_unterschied
        auto_verbrauch = verbrauch + verbrauch_zusatz
    elif 0.0 < temperatur <= 10.0:
        grad_unterschied = 20.0 - temperatur
        verbrauch_zusatz = heizung * grad_unterschied
        auto_verbrauch = (verbrauch * 1.05) + verbrauch_zusatz   # Unsicherheitsfaktor: 5% aufrechnen
    elif temperatur <= 0.0:
        grad_unterschied = 20.0 - temperatur
        verbrauch_zusatz = heizung * grad_unterschied
        auto_verbrauch = (verbrauch * 1.15) + verbrauch_zusatz  # Unsicherheitsfaktor: 10% aufrechnen
                                                                # BSP.: Bei minus 10°C folgt: (20kWh * 1.10%) + 3kWh
                                                                #       -> (20kWh + 2kWH) + 3kWh
    if temperatur > 20.0:
        grad_unterschied = temperatur - 20.0
        verbrauch_zusatz = kuehlen * grad_unterschied
        auto_verbrauch = verbrauch + verbrauch_zusatz

    return auto_verbrauch


def min_max_current(MaxCurrentSinglePhase, MaxCurrentPhase1, MaxCurrentPhase2, MaxCurrentPhase3, MinCurrentSinglePhase,
                    MinCurrentPhase1, MinCurrentPhase2, MinCurrentPhase3 ):
    max_current = 0
    min_current = 0
    load_mode = ''

# Berechnung von "saeule_max_leistung" bezogen auf AC 230V
    if MaxCurrentSinglePhase is None and MaxCurrentPhase1 is None and MaxCurrentPhase2 is None and MaxCurrentPhase3 is None:
        max_current = 32 * 3    # Mit 3 Phasen arbeiten mit Max 32A pro Phase
        load_mode = '3AC'

    elif MaxCurrentSinglePhase is not None:
        max_current = MaxCurrentSinglePhase
        load_mode = '1AC'       # 1AC = SinglePhase

    # Falls nur "einer" der MaxCurrentPhase "not None" ist
    elif ((MaxCurrentPhase1 is not None) and (MaxCurrentPhase2 is None) and (MaxCurrentPhase3 is None)) or (
            (MaxCurrentPhase1 is None) and (MaxCurrentPhase2 is not None) and (MaxCurrentPhase3 is None)) or (
            (MaxCurrentPhase1 is None) and (MaxCurrentPhase2 is None) and (MaxCurrentPhase3 is not None)):
        if MaxCurrentPhase1 is not None:
            max_current = MaxCurrentPhase1
        if MaxCurrentPhase2 is not None:
            max_current = MaxCurrentPhase2
        if MaxCurrentPhase3 is not None:
            max_current = MaxCurrentPhase3
        load_mode = '1AC'       # 1AC = SinglePhase

    # Falls "zwei" der MaxCurrentPhase "not None" ist
    elif ((MaxCurrentPhase1 is not None) and (MaxCurrentPhase2 is not None) and (MaxCurrentPhase3 is None)) or (
            (MaxCurrentPhase1 is None) and (MaxCurrentPhase2 is not None) and (MaxCurrentPhase3 is not None)) or (
            (MaxCurrentPhase1 is not None) and (MaxCurrentPhase2 is None) and (MaxCurrentPhase3 is not None)):
        if (MaxCurrentPhase1 is not None) and (MaxCurrentPhase2 is not None):
            max_current = MaxCurrentPhase1 + MaxCurrentPhase2
        if (MaxCurrentPhase2 is not None) and (MaxCurrentPhase3 is not None):
            max_current = MaxCurrentPhase2 + MaxCurrentPhase3
        if (MaxCurrentPhase1 is not None) and (MaxCurrentPhase3 is not None):
            max_current = MaxCurrentPhase1 + MaxCurrentPhase3
            load_mode = '2AC'       # 2AC = Zwei Phasen

    # Falls "drei" der MaxCurrentPhase "not None" ist
    elif (MaxCurrentPhase1 is not None) and (MaxCurrentPhase2 is not None) and (MaxCurrentPhase3 is not None):
        max_current = MaxCurrentPhase1 + MaxCurrentPhase2 + MaxCurrentPhase3
        load_mode = '3AC'       # 3AC = Drei Phasen

    if 0 <= max_current <= 6:   # Es darf mit mindestens 6A pro Phase geladen werden
        max_current = 6
        load_mode = '1AC'
    saeule_max_leistung = (230 * max_current) / 1000  # P(max) = 230V * A in kWh

# # Berechnung von "saeule_min_leistung" bezogen auf AC 230V
# ----------------------------------------------------------------------------------------------------------------------
    if MinCurrentSinglePhase is None and MinCurrentPhase1 is None and MinCurrentPhase2 is None and MinCurrentPhase3 is None:
        min_current = 6  # Mit 1 Phase arbeiten mit Min 6A pro Phase
        load_mode = '1AC'

    elif MinCurrentSinglePhase is not None:
        min_current = MinCurrentSinglePhase
        load_mode = '1AC'  # 1AC = SinglePhase

    # Falls nur "einer" der MinCurrentPhase "not None" ist
    elif ((MinCurrentPhase1 is not None) and (MinCurrentPhase2 is None) and (MinCurrentPhase3 is None)) or (
            (MinCurrentPhase1 is None) and (MinCurrentPhase2 is not None) and (MinCurrentPhase3 is None)) or (
            (MinCurrentPhase1 is None) and (MinCurrentPhase2 is None) and (MinCurrentPhase3 is not None)):
        if MinCurrentPhase1 is not None:
            min_current = MinCurrentPhase1
        if MinCurrentPhase2 is not None:
            min_current = MinCurrentPhase2
        if MinCurrentPhase3 is not None:
            min_current = MinCurrentPhase3
        load_mode = '1AC'  # 1AC = SinglePhase

    # Falls "zwei" der MinCurrentPhase "not None" ist
    elif ((MinCurrentPhase1 is not None) and (MinCurrentPhase2 is not None) and (MinCurrentPhase3 is None)) or (
            (MinCurrentPhase1 is None) and (MinCurrentPhase2 is not None) and (MinCurrentPhase3 is not None)) or (
            (MinCurrentPhase1 is not None) and (MinCurrentPhase2 is None) and (MinCurrentPhase3 is not None)):
        if (MinCurrentPhase1 is not None) and (MinCurrentPhase2 is not None):
            min_current = MinCurrentPhase1 + MinCurrentPhase2
        if (MinCurrentPhase2 is not None) and (MinCurrentPhase3 is not None):
            min_current = MinCurrentPhase2 + MinCurrentPhase3
        if (MinCurrentPhase1 is not None) and (MinCurrentPhase3 is not None):
            min_current = MinCurrentPhase1 + MinCurrentPhase3
            load_mode = '2AC'  # 2AC = Zwei Phasen

    # Falls "drei" der MinCurrentPhase "not None" ist
    elif (MinCurrentPhase1 is not None) and (MinCurrentPhase2 is not None) and (MinCurrentPhase3 is not None):
        min_current = MinCurrentPhase1 + MinCurrentPhase2 + MinCurrentPhase3
        load_mode = '3AC'  # 3AC = Drei Phasen

    if 0 <= min_current <= 6:  # Es darf mit mindestens 6A geladen werden
        min_current = 6
        load_mode = '1AC'
    saeule_min_leistung = (230 * min_current) / 1000  # P(min) = 230V * A in kWh

    return saeule_max_leistung, saeule_min_leistung, load_mode


def hour_rounder(t):
    # Rundet die Ankunftszeit auf die nächste volle Stunde. Damit eine Temperatur ausgelesen werden kann.
    return (t.replace(second=0, microsecond=0, minute=0, hour=t.hour)
            + datetime.timedelta(hours=1))