import sys
import csv
import os
import pandas as pd
from deepdiff import DeepDiff
from openpyxl import load_workbook
import sql_query
import Car
import Funktionen
import PVsystem
from datetime import datetime
from datetime import timedelta
import forecast
import price


# rechts-Click Local History!!! Für vorherige Speicherstände

timenow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
timenow = datetime.strptime(timenow, '%Y-%m-%d %H:%M:%S')
now = datetime.now()
current_time = now.strftime("%H:%M:%S")
wochentag = datetime.today().weekday()  # Ausgabe des Wochentags in Zahlen von Montag "0" bis Sonntag "6"

data = [timenow]
erzeuge_dateien = False
if os.stat('measure_time/python_time.csv').st_size == 0:  # Könnte der Fall sein beim ersten Ausführen
    with open('measure_time/python_time.csv', 'w') as f:  # open the file in the write mode and write data
        writer = csv.writer(f)  # create the csv writer
        writer.writerow(data)  # write a row to the csv file
else:
    with open('measure_time/python_time.csv', 'r') as file:  # open the file in the read mode
        reader = csv.reader(file)
        row1 = next(reader)  # Damit nur die erste Zeile gelesen wird
        start_date = row1[0]  # String in Datetime umwandeln
        start_date = datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S')
        differ_time = timenow - start_date
        duration_in_s = differ_time.total_seconds()
        # Wenn mehr als 60 min (3600sec) vorüber und es nach 7 Uhr bzw. vor 17 Uhr ist. Da nur "10" Aufrufe möglich!
        if duration_in_s >= 3600.0 and ('07:00:00' <= current_time <= '17:00:00'):
            erzeuge_dateien = True  # = True, neue aktuelle Zeit in csv schreiben und neue Input Dateien erzeugen
            # Vorhersage Preis,VorhersageWatterdaten usw.)


# dateien erzeugen lassen
# Neue aktuelle Zeit in csv schreiben
if erzeuge_dateien:
    with open('measure_time/python_time.csv', 'w') as f:  # open the file in the write mode
        writer = csv.writer(f)  # create the csv writer
        writer.writerow(data)  # Überschreibt automatisch letzte Zeit

# Festlegung Betrachtungszeitraum
stunden_vergangenheit = 9  # None eingeben, wenn keine Vergangenheit betrachtet werden soll
stunden_zukunft = 10
stundenfaktor = 0.25

# Abfrage Vergangenheitswerte
if stunden_vergangenheit is not None and erzeuge_dateien:
    sql_query.get_past_data(h_factor=stundenfaktor, hours_past=stunden_vergangenheit)
    vergangenheit_datei = r'Input/Vergangenheit_PV_Leistung_summe.xlsx'
else:
    vergangenheit_datei = None

# Definition Standort
laengengrad = 10.52814
breitengrad = 52.27256
instituiton = 'G0'  # Auswahl: H0, G0, G1, G2, G3, G4, G5, G6, L0, L1, L2 (nach den Standardlastprofilen des BDEW)
jahresverbrauch = 5  # Durschnittlicher Jahresverbrauch in MWh
lastprofil_max = 20  # maximale vorhandene Leistung am Standort in kW

# Abfrage Wettervorhersage
if erzeuge_dateien:
    forecast.forecast_weather(latitude=breitengrad, longitude=laengengrad, hours_forecast=stunden_zukunft,
                              h_factor=stundenfaktor,
                              # API Keys für openweathermap Schnittstelle und solcast Schnittstelle, Anmeldung als Student
                              openweathermap_key='92200213f31a23d94cdb51659301e466',  # max. 1000 Aufrufe pro Tag
                              solcast_key='_DkdfIZ_iv4YXTVlW1Sl_YQpYRGRTYo1')  # max. 10 Aufrufe pro Tag

    # Abfrage Standortlast, funktioniert aktuell nur mit Stundenfaktor 0.25 !!!
    forecast.forecast_load(hours_forecast=stunden_zukunft, h_factor=stundenfaktor,
                           institution=instituiton, average_load_a=jahresverbrauch, hours_past=stunden_vergangenheit)

    # Abfrage Strompreise
    price.forecast_price(h_factor=stundenfaktor, hours_forecast=stunden_zukunft, hours_past=stunden_vergangenheit)

wetterdaten = r'Input/VorhersageWetterdaten.xlsx'

lastprofil_datei = r'Input/VorhersageStandortlast.xlsx'

strompreis_datei = r'Input/VorhersagePreis.xlsx'

# Definition PV-Anlage
nameAnlage = 'PVelenia'
anstellwinkel = 40
azimut = 180  # azimut = Ausrichutng, 180 entspicht südausrichtung
maxLeistungPVModul = 0.275  # kWp/Modul
maxLeistungPV = 14.3  # Angabe in kWp, Größe PV: 14,3 kWp
anzahlModule = 52
alter = 7  # angenommenes Alter
albedo = 'Asphalt'  # Umgebung PV-Anlage für reflektierten Anteil der Einstrahlung

# Definition Wechselrichter der PV-Anlage
nennleistung = 17  # kW
wirkungsgrad_nennleistung = 98.2  # %
wirkungsgrad_eur = 97.8  # %

unsicherheitsfaktor = 0.04  # Verhältnis zwischen Median absoluter Abweichung aus Prognosevalidierung und Peakleistung
batteriespeicher_konzept = 'AC'  # None, AC, oder DC, DC kann noch erweitert werden

if erzeuge_dateien:
    pvelenia = PVsystem.PVsystem(pv_angle=anstellwinkel, pv_azimut=azimut, weatherdata=wetterdaten,
                                 historical_data=vergangenheit_datei, age=alter, uncertainty=unsicherheitsfaktor,
                                 inverter_power=nennleistung, efficiency_power=wirkungsgrad_nennleistung,
                                 name=nameAnlage,
                                 p_modul_max=maxLeistungPVModul, p_max=maxLeistungPV, efficiency_10power=None,
                                 efficiency_eur=wirkungsgrad_eur, efficiency_cec=None, locationload=lastprofil_datei,
                                 numbermodules=anzahlModule, battery=batteriespeicher_konzept, albedo=albedo)
    pvelenia.get_energy()

erneuerbare_energie_datei_PV = r'Input/Leistung_' + str(nameAnlage) + '.xlsx'
minimaler_soll_SOC = 1
einspeisung = 7.5  # ct / kWh , Einspeisung eventuell bei einem weiteren Ansatz berücksichtigen

# Ende von Codevorgabe
# --------------------------------------------------------------------------------------------------------------

dicAutos = Funktionen.getCars()  # JSON cars.json als dictionary auslesen
dicEqual = Funktionen.getEqualCars()
dicData = Funktionen.get_cars_Data()  # JSON cars_Daten.json als dictionary auslesen, um hier später Daten zu speichern

car_list = []  # Liste erzeugen für die Funktion "combicharging"

diff = DeepDiff(dicAutos, dicEqual)  # Vergleicht cars.json mit Equals_cars.json.
                                    # Um Änderungen seit letzem Python Aufruf zu identifizieren!

if diff != {}:  # Nur ausführen, wenn fahrzeugtechnische Änderungen vorhanden.

    Funktionen.update_Equal_Cars(dicAutos)  # In Equal_cars.json aktuelle Daten für späteren Abgleich speichern

    for key, value in dicAutos.items():  # TODO welche Änderungen müssen wie berücksichtigt werden?????!!!!!!!!

        read_dic_Data = Funktionen.read_Data_from_Dic(dicAutos)  # DicAutos Daten auslesen und Leere ("") Inhalte "None" setzen
        # Ansonsten müssen Leere und None Inhalte immer explizit behandelt (berücksichtigt) werden.

        # Zeiten für das jeweilige Fahrzeug erzeugen
        t = 8  # 8h
        ankunftszeit = timenow  # yyyy-mm-dd hh:mm:ss
        abfahrtszeit = ''
        if wochentag <= 3:  # Montag-Donnerstag 9to5
            if ((now + timedelta(hours=t)).strftime("%H:%M:%S")) < '17:00:00':
                abfahrtszeit = timenow + timedelta(
                    hours=t)  # yyyy-mm-dd hh:mm:ss Resultierende Abfahrtszeit 8h nach
            else:  # Ankunft jedoch noch vor 17 Uhr
                abfahrtszeit = timenow.replace(hour=17, minute=0, second=0)
        elif wochentag == 4:  # Freitag, ab 9Uhr bis 14 Uhr, ab 7 bis 12 Uhr
            if current_time <= '7:00:00':
                abfahrtszeit = timenow.replace(hour=12, minute=0, second=0)
            elif '7:00:00' < current_time <= '8:00:00':
                abfahrtszeit = timenow.replace(hour=13, minute=0, second=0)
            elif '8:00:00' < current_time <= '9:00:00':
                abfahrtszeit = timenow.replace(hour=14, minute=0, second=0)
            else:
                abfahrtszeit = timenow.replace(hour=15, minute=0, second=0)
        elif wochentag == 5 or wochentag == 6:  # Samstag oder Sonntag
            abfahrtszeit = timenow + timedelta(hours=t)

        # TODO Ist es sinnvoll die Abfahrtstemperatur extra zu berücksichtigen? Unterscheidet sich das so sehr?
        # Hier die Temperatur der Ankunftszeit (gerundet auf die nächste volle Stunde) ermitteln aus forecast/solcast Daten
        temperatur = 10.0  # Standard Temperatur, falls keine ermittelt werden kann
        zeit_runden = Funktionen.hour_rounder(ankunftszeit)  # Um die Temperatur einfach aus der Excel Datei lesen zu können
        wb_weather = load_workbook(wetterdaten)
        ws_weather = wb_weather.active
        for row in range(2, ws_weather.max_row + 1):
            if ws_weather['A' + str(row)].value == zeit_runden:
                temperatur = ws_weather['B' + str(row)].value
                break

        # Hier jetzt prüfen ob die Estimator Werte gesetzt werden müssen. Falls keine vom Backend oder Python kamen
        if read_dic_Data[key]['Distance'] is None:  # Wenn keine gefahrene Strecke in Go oder Python in cars.JSON gesetzt
            auto_gefahrene_strecke = Funktionen.estimated_distance(wochentag)  # Fkt.-aufruf für geschätzte Distanz
            read_dic_Data[key]['Distance'] = auto_gefahrene_strecke  # geschätzte Distanz dem Dictionary übergeben

        if read_dic_Data[key]['AutoVerbrauch'] is None:  # Wenn kein Verbrauch in Go oder Python in cars.JSON gesetzt
            auto_verbrauch = Funktionen.estimated_consumption(temperatur)  # Fkt.-aufruf für geschätzten Verbrauch
            read_dic_Data[key]['AutoVerbrauch'] = auto_verbrauch  # geschätzte Distanz dem Dictionary übergeben

        # Minimale/Maximale Leistung & Anzahl der Phasen für den jeweiligen Ladepunkt
        min_max_phase_current = Funktionen.min_max_current(value['MaxCurrentSinglePhase'], value['MaxCurrentPhase1'],
                                                    value['MaxCurrentPhase2'], value['MaxCurrentPhase3'],
                                                    value['MinCurrentSinglePhase'], value['MinCurrentPhase1'],
                                                    value['MinCurrentPhase2'], value['MinCurrentPhase3'])
        saeule_max_leistung = min_max_phase_current[0]
        saeule_min_leistung = min_max_phase_current[1]
        load_mode = min_max_phase_current[2]

        # Fest vordefinierte Werte
        Saeulen_ID_Name = key
        voltage = None  # Wird schon in "Car.min_max_phase_current" mit 230V AC berücksichtigt
        current = None  # Wird schon in "Car.min_max_phase_current" mit 230V AC berücksichtigt
        autobatterie_kapazitaet = 55  # in kWh. Durchschnittlich geschätzte Batteriekapazität
        state_of_health = 1  # TODO Sowie Batterie Wirkungsgrad beachten
        bat_kap_start = 0  # Mit leerem Auto starten, SOC = 0 %

        start_bat_cap = None
        effectiveness = None
        saeulen_effectiveness = 0.98

        # Klassen Car und Saeule ausführen
        cars = Car.Car(autobatterie_kapazitaet, read_dic_Data[key]['AutoVerbrauch'], read_dic_Data[key]['Name'],
                       read_dic_Data[key]['Distance'], ankunftszeit, abfahrtszeit, start_bat_cap, effectiveness,
                       state_of_health, saeule_max_leistung, Saeulen_ID_Name, saeule_min_leistung, load_mode,
                       voltage, current, saeulen_effectiveness)

        # cars Ergebnisse zur Liste hinzufügen
        car_list.append(cars)

        # Aktuelles Fahrzeug zur cars_Daten.JSON hinzufügen
        Car_Data = cars.__dict__

        if value['RfidKey'] is not None:
            x = value['RfidKey']  # rfid_key als Key für die JSON verwenden
        else:
            x = key  # Ladepunkt ID-Name z.B. "bender1" als Key für die JSON verwenden

        # Keys werden den Dictionaries zugeordnet
        dicData[x] = Car_Data

        # Berechnete Werte aus beiden Klassen speichern in cars_JSON
        Funktionen.updateCars(dicData)

# TODO if abfrage: eine 0 für neues Auto, 1 für auto fertig, 2 für Auto ladevorgang abgebrochen
# TODO set_GO-Value (welche Werte werden alle benötigt?)
# TODO verwendetes Auto mit allen wichtigen Daten in eine Extra JSON mit ID(RFID?)/Namen zur erkennung falls schon mal da gewesen
# TODO matplotlib mit allen aktiven fahrzeugen


# TODO Lade-Prioritäten: höchste min. Ladeleistung; übrige Ladezeit; SOC
for auto in car_list: # TODO wird gerade "nur" ausgeführt falls unterschiedliche Daten in cars.json und Equals_cars.json.
                        # Zum ausführen/testen einfach irgend ein Parameter ändern!

    auto.combicharging(price_data=strompreis_datei, renew_data=erneuerbare_energie_datei_PV, c_name=auto.name,
                       t_a=auto.t_a, t_d=auto.t_d, max_bat_cap=auto.max_bat_cap,
                       soc_load_start=auto.soc_arrive, soc_min=minimaler_soll_SOC, load_profile_data=lastprofil_datei,
                       max_load_profile=lastprofil_max, effectiveness=auto.saeulen_effectiveness,
                       powerfeed_price=einspeisung, name=auto.Saeulen_ID_Name, pv_name=nameAnlage, batt_name=None)

    # lastprofil = pd.read_excel(lastprofil_datei)
    # lastprofil = pd.concat(lastprofil, )

sys.exit()
